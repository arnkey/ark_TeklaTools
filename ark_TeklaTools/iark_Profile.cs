﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Tekla.Structures.Model;


namespace ArkLib.MarkElements
{
    /// <summary>
    /// Профиль марки
    /// </summary>
    [Serializable]
    public class ark_Profile
    {

        public string Color
        {
            get { return color; }
            set { this.color = value; }
        }
        public string Material
        {
            get { return material; }
            set { this.material = value; }
        }
        public string Finish
        {
            get { return finish; }
            set { this.finish = value; }
        }
        public string AssemblyPrefix
        {
            get { return asmPrefix; }
            set { this.asmPrefix = value; }
        }
        public string PartPrefix
        {
            get { return partPrefix; }
            set { this.partPrefix = value; }
        }
        public string Profile
        {
            get { return profile; }
            set { this.profile = value; }
        }
        public List<UDA> UDA
        {
            get { return uda; }
        }
        public Type ProfileType
        {
            get { return type; }
        }

        public Position GetPosition
        {
            get { return position; }
            set { this.position = value; }
        }
        public int AssemblyNumber
        {
            get { return asmNumber; }
            set { this.asmNumber = value; }
        }
        public int PartNumber
        {
            get { return partNumber; }
            set { this.partNumber = value; }
        }

        public ArkLib.MarkElements.ark_ProfileLoads Loads
        {
            get { return loads; }
            set { this.loads = value; }
        }

        public string ThisName
        {
            get { return this.name; }
            set { this.name = value; }
        }

        private string color;
        private string finish;
        private string material;
        private string asmPrefix;
        private int asmNumber;
        private string partPrefix;
        private int partNumber;
        private string profile;
        private List<UDA> uda;
        private Type type;
        private Position position = null;
        private ark_ProfileLoads loads = new ark_ProfileLoads();
        private string name = "";

        public enum Type
        {
            Beam,
            Plate
        }

        public void AddUDA(UDA uda)
        {
            this.uda.Add(uda);
        }
        public void DelUDA(UDA uda)
        {
            //this.uda.Remove(uda);
            foreach(UDA u in UDA)
            {
                if (u.Name == uda.Name && u.GetStringValue() == uda.GetStringValue())
                {
                    UDA.Remove(u); break;
                }
            }
        }
        public void AddUDAs(List<UDA> udas)
        {
            this.uda = new List<UDA>(udas);
        }
        private void Generate()
        {
            color = "1";
            finish = "";
            material = "STEEL";
            asmPrefix = "";
            asmNumber = 1;
            partPrefix = "";
            partNumber = 1;
            profile = "PL10*10";
            uda = new List<UDA>();
            type = Type.Beam;
            GetPosition = new Position();
        }

        private void ChangeValues(Part p)
        {
            Generate();
            if (p is Beam) type = Type.Beam;
            else if (p is ContourPlate) type = Type.Plate;
            else
            {
                //System.Windows.Forms.MessageBox.Show("Ошибка инициализации профиля!\nТолько балки и пластины!");
                return;
            }
            this.color = p.Class;
            this.finish = p.Finish;
            this.material = p.Material.MaterialString;
            this.profile = p.Profile.ProfileString;
            this.partNumber = p.PartNumber.StartNumber;
            this.partPrefix = p.PartNumber.Prefix;
            this.asmNumber = p.AssemblyNumber.StartNumber;
            this.asmPrefix = p.AssemblyNumber.Prefix;
            this.position = p.Position;
        }

        /// <summary>
        /// Балка для теклы
        /// </summary>
        /// <returns></returns>
        public void ReturnBeam(ref Beam b)
        {
            b.Name = this.ThisName;
            b.Material.MaterialString = this.Material;
            b.Profile.ProfileString = this.Profile;
            b.Position = this.GetPosition;
            b.Class = this.Color;
            b.PartNumber.StartNumber = this.PartNumber;
            b.PartNumber.Prefix = this.PartPrefix;
            b.AssemblyNumber.StartNumber = this.asmNumber;
            b.AssemblyNumber.Prefix = this.asmPrefix;
            b.Finish = this.Finish;
        }
        /// <summary>
        /// Контурная пластина для теклы
        /// </summary>
        /// <returns></returns>
        //public ContourPlate ReturnPlate()
        //{
        //    ContourPlate c = new ContourPlate();
        //    c.Name = this.ThisName;
        //    c.Material.MaterialString = this.Material;
        //    c.Profile.ProfileString = this.Profile;
        //    c.Position = this.GetPosition;
        //    c.Class = this.Color;
        //    c.PartNumber.StartNumber = this.PartNumber;
        //    c.PartNumber.Prefix = this.PartPrefix;
        //    c.AssemblyNumber.StartNumber = this.asmNumber;
        //    c.AssemblyNumber.Prefix = this.asmPrefix;
        //    c.Finish = this.Finish;
        //    return c;
        //}

        public ark_Profile(ark_Profile ap)
        {
            // TODO
            Generate();
            this.color = ap.color;
            this.finish = ap.finish;
            this.material = ap.material;
            this.profile = ap.profile;
            this.partNumber = ap.partNumber;
            this.partPrefix = ap.partPrefix;
            this.asmNumber = ap.asmNumber;
            this.asmPrefix = ap.asmPrefix;
            this.position = ap.position;
            this.Loads = ap.Loads;
            this.GetPosition = ap.GetPosition;
            this.ThisName = ap.ThisName;
            this.AddUDAs(ap.UDA);
        }

        public ark_Profile()
        {
            Generate();
        }

        public ark_Profile(Beam beam)
        {
            ChangeValues(beam as Part);
        }
        public ark_Profile(ContourPlate plate)
        {
            ChangeValues(plate as Part);
        }
        public ark_Profile(Part part)
        {
            ChangeValues(part);
        }
    }
}
