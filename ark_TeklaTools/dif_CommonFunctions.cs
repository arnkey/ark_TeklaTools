﻿using System;
using System.Linq;

namespace ArkLib.ark_Tekla.Differents
{
    public static class CommonFunctions
    {
        public static byte[,] Transpose(byte[,] temp)
        {
            byte[,] tMatrix = new byte[temp.GetLength(1), temp.GetLength(0)];
            for (int i = 0; i < temp.GetLength(1); i++)
                for (int j = 0; j < temp.GetLength(0); j++)
                    tMatrix[i, j] = temp[j, i];
            return tMatrix;
        }
        public static string[] GetStringColumnFromMatrix(byte[,] temp)
        {
            string[] str = new string[temp.GetLength(0)];
            for (int COL = 0; COL < temp.GetLength(0); COL++)
                for (int ROW = 0; ROW < temp.GetLength(1); ROW++)
                    str[COL] += temp[COL, ROW].ToString();
            return str;
        }
        public static string[] GetStringColumnFromMatrix(int[] temp)
        {
            string[] str = new string[1];
            for (int COL = 0; COL < temp.GetLength(0); COL++)
            {
                Array.Resize<string>(ref str, str.Length + 1);
                str[COL] += temp[COL].ToString();
            }
            return str;
        }
        public static string[] GetStringColumnFromMatrix(int[,] temp)
        {
            string[] str = new string[temp.GetLength(0)];
            for (int COL = 0; COL < temp.GetLength(0); COL++)
                for (int ROW = 0; ROW < temp.GetLength(1); ROW++)
                    str[COL] += temp[COL, ROW].ToString();
            return str;
        }
        public static byte[,] GetBytesFromString(string[] temp)
        {
            byte[,] _temp = new byte[temp.Length, temp[0].Length];
            for (int COL = 0; COL < _temp.GetLength(0); COL++)
            {
                for (int ROW = 0; ROW < _temp.GetLength(1); ROW++)
                {
                    _temp[COL, ROW] = Convert.ToByte(temp[COL].Substring(ROW, 1));
                }
            }
            return _temp;
        }
        public static int[] GetIntFromString(string[] temp)
        {
            int[] _temp = new int[temp.Length];
            for (int COL = 0; COL < _temp.GetLength(0); COL++)
            {
                _temp[COL] = Convert.ToInt32(temp[COL]);
            }
            return _temp;
        }
        public static Array ResizeArray(Array arr, int[] newSizes)
        {
            if (newSizes.Length != arr.Rank)
                throw new ArgumentException("arr must have the same number of dimensions " +
                                            "as there are elements in newSizes", "newSizes");

            var temp = Array.CreateInstance(arr.GetType().GetElementType(), newSizes);
            int length = arr.Length <= temp.Length ? arr.Length : temp.Length;
            Array.ConstrainedCopy(arr, 0, temp, 0, length);
            return temp;
        }
        public static double ReturnCorrectDouble(string number)
        {
            double num = 0;
            bool b = double.TryParse(number, out num);
            if (!b)
            {
                if (number.Contains('.'))
                {
                    number = number.Replace('.', ',');
                    b = double.TryParse(number, out num);
                    if (b) return num;
                    else return double.MaxValue;
                }
                else if (number.Contains(','))
                {
                    number = number.Replace(',', '.');
                    b = double.TryParse(number, out num);
                    if (b) return num;
                    else return double.MaxValue;
                }
                else  return num;
            } else return num;
        }

    }
}
