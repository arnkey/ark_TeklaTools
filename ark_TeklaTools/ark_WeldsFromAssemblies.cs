﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ts = Tekla.Structures;
using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;
using System.Collections;

namespace ark_TeklaTools
{
    public partial class ark_WeldsFromAssemblies : Form
    {
        public ark_WeldsFromAssemblies()
        {
            InitializeComponent();
        }

        private void ark_WeldsFromAssemblies_Load(object sender, EventArgs e)
        {
            tsm.UI.Picker picker = new tsm.UI.Picker();
            tsm.Model model = new tsm.Model();
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mos.GetSelectedObjects();

            tsm.Assembly asm = null;

            if (moe.GetSize() > 0)
            {
                while (moe.MoveNext())
                {
                    if (moe.Current is tsm.Assembly)
                        asm = moe.Current as tsm.Assembly;
                    if (moe.Current is tsm.Part)
                        asm = (moe.Current as tsm.Part).GetAssembly();
                    if (asm != null) break;
                }
            }

            tsm.Part mp = asm.GetMainPart() as tsm.Part;
            TreeNode tr0 = new TreeNode();
            tr0.Name = mp.Identifier.ID.ToString();
            string pos0 = ""; mp.GetReportProperty("PART_POS", ref pos0);
            tr0.Text = "Поз. " + pos0;
            moe = mp.GetWelds();
            while (moe.MoveNext())
            {
                tsm.BaseWeld bw = moe.Current as tsm.BaseWeld;
                TreeNode trw = new TreeNode();
                trw.Name = bw.Identifier.ID.ToString();
                trw.Text = "Сварка (ID): " + trw.Name + ";";
                tr0.Nodes.Add(trw);
            }
            treeView1.Nodes.Add(tr0);

            foreach (tsm.Part p in asm.GetSecondaries())
            {
                moe =  p.GetWelds();
                TreeNode tr = new TreeNode();
                tr.Name = p.Identifier.ID.ToString();
                string pos = ""; p.GetReportProperty("PART_POS", ref pos);
                tr.Text = "Поз. " + pos;

                while (moe.MoveNext())
                {
                    tsm.BaseWeld bw = moe.Current as tsm.BaseWeld;
                    TreeNode trw = new TreeNode();
                    trw.Name = bw.Identifier.ID.ToString();
                    trw.Text = "Сварка (ID): " + trw.Name + ";";
                    tr.Nodes.Add(trw);
                }
                treeView1.Nodes.Add(tr);
            }

            
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            ArrayList al = new ArrayList();
            tsm.ModelObject mo = new tsm.Model().SelectModelObject(new ts.Identifier(Convert.ToInt32(treeView1.SelectedNode.Name)));
            al.Add(mo);
            mos.Select(al);
        }
    }
}
