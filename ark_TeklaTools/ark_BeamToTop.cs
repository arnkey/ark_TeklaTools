﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using tsm = Tekla.Structures.Model;
using ts = Tekla.Structures;
using tsg = Tekla.Structures.Geometry3d;
using ui = Tekla.Structures.Model.UI;

using akCom = ArkLib.ark_Tekla.Common;
using System.Collections;

namespace ark_BeamToTop
{
    public partial class ark_BeamToTop_Form : Form
    {
        public ark_BeamToTop_Form()
        {
            InitializeComponent();
        }

        bool WrongNumber = true;

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            double dbl = 0;
            bool b = double.TryParse(textBox1.Text, out dbl);
            if (b)
            {
                WrongNumber = false;
                textBox1.ForeColor = Color.Black;
            }
            else
            {
                WrongNumber = true;
                textBox1.ForeColor = Color.Red;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
            if (model.GetConnectionStatus() == false) return;
            tsm.WorkPlaneHandler wph = model.GetWorkPlaneHandler();
            tsm.TransformationPlane tp = ArkLib.ark_Tekla.Geometry.ark_New.TP_GLOBAL;
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            wph.SetCurrentTransformationPlane(tp);
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mos.GetSelectedObjects();
            while (moe.MoveNext())
            {
                //if (moe.Current is tsm.Beam)
                //{
                //    tsm.Beam beam = (tsm.Beam)moe.Current;
                //    if (!WrongNumber)
                //    {
                //        tsm.Beam b = moe.Current as tsm.Beam;
                //        tsg.Point p1 = new tsg.Point(ctp.TransformationMatrixToGlobal.Transform(b.StartPoint));
                //        tsg.Point p2 = new tsg.Point(ctp.TransformationMatrixToGlobal.Transform(b.EndPoint));
                //        p1.Z = ArkLib.Differents.CommonFunctions.ReturnCorrectDouble(textBox1.Text);
                //        p2.Z = p1.Z;
                //        b.StartPoint = new tsg.Point(ctp.TransformationMatrixToLocal.Transform(p1));
                //        b.EndPoint = new tsg.Point(ctp.TransformationMatrixToLocal.Transform(p2));
                //        b.Modify();
                //    }
                //}
                tsm.ModelObject mobj = moe.Current as tsm.ModelObject;

                if (mobj is tsm.Part)
                {
                    tsm.Part p = mobj as tsm.Part;
                    double z = ctp.TransformationMatrixToGlobal.Transform(p.GetReferenceLine(false)[0] as tsg.Point).Z;
                    tsg.Vector v = new tsg.Vector(0, 0,
                        ArkLib.ark_Tekla.Differents.CommonFunctions.ReturnCorrectDouble(textBox1.Text) - z);
                    tsm.Operations.Operation.MoveObject(mobj, v);
                }
                //else if (mobj is tsm.Assembly)
                //{
                //    tsm.Assembly asm = mobj as tsm.Assembly;
                //    tsm.Part p = (asm.GetMainPart()) as tsm.Part;
                //    double z = ctp.TransformationMatrixToGlobal.Transform(p.GetReferenceLine(false)[0] as tsg.Point).Z;
                //    tsg.Vector v = new tsg.Vector(0, 0,
                //        ArkLib.Differents.CommonFunctions.ReturnCorrectDouble(textBox1.Text) - z);
                //    tsm.Operations.Operation.MoveObject(asm, v);
                //}

            }
            wph.SetCurrentTransformationPlane(ctp);
            model.CommitChanges();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (WrongNumber) return;
            //tsm.Model model = new tsm.Model();
            //if (model.GetConnectionStatus() == false) return;
            //tsm.WorkPlaneHandler wph = model.GetWorkPlaneHandler();
            //tsm.TransformationPlane tp = ArkLib.ark_Tekla.ark_New.TP_GLOBAL;
            //tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            //wph.SetCurrentTransformationPlane(tp);
            //tsm.UI.Picker picker = new tsm.UI.Picker();
            //tsg.Point p1 = picker.PickPoint();
            //p1.Z = Convert.ToDouble(textBox1.Text);
            //tsm.ControlPoint cp = new tsm.ControlPoint(new tsg.Point(ctp.TransformationMatrixToLocal.Transform(p1)));
            //cp.Insert();
            //wph.SetCurrentTransformationPlane(ctp);
            //model.CommitChanges();
        }

        public static tsm.ModelObjectEnumerator GetModelObjects
        {
            get
            {
                tsm.Model model = new tsm.Model();
                ui.ModelObjectSelector mos = new ui.ModelObjectSelector();
                return mos.GetSelectedObjects();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
            if (model.GetConnectionStatus() == false) return;
            tsm.WorkPlaneHandler wph = model.GetWorkPlaneHandler();
            tsm.TransformationPlane tp = ArkLib.ark_Tekla.Geometry.ark_New.TP_GLOBAL;
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            wph.SetCurrentTransformationPlane(tp);
            tsm.ModelObjectEnumerator moe = GetModelObjects;
            try
            {
                if (moe.GetSize() > 0)
                    while (moe.MoveNext())
                    {
                        //if (moe.Current is tsm.Beam)
                        //{
                        //    tsm.Beam b = moe.Current as tsm.Beam;
                        //    tsg.Point p1 = new tsg.Point(tp.TransformationMatrixToLocal.Transform(b.StartPoint));
                        //    tsg.Point p2 = new tsg.Point(tp.TransformationMatrixToLocal.Transform(b.EndPoint));
                        //    textBox1.Text = (p1.Z == p2.Z) ? p1.Z.ToString() : "Точки различны";
                        //    b.Modify();
                        //}
                        tsm.ModelObject mobj = moe.Current as tsm.ModelObject;
                        if (mobj is tsm.Part)
                        {
                            tsm.Part p = mobj as tsm.Part;
                            tsg.Point p1 = new tsg.Point(tp.TransformationMatrixToLocal.Transform(
                                p.GetReferenceLine(false)[0] as tsg.Point));
                            tsg.Point p2 = new tsg.Point(tp.TransformationMatrixToLocal.Transform(
                                p.GetReferenceLine(false)[1] as tsg.Point));
                            textBox1.Text = (p1.Z == p2.Z) ? p1.Z.ToString() : "Точки различны";
                        }
                    }
            }
            catch { }
            wph.SetCurrentTransformationPlane(ctp);
        }


        private void button4_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (WrongNumber) return;

            //    tsm.Model model = new tsm.Model();
            //    if (model.GetConnectionStatus() == false) return;
            //    tsm.WorkPlaneHandler wph = model.GetWorkPlaneHandler();
            //    tsm.TransformationPlane tp = ArkLib.ark_Tekla.ark_New.TP_GLOBAL;
            //    tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            //    wph.SetCurrentTransformationPlane(tp);

            //    tsm.ModelObjectEnumerator moe = ArkLib.Common.FastFunctions.GetModelObjects;

            //    tsm.UI.Picker picker = new tsm.UI.Picker();
            //    ArrayList p = picker.PickPoints(tsm.UI.Picker.PickPointEnum.PICK_TWO_POINTS);
            //    tsg.Point p1 = (tsg.Point)p[0];
            //    tsg.Point p2 = (tsg.Point)p[1];
            //    p1.Z = Convert.ToDouble(textBox1.Text);
            //    p2.Z = Convert.ToDouble(textBox1.Text);

            //    tsm.ControlLine cl = new tsm.ControlLine(
            //        new tsg.LineSegment(
            //            new tsg.Point(ctp.TransformationMatrixToLocal.Transform(p1)),
            //            new tsg.Point(ctp.TransformationMatrixToLocal.Transform(p2))), false);
            //    cl.Insert();

            //    akCom.FastFunctions.ModelCommitChanges();
            //    wph.SetCurrentTransformationPlane(ctp);
            //    model.CommitChanges();
            //}
            //catch { }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
            if (model.GetConnectionStatus() == false) return;
            tsm.WorkPlaneHandler wph = model.GetWorkPlaneHandler();
            tsm.TransformationPlane tp = ArkLib.ark_Tekla.Geometry.ark_New.TP_GLOBAL;
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            wph.SetCurrentTransformationPlane(tp);
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mos.GetSelectedObjects();
            while (moe.MoveNext())
            {
                tsm.ModelObject mobj = moe.Current as tsm.ModelObject;

                if (mobj is tsm.Part)
                {
                    tsm.Part p = mobj as tsm.Part;
                    double z = ctp.TransformationMatrixToGlobal.Transform(p.GetReferenceLine(false)[0] as tsg.Point).Z;
                    tsg.Vector v = new tsg.Vector(0, 0,
                        ArkLib.ark_Tekla.Differents.CommonFunctions.ReturnCorrectDouble(textBox1.Text) - z);
                    tsm.Operations.Operation.CopyObject(mobj, v);
                }
                wph.SetCurrentTransformationPlane(ctp);
                model.CommitChanges();
            }
        }
    }
}
