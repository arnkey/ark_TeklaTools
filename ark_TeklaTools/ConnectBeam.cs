﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Tekla.Structures.Model;
using Tekla.Structures.Catalogs;
using x = ArkLib.x_algoritms;

using Weld = ArkLib.ark_Tekla.MyAlgoritms.Plugins.Helper.Weld;

namespace ArkLib.ark_Tekla.MyAlgoritms.Plugins
{
    public class ConnectBeam
    {
        x.x_Profile.Type TypePrimary;
        x.x_Profile.Type TypeSecondary;

        ModelObject PrimaryBeam;
        ModelObject SecondaryBeam;

        public enum TypeConnection
        {
            Flange,
            Shear
        }

        public ConnectBeam(ModelObject Primary, ModelObject secondary, TypeConnection _tc, Weld w1)
        {
            PrimaryBeam = Primary;
            SecondaryBeam = secondary;
            TypePrimary = AnalyzeProfile(PrimaryBeam);
            TypeSecondary = AnalyzeProfile(SecondaryBeam);
            if (_tc == TypeConnection.Shear)
                ShearBeam();
            else
                new FlangeBeam(Primary, secondary, w1);
        }

        private x.x_Profile.Type AnalyzeProfile(ModelObject mobj)
        {
            return x.x_Profile.GetProfileType(mobj as Part);
        }

        
        void ShearBeam()
        {

        }
    }
}
