﻿/*using System;
using System.IO;
using System.Windows.Forms;
using TS = Tekla.Structures;

namespace ArkLib.ark_Tekla.Common
{   
    /// <summary>
    /// =====================================================================
    /// 26.11.2014 Исправлено:
    ///     Если на конце пути в xs_macros_directory не стояло "\" - вылетало
    /// =====================================================================
    /// ПРОВЕРИТЬ:
    ///     создавать ли папку modeling
    /// =====================================================================
    /// </summary>
    public static class macros
    {

        private static string xs_macros_directory = "";
        private static string str_begin = "namespace Tekla.Technology.Akit.UserScript\n{public class Script\n{public static void Run(Tekla.Technology.Akit.IScript akit)\n{\n";
        private static string str_end = "\n}\n}\n}";
        private static string _path = "modeling\\";
        private static string _name = "sdr_temp_macros";
        private static string strf = "";
        public static void WriteMacrosAndRun(string str)
        {
            TS.TeklaStructuresSettings.GetAdvancedOption("XS_MACRO_DIRECTORY", ref xs_macros_directory);
            string s = "";
            bool tr = false;
            s += xs_macros_directory.ToString();
            tr = s.Contains(";");
            if (tr)
            {

                string[] s0 = new string[1];
                s0 = xs_macros_directory.Split(new char[] { ';' });
                try
                {
                    for (int i = 0; i <= s0.Length - 1; i++)
                    {
                        if (s0[i] != null || s0[i] != "")
                        {
                            if (@s0[i].Substring(s0[i].Length - 1, 1) != "\\")
                                @s0[i] += "\\";
                            if (write((@s0[i] + @_path + _name), (@str_begin + @str + @str_end)))
                            {
                                i = s0.Length;
                                try
                                {
                                    File.Delete(strf + ".cs");
                                    File.Delete(strf + ".dll");
                                    File.Delete(strf + ".pdb");
                                }
                                catch { }
                            }
                            else
                            {
                                try
                                {
                                    File.Delete(strf + ".cs");
                                    File.Delete(strf + ".dll");
                                    File.Delete(strf + ".pdb");
                                }
                                catch { }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error!" + "\n" + ex.ToString());
                }
            }
            else
            {
                if (@xs_macros_directory.Substring(@xs_macros_directory.Length-1, 1) != "\\")
                    @xs_macros_directory += "\\";
                if (!write((@xs_macros_directory + @_path + @_name), (@str_begin + @str + @str_end))) MessageBox.Show("Error");
                try
                {
                    File.Delete(strf + ".cs");
                    File.Delete(strf + ".dll");
                    File.Delete(strf + ".pdb");
                }
                catch { }
            }

        }

        private static bool write(string @str, string _string)
        {
            StreamWriter _file = File.CreateText(str + ".cs");
            _file.WriteLine(_string);
            _file.Close();
            // Run Macro
            string ss = str.Replace(@"\", @"\\");
            strf = ss;
            return (Tekla.Structures.Model.Operations.Operation.RunMacro(_name + ".cs"));
        }
    }
}
*/