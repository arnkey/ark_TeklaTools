﻿using System.Collections.Generic;

using tsm = Tekla.Structures.Model;
using parser = ArkLib.ark_Tekla.Common.CommonFunctions;
using System;

namespace ArkLib.ark_Tekla.Geometry
{
    [Serializable]
    public class ark_Grid
    {
        //Координаты
        public List<double> X; 
        public List<double> Y; 
        public List<double> Z; 

        // Имена координат
        public List<string> n_X;
        public List<string> n_Y;
        public List<string> n_Z;

        public bool ReverseNameXY = false;

        #region Конструктор 

        public ark_Grid()
        {
            X = new List<double>();
            Y = new List<double>();
            Z = new List<double>();

            n_X = new List<string>();
            n_Y = new List<string>();
            n_Z = new List<string>();
        }

        public ark_Grid(ark_Grid ag)
        {
            X = new List<double>(ag.X);
            Y = new List<double>(ag.Y);
            Z = new List<double>(ag.Z);

            n_X = new List<string>(ag.n_X);
            n_Y = new List<string>(ag.n_Y);
            n_Z = new List<string>(ag.n_Z);
        }

        public ark_Grid(List<double> x, List<double> y, List<double> z)
        {
            X = new List<double>(x);
            Y = new List<double>(y);
            Z = new List<double>(z);

            SetNameXYZ();
        }

        public ark_Grid(List<double> x, List<double> y, List<double> z,
            List<string> nx, List<string> ny, List<string> nz)
        {
            X = new List<double>(x);
            Y = new List<double>(y);
            Z = new List<double>(z);

            n_X = new List<string>(nx);
            n_Y = new List<string>(ny);
            n_Z = new List<string>(nz);
        }

        #endregion

        public void SetXYZ(List<double> x, List<double> y, List<double> z)
        {
           SetX(x);
           SetY(y);
           SetZ(z);
        }
        public void SetXYZ(string x, string y, string z)
        {
            SetX(x);
            SetY(y);
            SetZ(z);
        }
        /// <summary>
        /// Должны быть забиты X Y Z, если false
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="nx"></param>
        /// <param name="ny"></param>
        /// <param name="nz"></param>
        public void SetNameXYZ(string x = "", string y = "", string z = "", bool nx = false, bool ny = false, bool nz = false)
        {
            n_X.Clear();
            n_Y.Clear();
            n_Z.Clear();
            if (nx == true)
            {
                List<string> _nx = parser.StringParse(x, ' ');
                foreach (string s in _nx)
                    n_X.Add(s);
            }
            else
            {
                int ix = X.Count;
                for (int i = 0; i < ix; i++)
                {
                    if (ReverseNameXY == false)
                        n_X.Add(parser.Digits()[i].ToString());
                    else
                        n_X.Add(parser.Letters()[i].ToUpper());
                }
            }
            if (ny == true)
            {
                List<string> _ny = parser.StringParse(y, ' ');
                foreach (string s in _ny)
                    n_Y.Add(s);
            }
            else
            {
                int ix = Y.Count;
                for (int i = 0; i < ix; i++)
                {
                    if (ReverseNameXY == true)
                        n_Y.Add(parser.Digits()[i].ToString());
                    else
                        n_Y.Add(parser.Letters()[i].ToUpper());
                }
            }
            if (nz == true)
            {
                List<string> _nz = parser.StringParse(z, ' ');
                foreach (string s in _nz)
                    n_Z.Add(s);
            }
            else
            {
                foreach (double _z in Z)
                {
                    double temp = _z / 1000;
                    n_Z.Add((temp >= 0 ? "+" + temp.ToString() : "-" + temp.ToString()));
                }
            }
        }

        #region X
        public void SetX(List<double> x)
        {
            X.Clear();
            foreach (double d in x)
                X.Add(d);
        }
        public void SetX(string x)
        {
            X.Clear();
            List<string> s = parser.StringParse(x, ' ');
            foreach (string s0 in s)
                X.Add(parser.ReturnCorrectDouble(s0));
        }
        #endregion
        #region Y
        public void SetY(List<double> y)
        {
            Y.Clear();
            foreach (double d in y)
                Y.Add(d);
        }
        public void SetY(string y)
        {
            Y.Clear();
            List<string> s = parser.StringParse(y, ' ');
            foreach (string s0 in s)
                Y.Add(parser.ReturnCorrectDouble(s0));
        }
        #endregion
        #region Z
        public void SetZ(List<double> z)
        {
            Z.Clear();
            foreach (double d in z)
                Z.Add(d);
        }
        public void SetZ(string z)
        {
            Z.Clear();
            List<string> s = parser.StringParse(z, ' ');
            foreach (string s0 in s)
                Z.Add(parser.ReturnCorrectDouble(s0));
        }
        #endregion
    }

    public static class ark_Grid_ToTekla
    {
        public static bool RUN(ark_Grid AG)
        {
            tsm.Grid gr = new tsm.Grid();
            gr.LabelX = parser.ListToString(AG.n_X);
            gr.LabelY = parser.ListToString(AG.n_Y);
            gr.LabelZ = parser.ListToString(AG.n_Z);
            gr.CoordinateX = parser.ListToString(AG.X);
            gr.CoordinateY = parser.ListToString(AG.Y);
            gr.CoordinateZ = parser.ListToString(AG.Z);
            gr.Name = "ArnKey's Grid";
            return gr.Insert();
        }
    }
}
