﻿
using System;
using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model;

namespace ArkLib.ark_Tekla.MyAlgoritms.Plugins
{
    public class x_Handrail
    {

        public bool Inserted = false;
        private Component HandRail = null;
        private Beam Beam = null;
        private string attributes = "standard";
        private bool NotModify = false;


        private void Default()
        {
            ComponentInput ci = new ComponentInput();
            ci.AddOneInputPosition(Beam.GetCenterLine(true)[0] as Point);
            ci.AddOneInputPosition(Beam.GetCenterLine(true)[1] as Point);
            HandRail = new Component(ci);
            HandRail.Name = "x_Handrail_Long";
            HandRail.Number = Component.PLUGIN_OBJECT_NUMBER;
            try { HandRail.LoadAttributesFromFile(attributes); }
            catch { }
            Inserted = HandRail.Insert();
        }

        public void Auto()
        {
            HandRail = new Component();
            HandRail.Name = "x_Handrail_Long";
            HandRail.Number = Component.PLUGIN_OBJECT_NUMBER;
            try { HandRail.LoadAttributesFromFile(attributes); }
            catch { }
            NotModify = true;
            double length = Distance.PointToPoint((Beam.GetCenterLine(true)[0] as Point), (Beam.GetCenterLine(true)[1] as Point));
            int count = (Convert.ToInt32(Math.Floor(length / 1000)) + 1);
            double width = 0; Beam.GetReportProperty("WIDTH", ref width);
            double height = 0; Beam.GetReportProperty("HEIGHT", ref height);
            ComponentInput ci = new ComponentInput();
            Point p1 = (Beam.GetCenterLine(true)[0] as Point);
            Point p2 = (Beam.GetCenterLine(true)[1] as Point);
            p1.Z += (height * 0.5);
            p2.Z += (height * 0.5);
            ci.AddOneInputPosition(p1);
            ci.AddOneInputPosition(p2);
            HandRail.SetComponentInput(ci);
            SetSideBeam(width);
            SetNumberColumn(count);
            Inserted = HandRail.Insert();
            ModelObjectEnumerator moe = HandRail.GetChildren();
            while (moe.MoveNext())
            {
                if (moe.Current is Beam && (moe.Current as Beam).Name == "Стойка")
                {
                    Connect(Beam, moe.Current);
                }
            }
        }

        private void Connect(ModelObject MainBeam, ModelObject Secondary)
        {
            Component cnt = new Component();
            cnt.Name = "x_Post_Shear_Plate";
            cnt.Number = -100000;
            try
            {
                cnt.LoadAttributesFromFile("standard");
            }
            catch
            {
            }
            ComponentInput ci = new ComponentInput();
            ci.AddInputObject(MainBeam);
            ci.AddInputObject(Secondary);
            cnt.Insert();
        }



        public x_Handrail(Beam _Beam, string _attributes = "standard")
        {
            Beam = _Beam;
            attributes = _attributes;
            Default();
        }

        public x_Handrail(Beam _Beam)
        {
            Beam = _Beam;
            Auto();
        }

        private void Modify()
        {
            if (!NotModify) HandRail.Modify();
        }


        public void SetNumberColumn(int i)
        {
                if (HandRail != null)
            {
                HandRail.SetAttribute("pst1_count", i);
                Modify();
            }
        }

        public void SetSide(int i)
        {
            if (HandRail != null)
            {
                HandRail.SetAttribute("pst1_mirr", i);
                Modify();
            }
        }

        //public void SetDX(double dx1, double dx2)
        //{
        //    Modify();
        //}
        public void SetSideBeam(double width)
        {
            if (HandRail != null)
            {
                HandRail.SetAttribute("pst1_middOffset", (width * 0.5));
                Modify();
            }
        }
        public void SetAttributes(string name)
        {
            if (HandRail != null)
            {
                HandRail.LoadAttributesFromFile(name);
                Modify();
            }
        }
    }
}
