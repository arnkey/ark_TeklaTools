﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Tekla.Structures.Model;
using x = ArkLib.x_algoritms.x_Profile;


namespace ArkLib.ark_Tekla.MyAlgoritms.Plugins
{
    public class ConnectBracing
    {
        public enum Type
        {
            ErectionWOBolts,
            ErectionWBolts,
            ShopWOBolts,
            ShopWBolts
        }

        public ConnectBracing(ModelObject Primary, List<ModelObject> Secondary, Type t)
        {
            Component cnt = new Component();
            cnt.Number = Component.PLUGIN_OBJECT_NUMBER;
            ComponentInput ci = new ComponentInput();

            ci.AddInputObject(Primary);
            foreach(ModelObject mobj in Secondary)
                ci.AddInputObject(mobj);

            x.Type TPB = AnalyzeProfile(Primary);
            if (t == Type.ErectionWOBolts)
            {
                if (TPB == x.Type.PL)
                {
                    cnt.Name = "x_Brace_End_Beam";
                    cnt.SetComponentInput(ci);
                    SetAttribute(ref cnt, t);
                    cnt.Insert();
                }
            }
        }


        // Для труб / уголков / двутавров
        x.Type AnalyzeProfile(ModelObject mobj)
        {
            return x.GetProfileType(mobj as Part);
        }

        void SetAttribute(ref Component cnt, Type t)
        {
            if (t == Type.ErectionWOBolts)
            {
                cnt.SetAttribute("b1_StrResult", 0);
                cnt.SetAttribute("w2_SizeAbove", 1);
                cnt.SetAttribute("w2_TypeAbove", 10);
                cnt.SetAttribute("w2_Shop", 0);
                cnt.SetAttribute("w1_SizeAbove", 1);
                cnt.SetAttribute("w1_TypeAbove", 10);
                cnt.SetAttribute("w1_Shop", 0);
                cnt.SetAttribute("b1_edgeLeft", 50);
                cnt.SetAttribute("b1_XdistList", 25);
                cnt.SetAttribute("b1_edgeRight", 50);
                cnt.SetAttribute("con_sysCoor", 0);
                cnt.SetAttribute("ft_type", 0);
            }

        }


    }
}
