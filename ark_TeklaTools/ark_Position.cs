﻿
using Tekla.Structures.Model;
using x = ArkLib.x_algoritms;

namespace ArkLib.ark_Tekla.Common
{
    public static class ark_Position
    {


        public static x.x_Profile.Type GetTypeProfile(ArkLib.MarkElements.ark_Profile ap)
        {
            Beam b = new Beam();
            ap.ReturnBeam(ref b);
            b.StartPoint = new Tekla.Structures.Geometry3d.Point(0, 0, 0);
            b.EndPoint = new Tekla.Structures.Geometry3d.Point(100, 0, 0);
            b.Insert();
            x.x_Profile.Type t = x.x_Profile.GetProfileType(b as Part);
            b.Delete();
            return t;
        }

        /// <summary>
        /// Общее положение
        /// </summary>
        /// <param name="type"></param>
        /// <param name="reverse"></param>
        /// <returns></returns>
        public static Position ReturnPosition(x.x_Profile.Type type, bool reverse = false)
        {
            //System.Windows.Forms.MessageBox.Show(type.ToString());
            Position pos = new Position();
            #region УГОЛОК
            if (type == x.x_Profile.Type.L)
            {
                pos.Plane = Position.PlaneEnum.RIGHT;
                pos.Rotation = Position.RotationEnum.BACK;
                pos.Depth = Position.DepthEnum.BEHIND;
                if (reverse == true)
                {
                    pos.Plane = Position.PlaneEnum.LEFT;
                    pos.Rotation = Position.RotationEnum.BELOW;
                    pos.Depth = Position.DepthEnum.BEHIND;
                }
            }
            #endregion
            #region Труба, кругляк
            if (type == x.x_Profile.Type.D || type == x.x_Profile.Type.RHS)
            {
                pos.Depth = Position.DepthEnum.MIDDLE;
                pos.Rotation = Position.RotationEnum.TOP;
                pos.Plane = Position.PlaneEnum.MIDDLE;
            }
            #endregion
            #region Двутавр - балка
            if (type == x.x_Profile.Type.I)
            {
                pos.Plane = Position.PlaneEnum.MIDDLE;
                pos.Rotation = Position.RotationEnum.TOP;
                pos.Depth = Position.DepthEnum.BEHIND;
                if (reverse)
                {
                    pos.Depth = Position.DepthEnum.MIDDLE;
                }
            }
            #endregion
            #region Швеллер
            if (type == x.x_Profile.Type.U)
            {
                pos.Plane = Position.PlaneEnum.RIGHT;
                pos.Rotation = Position.RotationEnum.TOP;
                pos.Depth = Position.DepthEnum.BEHIND;
            }
            #endregion
            return pos;
        }


        /// <summary>
        /// Для лестницы ( уголок )
        /// </summary>
        /// <param name="reverse"></param>
        /// <returns></returns>
        public static Position ReturnPositionLadder_L(bool reverse = false)
        {
            Position pos = new Position();

            pos.Plane = Position.PlaneEnum.RIGHT;
            pos.Rotation = Position.RotationEnum.TOP;
            pos.Depth = Position.DepthEnum.FRONT;
            if (reverse == true)
            {
                pos.Plane = Position.PlaneEnum.LEFT;
                pos.Rotation = Position.RotationEnum.FRONT;
                pos.Depth = Position.DepthEnum.FRONT;
            }

            return pos;
        }
    }
}
