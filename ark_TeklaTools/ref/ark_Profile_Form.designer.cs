﻿namespace ArkLib
{
    partial class ark_Profile_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_CANCEL = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmb_class = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_profile = new System.Windows.Forms.TextBox();
            this.txt_material = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_plane = new System.Windows.Forms.TextBox();
            this.cmb_plane = new System.Windows.Forms.ComboBox();
            this.txt_depth = new System.Windows.Forms.TextBox();
            this.cmb_rotation = new System.Windows.Forms.ComboBox();
            this.txt_rotation = new System.Windows.Forms.TextBox();
            this.cmb_depth = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_asm_number = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_part_number = new System.Windows.Forms.TextBox();
            this.txt_asm_prefix = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_part_prefix = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ep_M = new System.Windows.Forms.TextBox();
            this.ep_N = new System.Windows.Forms.TextBox();
            this.ep_Q = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.sp_M = new System.Windows.Forms.TextBox();
            this.sp_N = new System.Windows.Forms.TextBox();
            this.sp_Q = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_OK
            // 
            this.btn_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_OK.Location = new System.Drawing.Point(4, 211);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(75, 23);
            this.btn_OK.TabIndex = 15;
            this.btn_OK.Text = "Ok";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // btn_CANCEL
            // 
            this.btn_CANCEL.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_CANCEL.Location = new System.Drawing.Point(237, 211);
            this.btn_CANCEL.Name = "btn_CANCEL";
            this.btn_CANCEL.Size = new System.Drawing.Size(75, 23);
            this.btn_CANCEL.TabIndex = 16;
            this.btn_CANCEL.Text = "Cancel";
            this.btn_CANCEL.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(316, 209);
            this.tabControl1.TabIndex = 20;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(308, 183);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Основные";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txt_name);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.cmb_class);
            this.groupBox4.Location = new System.Drawing.Point(6, 93);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(294, 80);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(50, 50);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(238, 20);
            this.txt_name.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Имя";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Класс";
            // 
            // cmb_class
            // 
            this.cmb_class.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_class.FormattingEnabled = true;
            this.cmb_class.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14"});
            this.cmb_class.Location = new System.Drawing.Point(50, 19);
            this.cmb_class.Name = "cmb_class";
            this.cmb_class.Size = new System.Drawing.Size(46, 21);
            this.cmb_class.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.txt_profile);
            this.groupBox3.Controls.Add(this.txt_material);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(294, 83);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Профиль";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(224, 49);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Выбрать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(224, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Выбрать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txt_profile
            // 
            this.txt_profile.Location = new System.Drawing.Point(69, 25);
            this.txt_profile.Name = "txt_profile";
            this.txt_profile.Size = new System.Drawing.Size(149, 20);
            this.txt_profile.TabIndex = 0;
            // 
            // txt_material
            // 
            this.txt_material.Location = new System.Drawing.Point(69, 51);
            this.txt_material.Name = "txt_material";
            this.txt_material.Size = new System.Drawing.Size(149, 20);
            this.txt_material.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Материал";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(308, 183);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Положение";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_plane);
            this.groupBox1.Controls.Add(this.cmb_plane);
            this.groupBox1.Controls.Add(this.txt_depth);
            this.groupBox1.Controls.Add(this.cmb_rotation);
            this.groupBox1.Controls.Add(this.txt_rotation);
            this.groupBox1.Controls.Add(this.cmb_depth);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(294, 103);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "По глубине";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Поворот";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "На плоскости:";
            // 
            // txt_plane
            // 
            this.txt_plane.Location = new System.Drawing.Point(239, 20);
            this.txt_plane.Name = "txt_plane";
            this.txt_plane.Size = new System.Drawing.Size(49, 20);
            this.txt_plane.TabIndex = 7;
            // 
            // cmb_plane
            // 
            this.cmb_plane.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_plane.FormattingEnabled = true;
            this.cmb_plane.Items.AddRange(new object[] {
            "MIDDLE",
            "LEFT",
            "RIGHT"});
            this.cmb_plane.Location = new System.Drawing.Point(112, 19);
            this.cmb_plane.Name = "cmb_plane";
            this.cmb_plane.Size = new System.Drawing.Size(121, 21);
            this.cmb_plane.TabIndex = 2;
            // 
            // txt_depth
            // 
            this.txt_depth.Location = new System.Drawing.Point(239, 74);
            this.txt_depth.Name = "txt_depth";
            this.txt_depth.Size = new System.Drawing.Size(49, 20);
            this.txt_depth.TabIndex = 9;
            // 
            // cmb_rotation
            // 
            this.cmb_rotation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_rotation.FormattingEnabled = true;
            this.cmb_rotation.Items.AddRange(new object[] {
            "BACK",
            "BELOW",
            "FRONT",
            "TOP"});
            this.cmb_rotation.Location = new System.Drawing.Point(112, 46);
            this.cmb_rotation.Name = "cmb_rotation";
            this.cmb_rotation.Size = new System.Drawing.Size(121, 21);
            this.cmb_rotation.TabIndex = 3;
            // 
            // txt_rotation
            // 
            this.txt_rotation.Location = new System.Drawing.Point(239, 46);
            this.txt_rotation.Name = "txt_rotation";
            this.txt_rotation.Size = new System.Drawing.Size(49, 20);
            this.txt_rotation.TabIndex = 8;
            // 
            // cmb_depth
            // 
            this.cmb_depth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_depth.FormattingEnabled = true;
            this.cmb_depth.Items.AddRange(new object[] {
            "BEHIND",
            "FRONT",
            "MIDDLE"});
            this.cmb_depth.Location = new System.Drawing.Point(112, 73);
            this.cmb_depth.Name = "cmb_depth";
            this.cmb_depth.Size = new System.Drawing.Size(121, 21);
            this.cmb_depth.TabIndex = 4;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(308, 183);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Нумерация";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_asm_number);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txt_part_number);
            this.groupBox2.Controls.Add(this.txt_asm_prefix);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txt_part_prefix);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(297, 87);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            // 
            // txt_asm_number
            // 
            this.txt_asm_number.Location = new System.Drawing.Point(195, 59);
            this.txt_asm_number.Name = "txt_asm_number";
            this.txt_asm_number.Size = new System.Drawing.Size(96, 20);
            this.txt_asm_number.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(192, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Начальный номер";
            // 
            // txt_part_number
            // 
            this.txt_part_number.Location = new System.Drawing.Point(195, 35);
            this.txt_part_number.Name = "txt_part_number";
            this.txt_part_number.Size = new System.Drawing.Size(96, 20);
            this.txt_part_number.TabIndex = 15;
            // 
            // txt_asm_prefix
            // 
            this.txt_asm_prefix.Location = new System.Drawing.Point(78, 59);
            this.txt_asm_prefix.Name = "txt_asm_prefix";
            this.txt_asm_prefix.Size = new System.Drawing.Size(92, 20);
            this.txt_asm_prefix.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(75, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Префикс";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Сборка";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Деталь";
            // 
            // txt_part_prefix
            // 
            this.txt_part_prefix.Location = new System.Drawing.Point(78, 35);
            this.txt_part_prefix.Name = "txt_part_prefix";
            this.txt_part_prefix.Size = new System.Drawing.Size(92, 20);
            this.txt_part_prefix.TabIndex = 10;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(308, 183);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Нагрузки";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ep_M);
            this.groupBox6.Controls.Add(this.ep_N);
            this.groupBox6.Controls.Add(this.ep_Q);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Location = new System.Drawing.Point(167, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(133, 102);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Конечная точка";
            // 
            // ep_M
            // 
            this.ep_M.Location = new System.Drawing.Point(56, 71);
            this.ep_M.Name = "ep_M";
            this.ep_M.Size = new System.Drawing.Size(71, 20);
            this.ep_M.TabIndex = 5;
            // 
            // ep_N
            // 
            this.ep_N.Location = new System.Drawing.Point(56, 45);
            this.ep_N.Name = "ep_N";
            this.ep_N.Size = new System.Drawing.Size(71, 20);
            this.ep_N.TabIndex = 4;
            // 
            // ep_Q
            // 
            this.ep_Q.Location = new System.Drawing.Point(56, 19);
            this.ep_Q.Name = "ep_Q";
            this.ep_Q.Size = new System.Drawing.Size(71, 20);
            this.ep_Q.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 74);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "M";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "N";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Q";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.sp_M);
            this.groupBox5.Controls.Add(this.sp_N);
            this.groupBox5.Controls.Add(this.sp_Q);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Location = new System.Drawing.Point(8, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(133, 102);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Начальная точка";
            // 
            // sp_M
            // 
            this.sp_M.Location = new System.Drawing.Point(56, 71);
            this.sp_M.Name = "sp_M";
            this.sp_M.Size = new System.Drawing.Size(71, 20);
            this.sp_M.TabIndex = 5;
            // 
            // sp_N
            // 
            this.sp_N.Location = new System.Drawing.Point(56, 45);
            this.sp_N.Name = "sp_N";
            this.sp_N.Size = new System.Drawing.Size(71, 20);
            this.sp_N.TabIndex = 4;
            // 
            // sp_Q
            // 
            this.sp_Q.Location = new System.Drawing.Point(56, 19);
            this.sp_Q.Name = "sp_Q";
            this.sp_Q.Size = new System.Drawing.Size(71, 20);
            this.sp_Q.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "M";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "N";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Q";
            // 
            // ark_Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 242);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btn_CANCEL);
            this.Controls.Add(this.btn_OK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ark_Profile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Свойства балки";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.EditProperties_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Button btn_CANCEL;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmb_class;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_profile;
        private System.Windows.Forms.TextBox txt_material;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_plane;
        private System.Windows.Forms.ComboBox cmb_plane;
        private System.Windows.Forms.TextBox txt_depth;
        private System.Windows.Forms.ComboBox cmb_rotation;
        private System.Windows.Forms.TextBox txt_rotation;
        private System.Windows.Forms.ComboBox cmb_depth;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_asm_number;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_part_number;
        private System.Windows.Forms.TextBox txt_asm_prefix;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_part_prefix;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox ep_M;
        private System.Windows.Forms.TextBox ep_N;
        private System.Windows.Forms.TextBox ep_Q;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox sp_M;
        private System.Windows.Forms.TextBox sp_N;
        private System.Windows.Forms.TextBox sp_Q;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
    }
}