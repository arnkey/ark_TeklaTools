﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using tsm =  Tekla.Structures.Model;
using tsc = Tekla.Structures.Catalogs;
using g = ArkLib.ark_Tekla.Common.CommonFunctions;
using tdiui = Tekla.Structures.Dialog.UIControls;
using ArkLib.ark_Tekla.Catalogs;
using m = ArkLib.MarkElements;
using ArkLib.ark_Tekla;

namespace ArkLib
{
    public partial class ark_Profile_Form : Form
    {
        public m.ark_Profile ark_profile = new m.ark_Profile();

        

        private tsm.Position pos = new tsm.Position();

        public ark_Profile_Form()
        {
            InitializeComponent();
        }
        public ark_Profile_Form(m.ark_Profile mo)
        {
            try
            {
                InitializeComponent();
                ark_profile = mo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void EditProperties_Load(object sender, EventArgs e)
        {
            try
            {
                txt_profile.Text = ark_profile.Profile;
                txt_material.Text = ark_profile.Material;
                pos = ark_profile.GetPosition;
                cmb_plane.SelectedItem = pos.Plane.ToString();
                txt_plane.Text = pos.PlaneOffset.ToString();
                cmb_rotation.SelectedItem = pos.Rotation.ToString();
                txt_rotation.Text = pos.RotationOffset.ToString();
                cmb_depth.SelectedItem = pos.Depth.ToString();
                txt_depth.Text = pos.DepthOffset.ToString();
                txt_part_prefix.Text = ark_profile.PartPrefix;
                txt_part_number.Text = ark_profile.PartNumber.ToString();
                txt_asm_prefix.Text = ark_profile.AssemblyPrefix;
                txt_asm_number.Text = ark_profile.AssemblyNumber.ToString();
                cmb_class.SelectedItem = ark_profile.Color;
                txt_name.Text = ark_profile.ThisName;
            }
            catch 
            {
                //MessageBox.Show(ex.ToString());
            }
            if (ark_CatalogTekla.Materials == null || ark_CatalogTekla.Profiles == null)
                ark_CatalogTekla.Load();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                ark_profile.Profile = txt_profile.Text;
                ark_profile.Material = txt_material.Text;
                #region POSITION OF BEAM
                switch (cmb_plane.SelectedItem.ToString())
                {
                    case "MIDDLE":
                        pos.Plane = tsm.Position.PlaneEnum.MIDDLE;
                        break;
                    case "LEFT":
                        pos.Plane = tsm.Position.PlaneEnum.LEFT;
                        break;
                    case "RIGHT":
                        pos.Plane = tsm.Position.PlaneEnum.RIGHT;
                        break;
                }
                pos.PlaneOffset = g.ReturnCorrectDouble(txt_plane.Text);
                switch (cmb_rotation.SelectedItem.ToString())
                {
                    case "BACK":
                        pos.Rotation = tsm.Position.RotationEnum.BACK;
                        break;
                    case "BELOW":
                        pos.Rotation = tsm.Position.RotationEnum.BELOW;
                        break;
                    case "FRONT":
                        pos.Rotation = tsm.Position.RotationEnum.FRONT;
                        break;
                    case "TOP":
                        pos.Rotation = tsm.Position.RotationEnum.TOP;
                        break;
                }
                pos.RotationOffset = g.ReturnCorrectDouble(txt_rotation.Text);
                switch (cmb_depth.SelectedItem.ToString())
                {
                    case "BEHIND":
                        pos.Depth = tsm.Position.DepthEnum.BEHIND;
                        break;
                    case "FRONT":
                        pos.Depth = tsm.Position.DepthEnum.FRONT;
                        break;
                    case "MIDDLE":
                        pos.Depth = tsm.Position.DepthEnum.MIDDLE;
                        break;
                }
                pos.DepthOffset = g.ReturnCorrectDouble(txt_depth.Text);
                #endregion
                ark_profile.GetPosition = pos;
                #region Numeration
                ark_profile.PartPrefix = txt_part_prefix.Text;
                ark_profile.PartNumber = Convert.ToInt32(txt_part_number.Text);
                ark_profile.AssemblyPrefix = txt_asm_prefix.Text;
                ark_profile.AssemblyNumber = Convert.ToInt32(txt_asm_number.Text);
                #endregion
                ark_profile.ThisName = txt_name.Text;
                ark_profile.Color = cmb_class.SelectedItem.ToString();

                #region LOAD
                double q1 = 0.0; double q2 = 0.0; 
                double n1 = 0.0; double n2 = 0.0; 
                double m1 = 0.0; double m2 = 0.0;

                try{ q1 = g.ReturnCorrectDouble(sp_Q.Text); }
                catch { }
                try { q2 = g.ReturnCorrectDouble(ep_Q.Text); }
                catch { }

                try { n1 = g.ReturnCorrectDouble(sp_N.Text); }
                catch { }
                try { n2 = g.ReturnCorrectDouble(ep_N.Text); }
                catch { }

                try { m1 = g.ReturnCorrectDouble(sp_M.Text); }
                catch { }
                try { m2 = g.ReturnCorrectDouble(ep_M.Text); }
                catch { }
                #endregion
                m.ark_ProfileLoads l = new m.ark_ProfileLoads(q1, n1, m1, q2, n2, m2);
                ark_profile.Loads = l;

            }
            catch (Exception Exception) { MessageBox.Show(Exception.ToString()); }
        }

        private void txt_rotation_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tdiui.ProfileSelectionForm psf = new tdiui.ProfileSelectionForm(ark_CatalogTekla.Profiles.Item1, ark_CatalogTekla.Profiles.Item2);

            System.Windows.Forms.DialogResult DR = psf.ShowDialog();

            if (DR == System.Windows.Forms.DialogResult.OK || DR == System.Windows.Forms.DialogResult.Yes)
            {
                txt_profile.Text = psf.SelectedProfile;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tdiui.MaterialSelectionForm msf = new tdiui.MaterialSelectionForm(ark_CatalogTekla.Materials);
            System.Windows.Forms.DialogResult DR = msf.ShowDialog();

            if (DR == System.Windows.Forms.DialogResult.OK || DR == System.Windows.Forms.DialogResult.Yes)
            {
                txt_material.Text = msf.SelectedMaterial;
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            tdiui.ProfileSelectionForm psf = new tdiui.ProfileSelectionForm(ark_CatalogTekla.Profiles.Item1, ark_CatalogTekla.Profiles.Item2);

            System.Windows.Forms.DialogResult DR = psf.ShowDialog();

            if (DR == System.Windows.Forms.DialogResult.OK || DR == System.Windows.Forms.DialogResult.Yes)
            {
                txt_profile.Text = psf.SelectedProfile;
            }
        }

        
    }
}
