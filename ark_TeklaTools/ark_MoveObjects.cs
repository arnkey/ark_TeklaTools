﻿using ArkLib.ark_Tekla.Common;
//using org.mariuszgromada.math.mxparser;
using System;
/*using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using mx = org.mariuszgromada.math.mxparser;
//using mx1 = org.mariuszgromada.math.mxparser;


using sf = ArkLib.ark_Tekla.Common.StandardFunctionsTekla;

#warning ПОЧИНИТЬ МАТЕМАТИКУ

namespace iark_MoveObjects
{
    public partial class Form1 : Form
    {
        public enum TypeD
        {
            dx,
            dy,
            dz
        }

        public Form1()
        {
            InitializeComponent();
        }


        void Default(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Q)
            {
                if (sender == numericUpDown1)
                    numericUpDown1.Value = 0;
                else if (sender == textBox1)
                {
                    textBox1.Text = "0";
                }
                else
                {
                    numericUpDown1.Value = 0;
                    textBox1.Text = "0";
                }
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < textBox1.Text.Length; i++)
            {
                char chr = Convert.ToChar(textBox1.Text.Substring(i, 1));

                if (!(char.IsDigit(chr)))
                {
                    try
                    {
                        textBox1.Text = textBox1.Text.Remove(i, 1);
                        textBox1.SelectionStart = textBox1.Text.Length;
                    }
                    catch { }
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // зеленый фон , если не ноль
            // копировать если более нуля 
            this.TopMost = true;
            #region Присвоение всем элементам нажатие клавиши Q для сброса значение на 0
            foreach (Control c in this.Controls)
            {
                c.KeyUp += Default;
            }
            #endregion
        }

        TypeD Types;

        private void button4_Click(object sender, EventArgs e)
        {
            string temp = textBox1.Text;
            if (temp == "") return;

            if (temp.StartsWith("-1*("))
            {
                temp = temp.Remove(0, 4);
                temp = temp.Substring(0, temp.Length - 1);
                textBox1.Text = temp;
            }
            else
            {
                textBox1.Text = "-1*(" + temp + ")";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _Math();
            Types = TypeD.dx;
            Core();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _Math();
            Types = TypeD.dy;
            Core();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _Math();
            Types = TypeD.dz;
            Core();
        }

        void _Math()
        {
            //textBox1.Text = new Expression(textBox1.Text).calculate().ToString();
        }

        void Core()
        {
            double di = Convert.ToDouble(textBox1.Text);
            string types = "";
            if (Types == TypeD.dx) types = "dx";
            else if (Types == TypeD.dy) types = "dy";
            else if (Types == TypeD.dz) types = "dz";
            if (numericUpDown1.Value > 0)
            {
                sf.Copy(types, numericUpDown1.Value.ToString(), di.ToString());
            }
            else
            {
                sf.Move(types, di.ToString());
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > 0)
            {
                numericUpDown1.BackColor = System.Drawing.Color.Blue;
                numericUpDown1.ForeColor = System.Drawing.Color.White;
            }
            else if (numericUpDown1.Value == 0)
            {
                numericUpDown1.BackColor = Color.White;
                numericUpDown1.ForeColor = System.Drawing.Color.Black;
            }
        }

    }
}
*/