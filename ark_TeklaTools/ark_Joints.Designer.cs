﻿namespace ark_TeklaTools
{
    partial class ark_Joints
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btn_Beam_Modify = new System.Windows.Forms.Button();
            this.Настройки = new System.Windows.Forms.GroupBox();
            this.rb_FlangeBeam_Erection = new System.Windows.Forms.RadioButton();
            this.rb_FlangeBeam_Shop = new System.Windows.Forms.RadioButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_Modify = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_swb = new System.Windows.Forms.RadioButton();
            this.rb_swob = new System.Windows.Forms.RadioButton();
            this.rb_ewb = new System.Windows.Forms.RadioButton();
            this.rb_ewob = new System.Windows.Forms.RadioButton();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.Настройки.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(174, 293);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btn_Beam_Modify);
            this.tabPage1.Controls.Add(this.Настройки);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(166, 267);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Балка-Балка";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btn_Beam_Modify
            // 
//            this.btn_Beam_Modify.BackgroundImage = global::ark_TeklaTools.Properties.Resources.sweebr_pos_start_fee;
            this.btn_Beam_Modify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Beam_Modify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Beam_Modify.Location = new System.Drawing.Point(52, 209);
            this.btn_Beam_Modify.Name = "btn_Beam_Modify";
            this.btn_Beam_Modify.Size = new System.Drawing.Size(54, 50);
            this.btn_Beam_Modify.TabIndex = 6;
            this.btn_Beam_Modify.UseVisualStyleBackColor = true;
            this.btn_Beam_Modify.Click += new System.EventHandler(this.btn_Beam_Modify_Click);
            // 
            // Настройки
            // 
            this.Настройки.Controls.Add(this.rb_FlangeBeam_Erection);
            this.Настройки.Controls.Add(this.rb_FlangeBeam_Shop);
            this.Настройки.Location = new System.Drawing.Point(6, 6);
            this.Настройки.Name = "Настройки";
            this.Настройки.Size = new System.Drawing.Size(145, 73);
            this.Настройки.TabIndex = 2;
            this.Настройки.TabStop = false;
            this.Настройки.Text = "groupBox2";
            // 
            // rb_FlangeBeam_Erection
            // 
            this.rb_FlangeBeam_Erection.AutoSize = true;
            this.rb_FlangeBeam_Erection.Location = new System.Drawing.Point(6, 42);
            this.rb_FlangeBeam_Erection.Name = "rb_FlangeBeam_Erection";
            this.rb_FlangeBeam_Erection.Size = new System.Drawing.Size(133, 17);
            this.rb_FlangeBeam_Erection.TabIndex = 3;
            this.rb_FlangeBeam_Erection.TabStop = true;
            this.rb_FlangeBeam_Erection.Text = "Жесткий сварной (м)";
            this.rb_FlangeBeam_Erection.UseVisualStyleBackColor = true;
            // 
            // rb_FlangeBeam_Shop
            // 
            this.rb_FlangeBeam_Shop.AutoSize = true;
            this.rb_FlangeBeam_Shop.Checked = true;
            this.rb_FlangeBeam_Shop.Location = new System.Drawing.Point(6, 19);
            this.rb_FlangeBeam_Shop.Name = "rb_FlangeBeam_Shop";
            this.rb_FlangeBeam_Shop.Size = new System.Drawing.Size(131, 17);
            this.rb_FlangeBeam_Shop.TabIndex = 2;
            this.rb_FlangeBeam_Shop.TabStop = true;
            this.rb_FlangeBeam_Shop.Text = "Жесткий сварной (з)";
            this.rb_FlangeBeam_Shop.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_Modify);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(166, 267);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Связь";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_Modify
            // 
  //          this.btn_Modify.BackgroundImage = global::ark_TeklaTools.Properties.Resources.sweebr_pos_start_fee;
            this.btn_Modify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Modify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Modify.Location = new System.Drawing.Point(59, 128);
            this.btn_Modify.Name = "btn_Modify";
            this.btn_Modify.Size = new System.Drawing.Size(50, 50);
            this.btn_Modify.TabIndex = 5;
            this.btn_Modify.UseVisualStyleBackColor = true;
            this.btn_Modify.Click += new System.EventHandler(this.btn_Modify_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_swb);
            this.groupBox1.Controls.Add(this.rb_swob);
            this.groupBox1.Controls.Add(this.rb_ewb);
            this.groupBox1.Controls.Add(this.rb_ewob);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(160, 116);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройки";
            // 
            // rb_swb
            // 
            this.rb_swb.AutoSize = true;
            this.rb_swb.Location = new System.Drawing.Point(6, 88);
            this.rb_swb.Name = "rb_swb";
            this.rb_swb.Size = new System.Drawing.Size(135, 17);
            this.rb_swb.TabIndex = 7;
            this.rb_swb.TabStop = true;
            this.rb_swb.Text = "Заводской с болтами";
            this.rb_swb.UseVisualStyleBackColor = true;
            // 
            // rb_swob
            // 
            this.rb_swob.AutoSize = true;
            this.rb_swob.Location = new System.Drawing.Point(6, 65);
            this.rb_swob.Name = "rb_swob";
            this.rb_swob.Size = new System.Drawing.Size(139, 17);
            this.rb_swob.TabIndex = 6;
            this.rb_swob.TabStop = true;
            this.rb_swob.Text = "Заводской без болтов";
            this.rb_swob.UseVisualStyleBackColor = true;
            // 
            // rb_ewb
            // 
            this.rb_ewb.AutoSize = true;
            this.rb_ewb.Location = new System.Drawing.Point(6, 42);
            this.rb_ewb.Name = "rb_ewb";
            this.rb_ewb.Size = new System.Drawing.Size(140, 17);
            this.rb_ewb.TabIndex = 5;
            this.rb_ewb.TabStop = true;
            this.rb_ewb.Text = "Монтажный с болтами";
            this.rb_ewb.UseVisualStyleBackColor = true;
            // 
            // rb_ewob
            // 
            this.rb_ewob.AutoSize = true;
            this.rb_ewob.Checked = true;
            this.rb_ewob.Location = new System.Drawing.Point(6, 19);
            this.rb_ewob.Name = "rb_ewob";
            this.rb_ewob.Size = new System.Drawing.Size(144, 17);
            this.rb_ewob.TabIndex = 4;
            this.rb_ewob.TabStop = true;
            this.rb_ewob.Text = "Монтажный без болтов";
            this.rb_ewob.UseVisualStyleBackColor = true;
            // 
            // ark_Joints
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(174, 293);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ark_Joints";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ark_Joints";
            this.Load += new System.EventHandler(this.ark_Joints_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.Настройки.ResumeLayout(false);
            this.Настройки.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_Modify;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb_swb;
        private System.Windows.Forms.RadioButton rb_swob;
        private System.Windows.Forms.RadioButton rb_ewb;
        private System.Windows.Forms.RadioButton rb_ewob;
        private System.Windows.Forms.Button btn_Beam_Modify;
        private System.Windows.Forms.GroupBox Настройки;
        private System.Windows.Forms.RadioButton rb_FlangeBeam_Erection;
        private System.Windows.Forms.RadioButton rb_FlangeBeam_Shop;

    }
}