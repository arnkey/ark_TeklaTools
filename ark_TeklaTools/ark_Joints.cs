﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using Tekla.Structures.Model;
using ui = Tekla.Structures.Model.UI;
using ArkLib.ark_Tekla.MyAlgoritms.Plugins;

namespace ark_TeklaTools
{
    public partial class ark_Joints : Form
    {
        public ark_Joints()
        {
            InitializeComponent();
        }

        private void btn_Modify_Click(object sender, EventArgs e)
        {
            Model model = new Model();
            if (model.GetConnectionStatus() == false) return;

            ConnectBracing.Type t = ConnectBracing.Type.ErectionWOBolts;

            if (rb_ewob.Checked) t = ConnectBracing.Type.ErectionWOBolts;
            if (rb_ewb.Checked) t = ConnectBracing.Type.ErectionWBolts;
            if (rb_swob.Checked) t = ConnectBracing.Type.ShopWOBolts;
            if (rb_swb.Checked) t = ConnectBracing.Type.ShopWBolts;

            while (true)
            {
                try
                {
                    ModelObject mobj = null;
                    List<ModelObject> mobj2 = new List<ModelObject>();
                    Helper.PickObjects(ref mobj, ref mobj2);
                    ConnectBracing CB = new ConnectBracing(mobj, mobj2, t);
                }
                catch 
                {
                    break;
                }
            }
            model.CommitChanges();
        }

        private void ark_Joints_Load(object sender, EventArgs e)
        {

        }

        private void btn_Beam_Modify_Click(object sender, EventArgs e)
        {
            Model model = new Model();
            if (model.GetConnectionStatus() == false) return;

            ConnectBeam.TypeConnection t = ConnectBeam.TypeConnection.Flange;
            Helper.Weld Weld = Helper.Weld.Shop;

            if (rb_FlangeBeam_Shop.Checked || rb_FlangeBeam_Erection.Checked) t = ConnectBeam.TypeConnection.Flange;
            if (rb_FlangeBeam_Shop.Checked) Weld = Helper.Weld.Shop;
            if (rb_FlangeBeam_Erection.Checked) Weld = Helper.Weld.Erection;

            ui.ModelObjectSelector mos = new ui.ModelObjectSelector();
            ModelObjectEnumerator moe = mos.GetSelectedObjects();
            //while (moe.MoveNext())
            //{
            //    if (moe.Current is Component)
            //    {
            //        Component cp = moe.Current as Component;
            //        MessageBox.Show(cp.Name + " " + cp.Number);
            //    }
            //}
            //return;

            //while (true)
            //{
                try
                {
                    ModelObject mobj = null;
                    ModelObject mobj2 = null;
                    Helper.PickObjects(ref mobj, ref mobj2);
                    if (mobj != null && mobj2 != null)
                    {
                        ConnectBeam CB = new ConnectBeam(mobj, mobj2, t, Weld);
                    }
                    //else
                    //    break;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            //}
            model.CommitChanges();
        }



    }
}
