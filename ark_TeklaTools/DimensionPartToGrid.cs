﻿using System;
using System.Collections.Generic;
using Tekla.Structures.Model;
using ui = Tekla.Structures.Model.UI;
using Tekla.Structures;
using tsg = Tekla.Structures.Geometry3d;

using cs = cs_net_lib;

namespace ark_TeklaTools
{
	public class DimensionPartToGrid
	{

		Model model = new Model();

		WorkPlaneHandler wph;
		TransformationPlane tpCurrent;
		TransformationPlane tpGlobal;
		TransformationPlane tpView;

		List<tsg.Point> GetPointsFromGrid(ModelObjectEnumerator grid)
		{
			List<tsg.Point> points = new List<tsg.Point>();
			while (grid.MoveNext())
			{
				//tsg.Point point = (grid.Current as GridPlane).Plane.Origin;

				points.Add((grid.Current as GridPlane).Plane.Origin);
			}
			return points;
		}


		List<tsg.Point> GetPointsFromSelected()
		{
			List<tsg.Point> points = new List<tsg.Point>();
			ModelObjectEnumerator moe = new ui.ModelObjectSelector().GetSelectedObjects();
			while (moe.MoveNext())
			{
				if (moe.Current is Part)
				{
					Part p = moe.Current as Part;
					foreach (var fp in p.GetReferenceLine(false))
					{
						points.Add(fp as tsg.Point);
					}
				}
			}
			return points;
		}

		List<tsg.Point> GetMinMaxPointXY4(List<tsg.Point> points)
		{
			tsg.Point minX = new tsg.Point(double.MaxValue, 0, 0);
			tsg.Point maxX = new tsg.Point(double.MinValue, 0, 0);

			double distMinX = double.MaxValue;
			double distMaxX = double.MinValue;

			double distMinY = double.MaxValue;
			double distMaxY = double.MinValue;

			tsg.Point minY = new tsg.Point(0, double.MaxValue, 0);
			tsg.Point maxY = new tsg.Point(0, double.MinValue, 0);

			foreach (tsg.Point p in points)
			{
				double dist = tsg.Distance.PointToPoint(new tsg.Point(0, 0, 0), p);
				if (dist > distMaxX)
				{
					maxX = new tsg.Point(p.X, p.Y, p.Z);
					distMaxX = dist;
				}
				if (dist < distMinX)
				{
					minX = new tsg.Point(p.X, p.Y, p.Z);
					distMinX = dist;
				}

				if (dist > distMaxY)
				{
					maxY = new tsg.Point(p.X, p.Y, p.Z);
					distMaxY = dist;
				}
				if (dist < distMinY)
				{
					minY = new tsg.Point(p.X, p.Y, p.Z);
					distMinY = dist;
				}
				/*if (p.X > maxX.X)
				{
					maxX = new tsg.Point(p.X, p.Y, p.Z);
				}
				else if (p.X < minX.X)
				{
					minX = new tsg.Point(p.X, p.Y, p.Z);
				}

				if (p.Y > maxY.Y)
				{
					maxY = new tsg.Point(p.X, p.Y, p.Z);
				}
				else if (p.Y < minY.Y)
				{
					minY = new tsg.Point(p.X, p.Y, p.Z);
				}*/
			}

			return new List<tsg.Point>() { minX, maxX, minY, maxY };
		}


		void drawLine(List<tsg.Point> points)
		{
			ui.GraphicsDrawer drawer = new ui.GraphicsDrawer();

			//drawer.DrawText(new Point(0.0, 1000.0, 1000.0), "TEXT SAMPLE", new Color(1.0, 0.5, 0.0));
			drawer.DrawLineSegment(points [0], points[1], new ui.Color(1.0, 0.0, 0.0));
			//Mesh mesh = new Mesh();
			//mesh.AddPoint(new Point(0.0, 0.0, 0.0));
			//mesh.AddPoint(new Point(1000.0, 0.0, 0.0));
			//mesh.AddPoint(new Point(1000.0, 1000.0, 0.0));
			//mesh.AddPoint(new Point(0.0, 1000.0, 0.0));
			//mesh.AddTriangle(0, 1, 2);
			//mesh.AddTriangle(0, 2, 3);
			//mesh.AddLine(0, 1); mesh.AddLine(1, 2); mesh.AddLine(2, 3); mesh.AddLine(3, 1);

			//drawer.DrawMeshSurface(mesh, new Color(1.0, 0.0, 0.0, 0.5));
			//drawer.DrawMeshLines(mesh, new Color(0.0, 0.0, 1.0));

		}

		public DimensionPartToGrid()
		{
			wph = model.GetWorkPlaneHandler();
			tpCurrent = wph.GetCurrentTransformationPlane();
			tpGlobal = new TransformationPlane();

		//	ui.ModelViewEnumerator ViewEnum = ui.ViewHandler.GetSelectedViews(); ViewEnum.MoveNext();
		//	tpView = new TransformationPlane((ViewEnum.Current as ui.View).ViewCoordinateSystem);
			//debug(ViewEnum.Count.ToString());



			List<tsg.Point> GridPoints = GetPointsFromGrid(model.GetModelObjectSelector().GetAllObjectsWithType(new Type[] { typeof(GridPlane)}));
			List<tsg.Point> PartsPoints = GetPointsFromSelected();
			List<tsg.Point> RectangleParts = GetMinMaxPointXY4(PartsPoints);

			//drawLine(new List<tsg.Point>() { RectangleParts[0], RectangleParts[1] });
			drawLine(new List<tsg.Point>() { RectangleParts[2], RectangleParts[3] });
			//System.Windows.Forms.MessageBox.Show(moe.GetSize().ToString());

		}


		void debug(string s)
		{
			System.Windows.Forms.MessageBox.Show(s);
		}

	}
}
