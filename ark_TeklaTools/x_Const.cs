﻿// Type: x_algor.x_Const
// Assembly: WeldApp, Version=1.15.12.29, Culture=neutral, PublicKeyToken=null
// MVID: 8148BD28-E7ED-4BDF-BDEA-0ED2A6A16AE0
// Assembly location: C:\TeklaStructures\20.0\Environments\Common\macros\drawings\WeldApp.exe

namespace ArkLib.x_algoritms
{
  internal static class x_Const
  {
    public static double EpsArea = 0.0001;
    public static double Eps = 0.05;
    public static double EpsAng = 0.018;
    public static double AngRag_80 = 1.39626;
    public static int ThreadSleep = 500;
    public static char NumberDecimalSeparator = '.';
    public static string Postfix = "z";

    static x_Const()
    {
    }
  }
}
