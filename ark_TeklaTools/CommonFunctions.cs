﻿using System;
using System.Collections.Generic;
using System.Linq;

using TSM =  Tekla.Structures.Model;
using TG = Tekla.Structures.Geometry3d;
using ArkLib.ark_Tekla.Geometry;
using x = ArkLib.x_algoritms;

namespace ArkLib.ark_Tekla.Common
{
    public static class CommonFunctions
    {
        public enum TDirection
        {
            x, 
            y,
            z
        }

        public static TSM.TransformationPlane GetTransformationPlane(TG.Point Origin, TG.Point Direction, TDirection td)
        {
            TSM.TransformationPlane TP = new TSM.TransformationPlane();
            TG.Vector ox = x.x_New.Vector(Origin, Direction);

            TG.Vector v = new TG.Vector();

            if (td == TDirection.x) v = new TG.Vector(1, 0, 0);
            if (td == TDirection.y) v = new TG.Vector(0, 1, 0);
            if (td == TDirection.z) v = new TG.Vector(0, 0, 1);

            TG.Vector oy = x.x_Math.Rotate.VectorNew(ox, -90, v);
            TG.CoordinateSystem CoordinateSystem = new TG.CoordinateSystem(Origin, ox, oy);
            return new TSM.TransformationPlane(CoordinateSystem);
        }
        /// <summary>
        /// X, Y
        /// </summary>
        /// <param name="ag"></param>
        /// <returns></returns>
        public static Tuple<List<ark_Line>, List<ark_Line>> GridToLine(ark_Grid ag)
        {
           // тут не работает
            List<ark_Line> lX = new List<ark_Line>();
            List<ark_Line> lY = new List<ark_Line>();

            List<ark_Line> lines = new List<ark_Line>();

            List<ark_Point> ap = GetPoints(ag.X, ag.Y);

            ark_Point minPointX = new ark_Point(ap[0]);
            ark_Point maxPointX = new ark_Point(ap[0]);
            ark_Point minPointY = new ark_Point(ap[0]);
            ark_Point maxPointY = new ark_Point(ap[0]);

            foreach(ark_Point ap0 in ap)
            {
                if (minPointX.x > ap0.x) minPointX = new ark_Point(ap0);
                if (maxPointX.x < ap0.x) maxPointX = new ark_Point(ap0);
                if (minPointY.y > ap0.y) minPointY = new ark_Point(ap0);
                if (maxPointY.y < ap0.y) maxPointY = new ark_Point(ap0);
            }

            //string tX = "";
            //string tY = "";

            double step_Y = 0;
            for (int i = 0; i < ag.Y.Count; i++)
            {
                ark_Point sp = new ark_Point(minPointX.x, step_Y);
                ark_Point ep = new ark_Point(maxPointX.x, step_Y);
                lX.Add(new ark_Line(sp, ep));
                step_Y += ag.Y[i];
            }

            ark_Point sp0 = new ark_Point(minPointX.x, step_Y);
            ark_Point ep0 = new ark_Point(maxPointX.x, step_Y);
            lX.Add(new ark_Line(sp0, ep0));

            double step_X = 0;
            for (int i = 0; i < ag.X.Count; i++)
            {
                ark_Point sp = new ark_Point(step_X, minPointY.y);
                ark_Point ep = new ark_Point(step_X, maxPointY.y);
                lY.Add(new ark_Line(sp, ep));
                step_X += ag.X[i];
            }

            sp0 = new ark_Point(step_X, minPointY.y);
            ep0 = new ark_Point(step_X, maxPointY.y);
            lY.Add(new ark_Line(sp0, ep0));

            return new Tuple<List<ark_Line>, List<ark_Line>>(lX, lY);
        }

        public static double Max(double v1, double v2)
        {
            return Math.Max(v1, v2);
        }

        public static double Min(double v1, double v2)
        {
            return Math.Min(v1, v2);
        }

        public static List<ark_Point> GetPoints(List<double> x, List<double> y)
        {
            List<ark_Point> ap = new List<ark_Point>();

            //ark_Point Origin = new ark_Point(0,0,0);

            //ap.Add(Origin);

            string temp = "0 0 0\n";

            double temp_x = 0;
            for (int i = 0; i < x.Count; i++)
            {
                double temp_y = 0;
                for (int k = 0; k < y.Count; k++)
                {
                    ark_Point t_ap = new ark_Point(0, 0, 0);
                    t_ap.x = temp_x;
                    t_ap.y = y[k] + temp_y;
                    temp += t_ap.ToString() + "\n";
                    temp_y += y[k];
                    ap.Add(t_ap);
                }
                temp_x += x[i];
                temp += "\n";
            }
            ap.Add(new ark_Point(temp_x, y[y.Count - 1]));

           // mes(temp);

            return ap;
        }

        #region Debug
        static void mes(string str)
        {
            //System.Windows.Forms.MessageBox.Show(str);
        }
        static void mes(int str)
        {
            //System.Windows.Forms.MessageBox.Show(str.ToString());
        }
        static void mes(double str)
        {
            //System.Windows.Forms.MessageBox.Show(str.ToString());
        }
        #endregion

        public static string Separator = " ";

        /// <summary>
        /// Из листа в string
        /// </summary>
        /// <param name="text"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string ListToString(List<string> text)
        {
            string s = "";
            foreach (string t in text)
                s += t + Separator;
            return s;
        }

        public static string ListToString(List<double> text)
        {
            string s = "";
            foreach (double t in text)
                s += t.ToString() + Separator;
            return s;
        }

        public static List<string> StringParse(string text, char separator)
        {
            return text.Split(new char[1] { separator }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        /// <summary>
        /// Получение русского алфавита
        /// </summary>
        /// <returns></returns>
        public static List<string> Letters()
        {
            List<string> s = new List<string>();
            //for (int i = 1072; i <= 1103; i++)
            //    s.Add(char.ConvertFromUtf32(i));
            char[] arr = Enumerable.Range(0, 32).Select((x, i) => (char)('а' + i)).ToArray();
            foreach (char c in arr)
                s.Add(c.ToString());

            return s;
        }

        /// <summary>
        /// Получение цифр от 1 до 100
        /// </summary>
        /// <returns></returns>
        public static List<int> Digits()
        {
            List<int> it = new List<int>();
            for (int i = 1; i <= 100; i++)
                it.Add(i);
            return it;
        }

        /// <summary>
        /// Возвращаем нормальное число, разделить точка или запятая - независимо
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static double ReturnCorrectDouble(string number)
        {
            double num = 0;
            bool b = double.TryParse(number, out num);
            if (!b)
            {
                if (number.Contains("."))
                {
                    number = number.Replace(".", ",");
                    b = double.TryParse(number, out num);
                    if (b) return num;
                    else return double.MaxValue;
                }
                else if (number.Contains(","))
                {
                    number = number.Replace(",", ".");
                    b = double.TryParse(number, out num);
                    if (b) return num;
                    else return double.MaxValue;
                }
                else return num;
            }
            else return num;
        }
    }
}
