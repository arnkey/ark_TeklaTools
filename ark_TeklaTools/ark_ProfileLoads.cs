﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArkLib.MarkElements
{
    [Serializable]
    public class ark_ProfileLoads
    {
        [Serializable]
        public struct Load
        {
            public double Q;
            public double N;
            public double M;
        }

        public Load StartPoint;
        public Load EndPoint;

        public ark_ProfileLoads(double Q1 = 0.0, double N1 = 0.0, double M1 = 0.0, double Q2 = 0.0, double N2 = 0.0, double M2 = 0.0)
        {
            StartPoint = new Load();
            StartPoint.Q = Q1;
            StartPoint.N = N1;
            StartPoint.M = M1;

            EndPoint = new Load();
            EndPoint.Q = Q1;
            EndPoint.N = N1;
            EndPoint.M = M1;
        }
    }
}
