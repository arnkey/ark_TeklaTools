﻿namespace ark_TeklaTools
{
    partial class ark_TeklaModelTools_Form
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Parts = new System.Windows.Forms.TabPage();
            this.btn_CutBeamRelativeBeam = new System.Windows.Forms.Button();
            this.txt_PartOffset = new System.Windows.Forms.TextBox();
            this.btn_PartOffset = new System.Windows.Forms.Button();
            this.btn_SwapHandles = new System.Windows.Forms.Button();
            this.SetHandRail = new System.Windows.Forms.Button();
            this.SetMainPart = new System.Windows.Forms.Button();
            this.prjOnPart = new System.Windows.Forms.Button();
            this.Assemblies = new System.Windows.Forms.TabPage();
            this.btn_SelectSingleAssembly = new System.Windows.Forms.Button();
            this.SubProgramms = new System.Windows.Forms.TabPage();
            this.ark_Ladder_Show = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.ark_MoveObjects_button = new System.Windows.Forms.Button();
            this.ark_BeamToTop_button = new System.Windows.Forms.Button();
            this.debug = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_StepComponent = new System.Windows.Forms.Button();
            this.prj = new System.Windows.Forms.TabPage();
            this.btn_Profile_I_to_Plate = new System.Windows.Forms.Button();
            this.cmp_button_p1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.Parts.SuspendLayout();
            this.Assemblies.SuspendLayout();
            this.SubProgramms.SuspendLayout();
            this.debug.SuspendLayout();
            this.prj.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.Parts);
            this.tabControl1.Controls.Add(this.Assemblies);
            this.tabControl1.Controls.Add(this.SubProgramms);
            this.tabControl1.Controls.Add(this.debug);
            this.tabControl1.Controls.Add(this.prj);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(190, 314);
            this.tabControl1.TabIndex = 0;
            // 
            // Parts
            // 
            this.Parts.Controls.Add(this.btn_CutBeamRelativeBeam);
            this.Parts.Controls.Add(this.txt_PartOffset);
            this.Parts.Controls.Add(this.btn_PartOffset);
            this.Parts.Controls.Add(this.btn_SwapHandles);
            this.Parts.Controls.Add(this.SetHandRail);
            this.Parts.Controls.Add(this.SetMainPart);
            this.Parts.Controls.Add(this.prjOnPart);
            this.Parts.Location = new System.Drawing.Point(23, 4);
            this.Parts.Name = "Parts";
            this.Parts.Padding = new System.Windows.Forms.Padding(3);
            this.Parts.Size = new System.Drawing.Size(163, 306);
            this.Parts.TabIndex = 0;
            this.Parts.Text = "Детали";
            this.Parts.UseVisualStyleBackColor = true;
            // 
            // btn_CutBeamRelativeBeam
            // 
            this.btn_CutBeamRelativeBeam.Location = new System.Drawing.Point(37, 187);
            this.btn_CutBeamRelativeBeam.Name = "btn_CutBeamRelativeBeam";
            this.btn_CutBeamRelativeBeam.Size = new System.Drawing.Size(101, 46);
            this.btn_CutBeamRelativeBeam.TabIndex = 8;
            this.btn_CutBeamRelativeBeam.Text = "Подрезать балку";
            this.btn_CutBeamRelativeBeam.UseVisualStyleBackColor = true;
            this.btn_CutBeamRelativeBeam.Click += new System.EventHandler(this.btn_CutBeamRelativeBeam_Click);
            // 
            // txt_PartOffset
            // 
            this.txt_PartOffset.Location = new System.Drawing.Point(6, 243);
            this.txt_PartOffset.Name = "txt_PartOffset";
            this.txt_PartOffset.Size = new System.Drawing.Size(59, 20);
            this.txt_PartOffset.TabIndex = 7;
            this.txt_PartOffset.Text = "0";
            this.txt_PartOffset.Visible = false;
            // 
            // btn_PartOffset
            // 
            this.btn_PartOffset.Enabled = false;
            this.btn_PartOffset.Location = new System.Drawing.Point(71, 239);
            this.btn_PartOffset.Name = "btn_PartOffset";
            this.btn_PartOffset.Size = new System.Drawing.Size(67, 26);
            this.btn_PartOffset.TabIndex = 6;
            this.btn_PartOffset.Text = "Offset\'s";
            this.btn_PartOffset.UseVisualStyleBackColor = true;
            this.btn_PartOffset.Visible = false;
            this.btn_PartOffset.Click += new System.EventHandler(this.btn_PartOffset_Click);
            // 
            // btn_SwapHandles
            // 
            this.btn_SwapHandles.Location = new System.Drawing.Point(37, 135);
            this.btn_SwapHandles.Name = "btn_SwapHandles";
            this.btn_SwapHandles.Size = new System.Drawing.Size(101, 46);
            this.btn_SwapHandles.TabIndex = 4;
            this.btn_SwapHandles.Text = "Смена рабочих точек";
            this.btn_SwapHandles.UseVisualStyleBackColor = true;
            this.btn_SwapHandles.Click += new System.EventHandler(this.btn_SwapHandles_Click);
            // 
            // SetHandRail
            // 
            this.SetHandRail.Location = new System.Drawing.Point(37, 88);
            this.SetHandRail.Name = "SetHandRail";
            this.SetHandRail.Size = new System.Drawing.Size(101, 46);
            this.SetHandRail.TabIndex = 3;
            this.SetHandRail.Text = "Поставить ограждение";
            this.SetHandRail.UseVisualStyleBackColor = true;
            this.SetHandRail.Click += new System.EventHandler(this.SetHandRail_Click);
            // 
            // SetMainPart
            // 
            this.SetMainPart.Location = new System.Drawing.Point(37, 48);
            this.SetMainPart.Name = "SetMainPart";
            this.SetMainPart.Size = new System.Drawing.Size(101, 34);
            this.SetMainPart.TabIndex = 2;
            this.SetMainPart.Text = "Задать главную деталь сборки";
            this.SetMainPart.UseVisualStyleBackColor = true;
            this.SetMainPart.Click += new System.EventHandler(this.SetMainPart_Click);
            // 
            // prjOnPart
            // 
            this.prjOnPart.Location = new System.Drawing.Point(37, 8);
            this.prjOnPart.Name = "prjOnPart";
            this.prjOnPart.Size = new System.Drawing.Size(101, 36);
            this.prjOnPart.TabIndex = 1;
            this.prjOnPart.Text = "Проекция по детали";
            this.prjOnPart.UseVisualStyleBackColor = true;
            this.prjOnPart.Click += new System.EventHandler(this.prjOnPart_Click);
            // 
            // Assemblies
            // 
            this.Assemblies.Controls.Add(this.btn_SelectSingleAssembly);
            this.Assemblies.Location = new System.Drawing.Point(23, 4);
            this.Assemblies.Name = "Assemblies";
            this.Assemblies.Size = new System.Drawing.Size(163, 306);
            this.Assemblies.TabIndex = 2;
            this.Assemblies.Text = "Сборки";
            this.Assemblies.UseVisualStyleBackColor = true;
            // 
            // btn_SelectSingleAssembly
            // 
            this.btn_SelectSingleAssembly.Location = new System.Drawing.Point(8, 3);
            this.btn_SelectSingleAssembly.Name = "btn_SelectSingleAssembly";
            this.btn_SelectSingleAssembly.Size = new System.Drawing.Size(134, 36);
            this.btn_SelectSingleAssembly.TabIndex = 5;
            this.btn_SelectSingleAssembly.Text = "Выбрать однодетальные сборки";
            this.btn_SelectSingleAssembly.UseVisualStyleBackColor = true;
            this.btn_SelectSingleAssembly.Click += new System.EventHandler(this.btn_SelectSingleAssembly_Click);
            // 
            // SubProgramms
            // 
            this.SubProgramms.Controls.Add(this.ark_Ladder_Show);
            this.SubProgramms.Controls.Add(this.button2);
            this.SubProgramms.Controls.Add(this.button1);
            this.SubProgramms.Controls.Add(this.ark_MoveObjects_button);
            this.SubProgramms.Controls.Add(this.ark_BeamToTop_button);
            this.SubProgramms.Location = new System.Drawing.Point(23, 4);
            this.SubProgramms.Name = "SubProgramms";
            this.SubProgramms.Padding = new System.Windows.Forms.Padding(3);
            this.SubProgramms.Size = new System.Drawing.Size(163, 306);
            this.SubProgramms.TabIndex = 1;
            this.SubProgramms.Text = "Подпрограммы";
            this.SubProgramms.UseVisualStyleBackColor = true;
            // 
            // ark_Ladder_Show
            // 
            this.ark_Ladder_Show.Location = new System.Drawing.Point(22, 122);
            this.ark_Ladder_Show.Name = "ark_Ladder_Show";
            this.ark_Ladder_Show.Size = new System.Drawing.Size(101, 23);
            this.ark_Ladder_Show.TabIndex = 5;
            this.ark_Ladder_Show.Text = "ark_Ladder";
            this.ark_Ladder_Show.UseVisualStyleBackColor = true;
            this.ark_Ladder_Show.Click += new System.EventHandler(this.ark_Ladder_Show_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(22, 93);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "ark_Joints";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(22, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "ark_MovePoint";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ark_MoveObjects_button
            // 
            this.ark_MoveObjects_button.Location = new System.Drawing.Point(22, 35);
            this.ark_MoveObjects_button.Name = "ark_MoveObjects_button";
            this.ark_MoveObjects_button.Size = new System.Drawing.Size(101, 23);
            this.ark_MoveObjects_button.TabIndex = 2;
            this.ark_MoveObjects_button.Text = "ark_MoveObjects";
            this.ark_MoveObjects_button.UseVisualStyleBackColor = true;
            this.ark_MoveObjects_button.Click += new System.EventHandler(this.ark_MoveObjects_button_Click);
            // 
            // ark_BeamToTop_button
            // 
            this.ark_BeamToTop_button.Location = new System.Drawing.Point(22, 6);
            this.ark_BeamToTop_button.Name = "ark_BeamToTop_button";
            this.ark_BeamToTop_button.Size = new System.Drawing.Size(101, 23);
            this.ark_BeamToTop_button.TabIndex = 1;
            this.ark_BeamToTop_button.Text = "ark_BeamToTop";
            this.ark_BeamToTop_button.UseVisualStyleBackColor = true;
            this.ark_BeamToTop_button.Click += new System.EventHandler(this.ark_BeamToTop_button_Click);
            // 
            // debug
            // 
            this.debug.Controls.Add(this.button7);
            this.debug.Controls.Add(this.button6);
            this.debug.Controls.Add(this.button5);
            this.debug.Controls.Add(this.button4);
            this.debug.Controls.Add(this.button3);
            this.debug.Controls.Add(this.btn_StepComponent);
            this.debug.Location = new System.Drawing.Point(23, 4);
            this.debug.Name = "debug";
            this.debug.Size = new System.Drawing.Size(163, 306);
            this.debug.TabIndex = 3;
            this.debug.Text = "ArnKey\'s";
            this.debug.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(40, 82);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 5;
            this.button7.Text = "List welds";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(12, 142);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(123, 23);
            this.button6.TabIndex = 4;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(15, 246);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(123, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(15, 275);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(123, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(15, 53);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(123, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_StepComponent
            // 
            this.btn_StepComponent.Location = new System.Drawing.Point(15, 24);
            this.btn_StepComponent.Name = "btn_StepComponent";
            this.btn_StepComponent.Size = new System.Drawing.Size(123, 23);
            this.btn_StepComponent.TabIndex = 0;
            this.btn_StepComponent.Text = "btn_StepComponent";
            this.btn_StepComponent.UseVisualStyleBackColor = true;
            this.btn_StepComponent.Click += new System.EventHandler(this.btn_StepComponent_Click);
            // 
            // prj
            // 
            this.prj.Controls.Add(this.btn_Profile_I_to_Plate);
            this.prj.Controls.Add(this.cmp_button_p1);
            this.prj.Location = new System.Drawing.Point(23, 4);
            this.prj.Name = "prj";
            this.prj.Size = new System.Drawing.Size(163, 306);
            this.prj.TabIndex = 4;
            this.prj.Text = "!Common";
            this.prj.UseVisualStyleBackColor = true;
            // 
            // btn_Profile_I_to_Plate
            // 
            this.btn_Profile_I_to_Plate.Location = new System.Drawing.Point(12, 84);
            this.btn_Profile_I_to_Plate.Name = "btn_Profile_I_to_Plate";
            this.btn_Profile_I_to_Plate.Size = new System.Drawing.Size(84, 70);
            this.btn_Profile_I_to_Plate.TabIndex = 2;
            this.btn_Profile_I_to_Plate.Text = "I profile to plate";
            this.btn_Profile_I_to_Plate.UseVisualStyleBackColor = true;
            this.btn_Profile_I_to_Plate.Click += new System.EventHandler(this.btn_Profile_I_to_Plate_Click);
            // 
            // cmp_button_p1
            // 
            this.cmp_button_p1.Location = new System.Drawing.Point(12, 8);
            this.cmp_button_p1.Name = "cmp_button_p1";
            this.cmp_button_p1.Size = new System.Drawing.Size(84, 70);
            this.cmp_button_p1.TabIndex = 1;
            this.cmp_button_p1.Text = "Разбить на пластины WI";
            this.cmp_button_p1.UseVisualStyleBackColor = true;
            this.cmp_button_p1.Click += new System.EventHandler(this.cmp_button_p1_Click);
            // 
            // ark_TeklaModelTools_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(190, 314);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ark_TeklaModelTools_Form";
            this.Text = "ark_TeklaTools";
            this.Load += new System.EventHandler(this.ark_TeklaTools_Form_Load);
            this.tabControl1.ResumeLayout(false);
            this.Parts.ResumeLayout(false);
            this.Parts.PerformLayout();
            this.Assemblies.ResumeLayout(false);
            this.SubProgramms.ResumeLayout(false);
            this.debug.ResumeLayout(false);
            this.prj.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Parts;
        private System.Windows.Forms.TabPage SubProgramms;
        private System.Windows.Forms.Button prjOnPart;
        private System.Windows.Forms.Button SetMainPart;
        private System.Windows.Forms.Button SetHandRail;
        private System.Windows.Forms.Button ark_BeamToTop_button;
        private System.Windows.Forms.TabPage Assemblies;
        private System.Windows.Forms.Button btn_SelectSingleAssembly;
        private System.Windows.Forms.Button ark_MoveObjects_button;
        private System.Windows.Forms.Button btn_SwapHandles;
        private System.Windows.Forms.TextBox txt_PartOffset;
        private System.Windows.Forms.Button btn_PartOffset;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage debug;
        private System.Windows.Forms.Button btn_StepComponent;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btn_CutBeamRelativeBeam;
        private System.Windows.Forms.Button ark_Ladder_Show;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TabPage prj;
        private System.Windows.Forms.Button cmp_button_p1;
        private System.Windows.Forms.Button btn_Profile_I_to_Plate;
    }
}

