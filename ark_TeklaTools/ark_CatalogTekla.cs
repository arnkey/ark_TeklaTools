﻿using System;
using System.Collections.Generic;

using Tekla.Structures.Catalogs;
using Tekla.Structures.Dialog;


namespace ArkLib.ark_Tekla.Catalogs
{
    [Serializable]
    public static class ark_CatalogTekla
    {

        public static Tuple<List<LibraryProfileItem>, List<ParametricProfileItem>>Profiles = null;

        public static List<MaterialItem> Materials = null;


        public static void Load()
        {
            Profiles = GetProfilesFromModel();
            Materials = GetMaterialsFromModel();
        }


        private static Tuple<List<LibraryProfileItem>, List<ParametricProfileItem>> GetProfilesFromModel()
        {
            List<LibraryProfileItem> libs = new List<LibraryProfileItem>();
            List<ParametricProfileItem> libparams = new List<ParametricProfileItem>();

            CatalogHandler ch = new CatalogHandler();
            if (ch.GetConnectionStatus())
            {
                ProfileItemEnumerator profiles = ch.GetLibraryProfileItems();
                while (profiles.MoveNext())
                {
                    if (profiles.Current is LibraryProfileItem)
                        libs.Add(profiles.Current as LibraryProfileItem);
                    else if (profiles.Current is ParametricProfileItem)
                        libparams.Add(profiles.Current as ParametricProfileItem);
                }
            }
            return Tuple.Create(libs, libparams);
        }

        private static List<MaterialItem> GetMaterialsFromModel()
        {
            List<MaterialItem> temp = new List<MaterialItem>();
            CatalogHandler ch = new CatalogHandler();
            if (ch.GetConnectionStatus())
            {
                MaterialItemEnumerator material = ch.GetMaterialItems();
                while (material.MoveNext())
                {
                    temp.Add(material.Current as MaterialItem);
                }
            }
            return temp;
        }

    }
}
