﻿namespace ark_TeklaTools
{
    partial class ark_MovePoints
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_modify = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_top = new System.Windows.Forms.RadioButton();
            this.rb_bottom = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rb_dy = new System.Windows.Forms.RadioButton();
            this.rb_dx = new System.Windows.Forms.RadioButton();
            this.rb_dz = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_value = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_modify
            // 
 //           this.btn_modify.BackgroundImage = global::ark_TeklaTools.Properties.Resources.sweebr_pos_start_fee;
            this.btn_modify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_modify.Location = new System.Drawing.Point(33, 247);
            this.btn_modify.Name = "btn_modify";
            this.btn_modify.Size = new System.Drawing.Size(50, 50);
            this.btn_modify.TabIndex = 0;
            this.btn_modify.UseVisualStyleBackColor = true;
            this.btn_modify.Click += new System.EventHandler(this.btn_modify_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_bottom);
            this.groupBox1.Controls.Add(this.rb_top);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(99, 69);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Точки";
            // 
            // rb_top
            // 
            this.rb_top.AutoSize = true;
            this.rb_top.Checked = true;
            this.rb_top.Location = new System.Drawing.Point(6, 19);
            this.rb_top.Name = "rb_top";
            this.rb_top.Size = new System.Drawing.Size(67, 17);
            this.rb_top.TabIndex = 0;
            this.rb_top.TabStop = true;
            this.rb_top.Text = "Верхняя";
            this.rb_top.UseVisualStyleBackColor = true;
            // 
            // rb_bottom
            // 
            this.rb_bottom.AutoSize = true;
            this.rb_bottom.Location = new System.Drawing.Point(6, 42);
            this.rb_bottom.Name = "rb_bottom";
            this.rb_bottom.Size = new System.Drawing.Size(65, 17);
            this.rb_bottom.TabIndex = 1;
            this.rb_bottom.Text = "Нижняя";
            this.rb_bottom.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rb_dz);
            this.groupBox2.Controls.Add(this.rb_dy);
            this.groupBox2.Controls.Add(this.rb_dx);
            this.groupBox2.Location = new System.Drawing.Point(12, 87);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(99, 92);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Направление";
            // 
            // rb_dy
            // 
            this.rb_dy.AutoSize = true;
            this.rb_dy.Location = new System.Drawing.Point(6, 42);
            this.rb_dy.Name = "rb_dy";
            this.rb_dy.Size = new System.Drawing.Size(36, 17);
            this.rb_dy.TabIndex = 1;
            this.rb_dy.Text = "dy";
            this.rb_dy.UseVisualStyleBackColor = true;
//            this.rb_dy.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // rb_dx
            // 
            this.rb_dx.AutoSize = true;
            this.rb_dx.Location = new System.Drawing.Point(6, 19);
            this.rb_dx.Name = "rb_dx";
            this.rb_dx.Size = new System.Drawing.Size(36, 17);
            this.rb_dx.TabIndex = 0;
            this.rb_dx.Text = "dx";
            this.rb_dx.UseVisualStyleBackColor = true;
            // 
            // rb_dz
            // 
            this.rb_dz.AutoSize = true;
            this.rb_dz.Checked = true;
            this.rb_dz.Location = new System.Drawing.Point(6, 65);
            this.rb_dz.Name = "rb_dz";
            this.rb_dz.Size = new System.Drawing.Size(36, 17);
            this.rb_dz.TabIndex = 2;
            this.rb_dz.TabStop = true;
            this.rb_dz.Text = "dz";
            this.rb_dz.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txt_value);
            this.groupBox3.Location = new System.Drawing.Point(12, 185);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(99, 56);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Значение";
            // 
            // txt_value
            // 
            this.txt_value.Location = new System.Drawing.Point(6, 19);
            this.txt_value.Name = "txt_value";
            this.txt_value.Size = new System.Drawing.Size(86, 20);
            this.txt_value.TabIndex = 0;
            this.txt_value.Text = "100";
            // 
            // ark_MovePoints
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(124, 306);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_modify);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ark_MovePoints";
            this.Text = "ark_MovePoints";
            this.Load += new System.EventHandler(this.ark_MovePoints_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_modify;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb_bottom;
        private System.Windows.Forms.RadioButton rb_top;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rb_dy;
        private System.Windows.Forms.RadioButton rb_dx;
        private System.Windows.Forms.RadioButton rb_dz;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txt_value;
    }
}