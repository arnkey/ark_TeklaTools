using System;

using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;
using tsc = Tekla.Structures.Catalogs;

namespace ark_Functions
{


    public class ItoPlate
    {
        public static void Run()
        {
            tsm.Model model = new tsm.Model();
            tsm.UI.ModelObjectSelector mobs = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mobs.GetSelectedObjects();

            tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            while (moe.MoveNext())
            {
                if (moe.Current is tsm.PolyBeam)
                {

                    double width = 0;
                    double t1_flange = 0;
                    double t_web = 0;
                    double height_full = 0;
                    double height = 0;

                    tsm.PolyBeam pb = moe.Current as tsm.PolyBeam;

                    geom(ref width, ref t1_flange, ref t_web, ref height_full, ref height, moe.Current);
                tsm.PolyBeam npb = new tsm.PolyBeam();
                npb.Position.Depth = tsm.Position.DepthEnum.FRONT;
                npb.Position.Plane = tsm.Position.PlaneEnum.MIDDLE;
                npb.Position.Rotation = tsm.Position.RotationEnum.BACK;
                npb.Material.MaterialString = pb.Material.MaterialString;
                npb.Class = "2";
                npb.Profile.ProfileString = $"PL{t1_flange}*{width}";
                npb.Name = pb.Name;
                npb.AssemblyNumber.Prefix = pb.AssemblyNumber.Prefix;
                npb.AssemblyNumber.StartNumber = pb.AssemblyNumber.StartNumber;
                npb.PartNumber.Prefix = pb.AssemblyNumber.Prefix;
                npb.PartNumber.StartNumber = pb.PartNumber.StartNumber;
                npb.Contour.ContourPoints.Clear();
                wph.SetCurrentTransformationPlane(new tsm.TransformationPlane());

                tsm.Contour pb1 = new tsm.Contour();
                tsm.Contour pb2 = new tsm.Contour();
                tsm.Contour cplate = new tsm.Contour();

                for (int i = 0; i < pb.Contour.ContourPoints.Count - 1; i++)
                {
                    tsm.ContourPoint cp1 = pb.Contour.ContourPoints[i] as tsm.ContourPoint;
                    tsm.ContourPoint cp2 = pb.Contour.ContourPoints[i + 1] as tsm.ContourPoint;

                    tsm.TransformationPlane tp = new tsm.TransformationPlane();
                    WiToPlate.GetTP(ref tp, cp1, cp2);
                    tsg.CoordinateSystem LocalCS = new tsg.CoordinateSystem();
                    WiToPlate.CS(ref LocalCS, cp1, cp2);
                    tsg.Matrix matrixLocal = tsg.MatrixFactory.ToCoordinateSystem(LocalCS);
                    wph.SetCurrentTransformationPlane(tp);
                    //new tsm.Model().CommitChanges();
                    //mes("A");
                    if (i != 0)
                    {
                             cp1.Chamfer.Type = tsm.Chamfer.ChamferTypeEnum.CHAMFER_ROUNDING;
                            cp2.Chamfer.Type = tsm.Chamfer.ChamferTypeEnum.CHAMFER_ROUNDING;
                            cp2.Chamfer.X = (pb.Contour.ContourPoints[i] as tsm.ContourPoint).Chamfer.X ;
                             cp1.Chamfer.X = (pb.Contour.ContourPoints[i] as tsm.ContourPoint).Chamfer.X + height_full;
                            //cp1.Chamfer = null;
                        }

                    tsg.Point p0 = new tsg.Point(0, 0.5, 0);
                    tsm.ContourPoint lcp0 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(p0), cp2.Chamfer);
                    pb1.AddContourPoint(lcp0);
                    p0.Y += height_full;

                        tsg.Point cpl0 = new tsg.Point(0, t1_flange + 0.5, 0);
                        tsg.Point cpl1 = new tsg.Point(0, height + t1_flange + 0.5, 0);

                        if (i != 0 && i != pb.Contour.ContourPoints.Count - 1)
                        {
                            p0.X -= height_full;
                            //cpl1.X -= height + 0.5;// + t1_flange + 0.5;
                            //cpl0.X -= t1_flange + 0.5;
                            cpl0.X -= t1_flange;

                           // c

                            cpl1.X -= t1_flange;
                            cpl1.X -= height - 1;
                            cpl1.Y -= 1;
                            //cpl0.Y -= t1_flange;

                        }
                    lcp0 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(p0), cp1.Chamfer);
                    pb2.AddContourPoint(lcp0);

                   
                        tsm.Chamfer chamf = new tsm.Chamfer() {  Type = tsm.Chamfer.ChamferTypeEnum.CHAMFER_ROUNDING, X = (pb.Contour.ContourPoints[i] as tsm.ContourPoint).Chamfer.X - height - t1_flange};
                    tsm.ContourPoint lcpPL0 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl0), chamf);

                        tsm.Chamfer chamf1 = new tsm.Chamfer() { Type = tsm.Chamfer.ChamferTypeEnum.CHAMFER_ROUNDING, X = (pb.Contour.ContourPoints[i] as tsm.ContourPoint).Chamfer.X - t1_flange};

                        tsm.ContourPoint lcpPL1 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl1), chamf1);

                        if (i == 0)
                        {
                            lcpPL0 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl0), null);
                            lcpPL1 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl1), null);
                        }

                    cplate.AddContourPoint(lcpPL0);
                    cplate.AddContourPoint(lcpPL1);

                    if (i == pb.Contour.ContourPoints.Count - 2)
                    {
                        cp2.Chamfer = cp1.Chamfer;
                        tsg.Point p1 = matrixLocal.Transform(new tsg.Point(cp2.X, cp2.Y, cp2.Z));
                            p1.Y += 0;
                        tsm.ContourPoint lcp1 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(p1), chamf);
                        pb1.AddContourPoint(lcp1);
                        p1.Y += (height_full);
                        lcp1 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(p1), chamf1);
                        pb2.AddContourPoint(lcp1);

                        tsg.Point cpl20 = new tsg.Point(p1.X, t1_flange + 0.5, 0);
                            tsg.Point cpl21 = new tsg.Point(p1.X, height + t1_flange + 0.5, 0);

                            tsm.ContourPoint lcpPL20 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl20), new tsm.Chamfer());
                        tsm.ContourPoint lcpPL21 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl21), new tsm.Chamfer());

                        cplate.AddContourPoint(lcpPL20);
                        cplate.AddContourPoint(lcpPL21);

                    }
                    wph.SetCurrentTransformationPlane(ctp);

                }
                npb.Contour = pb1;
                npb.Insert();

                foreach(tsm.ModelObject _bp in pb.GetBooleans())
                    {
                       if (_bp is tsm.BooleanPart)
                        {
                            tsm.BooleanPart pp = (_bp as tsm.BooleanPart);
                            pp.Father = npb;
                            pp.Insert();
                        }
                       else if (_bp is tsm.CutPlane)
                        {
                            tsm.CutPlane cp = (_bp as tsm.CutPlane);
                            cp.Father = npb;
                           cp.Insert();
                        }
                        else if (_bp is tsm.Fitting)
                        {
                            tsm.Fitting cp = (_bp as tsm.Fitting);
                            cp.Father = npb;
                            cp.Insert();
                        }
                    }

                npb.Position.Depth = tsm.Position.DepthEnum.BEHIND;
                for (int i = 0; i < pb2.ContourPoints.Count; i++)
                {
                    tsm.ContourPoint cp = pb.Contour.ContourPoints[i] as tsm.ContourPoint;
                    if (!(i == 0 || i == pb.Contour.ContourPoints.Count - 1))
                    {
                       // cp.Chamfer.Type = tsm.Chamfer.ChamferTypeEnum.CHAMFER_ROUNDING;
                       //cp.Chamfer.X = (pb.Contour.ContourPoints[i] as tsm.ContourPoint).Chamfer.X;
                    }
                }
                npb.Contour = pb2;
                npb.Insert();
                    foreach (tsm.ModelObject _bp in pb.GetBooleans())
                    {
                        if (_bp is tsm.BooleanPart)
                        {
                            tsm.BooleanPart pp = (_bp as tsm.BooleanPart);
                            pp.Father = npb;
                            pp.Insert();
                        }
                        else if (_bp is tsm.CutPlane)
                        {
                            tsm.CutPlane cp = (_bp as tsm.CutPlane);
                            cp.Father = npb;
                            cp.Insert();
                        }
                        else if (_bp is tsm.Fitting)
                        {
                            tsm.Fitting cp = (_bp as tsm.Fitting);
                            cp.Father = npb;
                            cp.Insert();
                        }
                    }
                    #region plate
                    tsm.ContourPlate PLATE = new tsm.ContourPlate();
                tsm.Contour cplateSorting = new tsm.Contour();
                for (int i = 0; i < cplate.ContourPoints.Count; i++)
                {
                    cplateSorting.AddContourPoint(cplate.ContourPoints[i] as tsm.ContourPoint);
                    i++;
                }
                for (int i = cplate.ContourPoints.Count - 1; i > 0; i--)
                {
                    cplateSorting.AddContourPoint(cplate.ContourPoints[i] as tsm.ContourPoint);
                    i--;
                }

                PLATE.Contour = cplateSorting;
                    PLATE.Class = "3";
                PLATE.Material = pb.Material;
                PLATE.Name = "�����";
                PLATE.Profile.ProfileString = $"PL{t_web}";

                PLATE.AssemblyNumber.Prefix = pb.AssemblyNumber.Prefix;
                PLATE.AssemblyNumber.StartNumber = pb.AssemblyNumber.StartNumber;
                PLATE.PartNumber.Prefix = pb.AssemblyNumber.Prefix;
                PLATE.PartNumber.StartNumber = pb.PartNumber.StartNumber;

                    //PLATE.Type = tsm.ContourPlate.ContourPlateTypeEnum.PLATE;

                    PLATE.Insert();
                    foreach (tsm.ModelObject _bp in pb.GetBooleans())
                    {
                        if (_bp is tsm.BooleanPart)
                        {
                            tsm.BooleanPart pp = (_bp as tsm.BooleanPart);
                            pp.Father = PLATE;
                            pp.Insert();
                        }
                        else if (_bp is tsm.CutPlane)
                        {
                            tsm.CutPlane cp = (_bp as tsm.CutPlane);
                            cp.Father = PLATE;
                            cp.Insert();
                        }
                        else if (_bp is tsm.Fitting)
                        {
                            tsm.Fitting cp = (_bp as tsm.Fitting);
                            cp.Father = PLATE;
                            cp.Insert();
                        }
                    }
                    //mes(npb.Insert());
                    // wph.SetCurrentTransformationPlane(ctp);
                    #endregion
                }
        }
        wph.SetCurrentTransformationPlane(ctp);
            new tsm.Model().CommitChanges();
    }


        static void geom(ref double width, ref double t1_flange, ref double t_web, ref double height_full, ref double height, tsm.ModelObject obj)
        {
            string profile = ""; obj.GetReportProperty("PROFILE", ref profile);
            obj.GetReportProperty("PROFILE.WIDTH", ref width);
            obj.GetReportProperty("PROFILE.WEB_THICKNESS", ref t_web);
            obj.GetReportProperty("PROFILE.HEIGHT", ref height_full);
            obj.GetReportProperty("PROFILE.FLANGE_THICKNESS", ref t1_flange);

            t_web = Math.Floor(t_web);
            t1_flange = Math.Floor(t1_flange);

            height = height_full - t1_flange * 2;
        }
    }


    public class WiToPlate
    {



        static void geom(ref double width, ref double t1_flange, ref double t_web, ref double height_full, ref double height, string profile)
        {
            width = double.Parse(ark_split(profile)[1]);
            t1_flange = double.Parse(profile.Split(new char[] { '-' })[1]);
            t_web = double.Parse(profile.Split(new char[] { '-' })[2].Split(new char[] { '*' })[0]);
            height_full = double.Parse(ark_split(ark_split(profile, '-')[0])[0].Replace("WI", ""));
            height = height_full - t1_flange * 2;
        }

        public static void Run()
        {
            tsm.Model model = new tsm.Model();
            tsm.UI.ModelObjectSelector mobs = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mobs.GetSelectedObjects();

            tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();


            while (moe.MoveNext())
            {
                if (moe.Current is tsm.Beam)
                {
                    tsm.Beam pb = moe.Current as tsm.Beam;
                    string profile = pb.Profile.ProfileString;
                    //if (profile.Contains("WI") == false) continue;

                    double width = 0;
                    double t1_flange = 0;
                    double t_web = 0;
                    double height_full = 0;
                    double height = 0;

                    geom(ref width, ref t1_flange, ref t_web, ref height_full, ref height, profile);


                    tsm.Beam npb = new tsm.Beam();
                    npb.Position.Depth = tsm.Position.DepthEnum.FRONT;
                    npb.Position.Plane = tsm.Position.PlaneEnum.MIDDLE;
                    npb.Position.Rotation = tsm.Position.RotationEnum.BACK;
                    npb.Material.MaterialString = pb.Material.MaterialString;
                    npb.Class = pb.Class;
                    npb.Profile.ProfileString = $"PL{t_web}*{width}";
                    npb.Name = pb.Name;
                    npb.AssemblyNumber.Prefix = pb.AssemblyNumber.Prefix;
                    npb.AssemblyNumber.StartNumber = pb.AssemblyNumber.StartNumber;
                    npb.PartNumber.Prefix = pb.AssemblyNumber.Prefix;
                    npb.PartNumber.StartNumber = pb.PartNumber.StartNumber;
                    wph.SetCurrentTransformationPlane(new tsm.TransformationPlane());

                    tsm.Contour cplate = new tsm.Contour();


                    tsm.TransformationPlane tp = new tsm.TransformationPlane();
                    GetTP(ref tp, pb.StartPoint, pb.EndPoint);
                    tsg.CoordinateSystem LocalCS = new tsg.CoordinateSystem();
                    CS(ref LocalCS, pb.StartPoint, pb.EndPoint);
                    tsg.Matrix matrixLocal = tsg.MatrixFactory.ToCoordinateSystem(LocalCS);
                   // wph.SetCurrentTransformationPlane(tp);

                    tsg.Point p0 = new tsg.Point(0, -1 * (height_full * 0.5), 0);
                    tsg.Point lcp0 = new tsg.Point(tp.TransformationMatrixToGlobal.Transform(p0));
                    tsg.Point p1 = matrixLocal.Transform(new tsg.Point(pb.EndPoint.X, pb.EndPoint.Y, pb.EndPoint.Z));
                    p1.Y += -1 * (height_full * 0.5);
                    tsg.Point lcp1 = new tsg.Point(tp.TransformationMatrixToGlobal.Transform(p1));

                    npb.StartPoint = new tsg.Point(lcp0);
                    npb.EndPoint = new tsg.Point(lcp1);

                    npb.Insert();

                    tsg.Point cpl0 = new tsg.Point(0, -1 * (height_full * 0.5 - t_web), 0);
                    tsg.Point cpl1 = new tsg.Point(0, (height_full * 0.5 - t_web), 0);

                    tsm.ContourPoint lcpPL0 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl0), new tsm.Chamfer());
                    tsm.ContourPoint lcpPL1 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl1), new tsm.Chamfer());

                    cplate.AddContourPoint(lcpPL0);
                    cplate.AddContourPoint(lcpPL1);

                    p0.Y += (height_full);
                    p1.Y += (height_full);

                    lcp0 = new tsg.Point(tp.TransformationMatrixToGlobal.Transform(p0));
                    lcp1 = new tsg.Point(tp.TransformationMatrixToGlobal.Transform(p1));

                    npb.StartPoint = new tsg.Point(lcp0);
                    npb.EndPoint = new tsg.Point(lcp1);
                    npb.Position.Depth = tsm.Position.DepthEnum.BEHIND;
                    npb.Insert();

                    //npb.Position.Depth = tsm.Position.DepthEnum.BEHIND;

                    tsg.Point cpl20 = new tsg.Point(p1.X, -1 * (height_full * 0.5 - t_web), 0);
                    tsg.Point cpl21 = new tsg.Point(p1.X, (height_full * 0.5 - t_web), 0);

                    tsm.ContourPoint lcpPL20 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl20), new tsm.Chamfer());
                    tsm.ContourPoint lcpPL21 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl21), new tsm.Chamfer());

                    cplate.AddContourPoint(lcpPL20);
                    cplate.AddContourPoint(lcpPL21);

                    tsm.ContourPlate PLATE = new tsm.ContourPlate();
                    tsm.Contour cplateSorting = new tsm.Contour();
                    for (int i = 0; i < cplate.ContourPoints.Count; i++)
                    {
                        cplateSorting.AddContourPoint(cplate.ContourPoints[i] as tsm.ContourPoint);
                        i++;
                    }
                    for (int i = cplate.ContourPoints.Count - 1; i > 0; i--)
                    {
                        cplateSorting.AddContourPoint(cplate.ContourPoints[i] as tsm.ContourPoint);
                        i--;
                    }

                    PLATE.Contour = cplateSorting;
                    PLATE.Class = pb.Class;
                    PLATE.Material = pb.Material;
                    PLATE.Name = "�����";
                    PLATE.Profile.ProfileString = $"PL{t1_flange}";

                    PLATE.AssemblyNumber.Prefix = pb.AssemblyNumber.Prefix;
                    PLATE.AssemblyNumber.StartNumber = pb.AssemblyNumber.StartNumber;
                    PLATE.PartNumber.Prefix = pb.AssemblyNumber.Prefix;
                    PLATE.PartNumber.StartNumber = pb.PartNumber.StartNumber;

                    PLATE.Insert();


                   // wph.SetCurrentTransformationPlane(ctp);
                    new tsm.Model().CommitChanges();
                }
                if (moe.Current is tsm.PolyBeam)
                {
                    tsm.PolyBeam pb = moe.Current as tsm.PolyBeam;

                    string profile = pb.Profile.ProfileString;
                    double width = 0;
                    double t1_flange = 0;
                    double t_web = 0;
                    double height_full = 0;
                    double height = 0;

                    geom(ref width, ref t1_flange, ref t_web, ref height_full, ref height, profile);

                    tsm.PolyBeam npb = new tsm.PolyBeam();
                    npb.Position.Depth = tsm.Position.DepthEnum.FRONT;
                    npb.Position.Plane = tsm.Position.PlaneEnum.MIDDLE;
                    npb.Position.Rotation = tsm.Position.RotationEnum.BACK;
                    npb.Material.MaterialString = pb.Material.MaterialString;
                    npb.Class = pb.Class;
                    npb.Profile.ProfileString = $"PL{t_web}*{width}";
                    npb.Name = pb.Name;
                    npb.AssemblyNumber.Prefix = pb.AssemblyNumber.Prefix;
                    npb.AssemblyNumber.StartNumber = pb.AssemblyNumber.StartNumber;
                    npb.PartNumber.Prefix = pb.AssemblyNumber.Prefix;
                    npb.PartNumber.StartNumber = pb.PartNumber.StartNumber;
                    npb.Contour.ContourPoints.Clear();
                    wph.SetCurrentTransformationPlane(new tsm.TransformationPlane());

                    tsm.Contour pb1 = new tsm.Contour();
                    tsm.Contour pb2 = new tsm.Contour();
                    tsm.Contour cplate = new tsm.Contour();

                    for (int i = 0; i < pb.Contour.ContourPoints.Count - 1; i++)
                    {
                        tsm.ContourPoint cp1 = pb.Contour.ContourPoints[i] as tsm.ContourPoint;
                        tsm.ContourPoint cp2 = pb.Contour.ContourPoints[i + 1] as tsm.ContourPoint;

                        tsm.TransformationPlane tp = new tsm.TransformationPlane();
                        GetTP(ref tp, cp1, cp2);
                        tsg.CoordinateSystem LocalCS = new tsg.CoordinateSystem();
                        CS(ref LocalCS, cp1, cp2);
                        tsg.Matrix matrixLocal = tsg.MatrixFactory.ToCoordinateSystem(LocalCS);
                        wph.SetCurrentTransformationPlane(tp);
                        //new tsm.Model().CommitChanges();
                        //mes("A");
                        if (i != 0)
                        {
                            cp1.Chamfer.Type = tsm.Chamfer.ChamferTypeEnum.CHAMFER_ROUNDING;
                            cp1.Chamfer.X = 220;
                        }

                        tsg.Point p0 = new tsg.Point(0, -1 * (height_full * 0.5), 0);
                        tsm.ContourPoint lcp0 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(p0), cp1.Chamfer);
                        pb1.AddContourPoint(lcp0);
                        p0.Y += (height_full);
                        lcp0 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(p0), cp1.Chamfer);
                        pb2.AddContourPoint(lcp0);

                        tsg.Point cpl0 = new tsg.Point(0, -1 * (height_full * 0.5 - t_web - 0.5), 0);
                        tsg.Point cpl1 = new tsg.Point(0, (height_full * 0.5 - t_web + 0.5), 0);

                        tsm.ContourPoint lcpPL0 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl0), cp1.Chamfer);
                        tsm.ContourPoint lcpPL1 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl1), cp1.Chamfer);

                        cplate.AddContourPoint(lcpPL0);
                        cplate.AddContourPoint(lcpPL1);

                        if (i == pb.Contour.ContourPoints.Count - 2)
                        {
                            cp2.Chamfer = cp1.Chamfer;
                            tsg.Point p1 = matrixLocal.Transform(new tsg.Point(cp2.X, cp2.Y - 0.5, cp2.Z));
                            p1.Y += -1 * (height_full * 0.5);
                            tsm.ContourPoint lcp1 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(p1), cp2.Chamfer);
                            pb1.AddContourPoint(lcp1);
                            p1.Y += (height_full);
                            lcp1 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(p1), cp2.Chamfer);
                            pb2.AddContourPoint(lcp1);

                            tsg.Point cpl20 = new tsg.Point(p1.X, -1 * (height_full * 0.5 - t_web - 0.5), 0);
                            tsg.Point cpl21 = new tsg.Point(p1.X, (height_full * 0.5 - t_web + 0.5), 0);

                            tsm.ContourPoint lcpPL20 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl20), new tsm.Chamfer());
                            tsm.ContourPoint lcpPL21 = new tsm.ContourPoint(tp.TransformationMatrixToGlobal.Transform(cpl21), new tsm.Chamfer());

                            cplate.AddContourPoint(lcpPL20);
                            cplate.AddContourPoint(lcpPL21);

                        }
                        wph.SetCurrentTransformationPlane(ctp);

                    }
                    npb.Contour = pb1;
                    npb.Insert();
                    npb.Position.Depth = tsm.Position.DepthEnum.BEHIND;
                    for (int i = 0; i < pb2.ContourPoints.Count; i++)
                    {
                        tsm.ContourPoint cp = pb.Contour.ContourPoints[i] as tsm.ContourPoint;
                        if (!(i == 0 || i == pb.Contour.ContourPoints.Count - 1))
                        {
                            cp.Chamfer.Type = tsm.Chamfer.ChamferTypeEnum.CHAMFER_ROUNDING;
                            cp.Chamfer.X = 20;
                        }
                    }
                    npb.Contour = pb2;
                    npb.Insert();
                    tsm.ContourPlate PLATE = new tsm.ContourPlate();
                    tsm.Contour cplateSorting = new tsm.Contour();
                    for (int i = 0; i < cplate.ContourPoints.Count; i++)
                    {
                        cplateSorting.AddContourPoint(cplate.ContourPoints[i] as tsm.ContourPoint);
                        i++;
                    }
                    for (int i = cplate.ContourPoints.Count - 1; i > 0; i--)
                    {
                        cplateSorting.AddContourPoint(cplate.ContourPoints[i] as tsm.ContourPoint);
                        i--;
                    }

                    PLATE.Contour = cplateSorting;
                    PLATE.Class = pb.Class;
                    PLATE.Material = pb.Material;
                    PLATE.Name = "�����";
                    PLATE.Profile.ProfileString = $"PL{t1_flange}";

                    PLATE.AssemblyNumber.Prefix = pb.AssemblyNumber.Prefix;
                    PLATE.AssemblyNumber.StartNumber = pb.AssemblyNumber.StartNumber;
                    PLATE.PartNumber.Prefix = pb.AssemblyNumber.Prefix;
                    PLATE.PartNumber.StartNumber = pb.PartNumber.StartNumber;

                    //PLATE.Type = tsm.ContourPlate.ContourPlateTypeEnum.PLATE;

                    PLATE.Insert();

                    //mes(npb.Insert());
                    // wph.SetCurrentTransformationPlane(ctp);
                }
            }
            wph.SetCurrentTransformationPlane(ctp);
            new tsm.Model().CommitChanges();
        }


        public static void CS(ref tsg.CoordinateSystem cs, tsm.ContourPoint cp1, tsm.ContourPoint cp2)
        {
            tsm.Beam b = new tsm.Beam();
            b.StartPoint = new tsg.Point(cp1.X, cp1.Y, cp1.Z);
            b.EndPoint = new tsg.Point(cp2.X, cp2.Y, cp2.Z);
            b.Profile.ProfileString = "PL1*1";
            b.Insert();
            cs = b.GetCoordinateSystem();
            b.Delete();
        }

        public static void CS(ref tsg.CoordinateSystem cs, tsg.Point cp1, tsg.Point cp2)
        {
            tsm.Beam b = new tsm.Beam();
            b.StartPoint = new tsg.Point(cp1.X, cp1.Y, cp1.Z);
            b.EndPoint = new tsg.Point(cp2.X, cp2.Y, cp2.Z);
            b.Profile.ProfileString = "PL1*1";
            b.Insert();
            cs = b.GetCoordinateSystem();
            b.Delete();
        }

        public static void GetTP(ref tsm.TransformationPlane tp, tsm.ContourPoint cp1, tsm.ContourPoint cp2)
        {
            tsm.Beam b = new tsm.Beam();
            b.StartPoint = new tsg.Point(cp1.X, cp1.Y, cp1.Z);
            b.EndPoint = new tsg.Point(cp2.X, cp2.Y, cp2.Z);
            b.Profile.ProfileString = "PL1*1";
            b.Insert();
            tp = new tsm.TransformationPlane(b.GetCoordinateSystem());
            b.Delete();
        }

        public static void GetTP(ref tsm.TransformationPlane tp, tsg.Point cp1, tsg.Point cp2)
        {
            tsm.Beam b = new tsm.Beam();
            b.StartPoint = new tsg.Point(cp1.X, cp1.Y, cp1.Z);
            b.EndPoint = new tsg.Point(cp2.X, cp2.Y, cp2.Z);
            b.Profile.ProfileString = "PL1*1";
            b.Insert();
            tp = new tsm.TransformationPlane(b.GetCoordinateSystem());
            b.Delete();
        }

        static string[] ark_split(string s, char r = '*')
        {
            return (s.Split(new char[] { r }));
        }
    }
}