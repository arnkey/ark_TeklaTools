﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;

namespace ArkLib.ark_Tekla.Geometry
{
    public static class ark_New
    {
        public static tsg.Point POINT_ZERO
        {
            get { return new tsg.Point(0, 0, 0); }
        }
        public static tsg.Vector VECTOR_X
        {
            get { return new tsg.Vector(1,0,0); }
        }
        public static tsg.Vector VECTOR_Y
        {
            get { return new tsg.Vector(0, 1, 0); }
        }
        public static tsg.Vector VECTOR_Z
        {
            get { return new tsg.Vector(0, 0, 1); }
        }

        public static tsm.TransformationPlane TP_GLOBAL
        {
            get { return new tsm.TransformationPlane(CS_GLOBAL); }
        }
        public static tsg.CoordinateSystem CS_GLOBAL
        {
            get { return new tsg.CoordinateSystem(POINT_ZERO, VECTOR_X, VECTOR_Y); }
        }
    }
}
