﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;
using System.Collections;

namespace ArkLib.ark_Tekla.MyAlgoritms
{
    public static class Offset
    {
        public static tsm.ModelObject Start(tsm.ModelObject mobj, double offset)
        {
            if (mobj is tsm.Beam) return Beam(mobj, offset);
            else if (mobj is tsm.ContourPlate) return CP(mobj, offset);
            else if (mobj is tsm.PolyBeam) return PolyBeam(mobj, offset);
            else
                return null;
        }
        private static tsm.ModelObject Beam(tsm.ModelObject mobj, double offset)
        {
            tsm.Beam b = mobj as tsm.Beam;
            b.StartPoint.X -= offset;
            b.EndPoint.X += offset;
            return b as tsm.ModelObject;
        }
        private static tsm.ModelObject PolyBeam(tsm.ModelObject mobj, double offset)
        {
            tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();
            tsm.TransformationPlane tp = ArkLib.ark_Tekla.Geometry.ark_New.TP_GLOBAL;
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            wph.SetCurrentTransformationPlane(tp);
            tsm.PolyBeam pb = mobj as tsm.PolyBeam;
            ArrayList al2 = iCountor(pb.Contour.ContourPoints, offset);
            pb.Contour.ContourPoints.Clear();
            for (int i = 0; i < al2.Count; i++)
                pb.Contour.AddContourPoint(al2[i] as tsm.ContourPoint);
            wph.SetCurrentTransformationPlane(ctp);
            return pb as tsm.ModelObject;
        }
        private static tsm.ModelObject CP(tsm.ModelObject mobj, double offset)
        {
            tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();
            tsm.TransformationPlane tp = ArkLib.ark_Tekla.Geometry.ark_New.TP_GLOBAL;
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            wph.SetCurrentTransformationPlane(tp);
            tsm.ContourPlate cp = mobj as tsm.ContourPlate;
            ArrayList al2 = iCountor(cp.Contour.ContourPoints, offset);
            cp.Contour.ContourPoints.Clear();
            for (int i = 0; i < al2.Count; i++)
                cp.Contour.AddContourPoint(al2[i] as tsm.ContourPoint);
            wph.SetCurrentTransformationPlane(ctp);
            return cp as tsm.ModelObject;
        }
        private static ArrayList iCountor(ArrayList al, double offset)
        {
            ArrayList al2 = new ArrayList();
            for (int i = 0; i < al.Count; i++)
            {
                tsm.ContourPoint c = al[i] as tsm.ContourPoint;
                c.X += offset;
                c.Y += offset;
                al2.Add(c);
            }
            return al2;
        }
    }
}
