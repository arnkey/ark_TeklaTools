﻿namespace Components_Tekla
{
    partial class Ladder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.profile_main = new System.Windows.Forms.TextBox();
            this.btn_main = new System.Windows.Forms.Button();
            this.btn_step = new System.Windows.Forms.Button();
            this.profile_step = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_create = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_h1 = new System.Windows.Forms.TextBox();
            this.txt_h3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chk_AutoStep = new System.Windows.Forms.CheckBox();
            this.txt_width = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_second = new System.Windows.Forms.Button();
            this.profile_second = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_h2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_dx = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Профиль тетивы 1";
            // 
            // profile_main
            // 
            this.profile_main.Location = new System.Drawing.Point(135, 40);
            this.profile_main.Name = "profile_main";
            this.profile_main.ReadOnly = true;
            this.profile_main.Size = new System.Drawing.Size(125, 20);
            this.profile_main.TabIndex = 1;
            this.profile_main.TextChanged += new System.EventHandler(this.profile_main_TextChanged);
            // 
            // btn_main
            // 
            this.btn_main.Location = new System.Drawing.Point(266, 38);
            this.btn_main.Name = "btn_main";
            this.btn_main.Size = new System.Drawing.Size(75, 23);
            this.btn_main.TabIndex = 2;
            this.btn_main.Text = "Выбрать";
            this.btn_main.UseVisualStyleBackColor = true;
            this.btn_main.Click += new System.EventHandler(this.btn_main_Click);
            // 
            // btn_step
            // 
            this.btn_step.Location = new System.Drawing.Point(267, 96);
            this.btn_step.Name = "btn_step";
            this.btn_step.Size = new System.Drawing.Size(75, 23);
            this.btn_step.TabIndex = 5;
            this.btn_step.Text = "Выбрать";
            this.btn_step.UseVisualStyleBackColor = true;
            this.btn_step.Click += new System.EventHandler(this.btn_step_Click);
            // 
            // profile_step
            // 
            this.profile_step.Location = new System.Drawing.Point(136, 98);
            this.profile_step.Name = "profile_step";
            this.profile_step.ReadOnly = true;
            this.profile_step.Size = new System.Drawing.Size(125, 20);
            this.profile_step.TabIndex = 4;
            this.profile_step.TextChanged += new System.EventHandler(this.profile_step_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Профиль ступени";
            // 
            // btn_create
            // 
            this.btn_create.Location = new System.Drawing.Point(15, 261);
            this.btn_create.Name = "btn_create";
            this.btn_create.Size = new System.Drawing.Size(327, 23);
            this.btn_create.TabIndex = 6;
            this.btn_create.Text = "Создать";
            this.btn_create.UseVisualStyleBackColor = true;
            this.btn_create.Click += new System.EventHandler(this.btn_create_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "h1";
            // 
            // txt_h1
            // 
            this.txt_h1.Location = new System.Drawing.Point(135, 131);
            this.txt_h1.Name = "txt_h1";
            this.txt_h1.Size = new System.Drawing.Size(207, 20);
            this.txt_h1.TabIndex = 8;
            this.txt_h1.Text = "1000";
            this.txt_h1.TextChanged += new System.EventHandler(this.txt_h1_TextChanged);
            // 
            // txt_h3
            // 
            this.txt_h3.Location = new System.Drawing.Point(135, 209);
            this.txt_h3.Name = "txt_h3";
            this.txt_h3.Size = new System.Drawing.Size(151, 20);
            this.txt_h3.TabIndex = 10;
            this.txt_h3.Text = "300";
            this.txt_h3.TextChanged += new System.EventHandler(this.txt_h3_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "h3";
            // 
            // chk_AutoStep
            // 
            this.chk_AutoStep.AutoSize = true;
            this.chk_AutoStep.Checked = true;
            this.chk_AutoStep.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_AutoStep.Location = new System.Drawing.Point(292, 212);
            this.chk_AutoStep.Name = "chk_AutoStep";
            this.chk_AutoStep.Size = new System.Drawing.Size(50, 17);
            this.chk_AutoStep.TabIndex = 11;
            this.chk_AutoStep.Text = "Авто";
            this.chk_AutoStep.UseVisualStyleBackColor = true;
            this.chk_AutoStep.CheckedChanged += new System.EventHandler(this.chk_AutoStep_CheckedChanged);
            // 
            // txt_width
            // 
            this.txt_width.Location = new System.Drawing.Point(135, 157);
            this.txt_width.Name = "txt_width";
            this.txt_width.Size = new System.Drawing.Size(207, 20);
            this.txt_width.TabIndex = 13;
            this.txt_width.Text = "700";
            this.txt_width.TextChanged += new System.EventHandler(this.txt_width_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "w";
            // 
            // btn_second
            // 
            this.btn_second.Location = new System.Drawing.Point(266, 67);
            this.btn_second.Name = "btn_second";
            this.btn_second.Size = new System.Drawing.Size(75, 23);
            this.btn_second.TabIndex = 16;
            this.btn_second.Text = "Выбрать";
            this.btn_second.UseVisualStyleBackColor = true;
            this.btn_second.Click += new System.EventHandler(this.button1_Click);
            // 
            // profile_second
            // 
            this.profile_second.Location = new System.Drawing.Point(135, 69);
            this.profile_second.Name = "profile_second";
            this.profile_second.ReadOnly = true;
            this.profile_second.Size = new System.Drawing.Size(125, 20);
            this.profile_second.TabIndex = 15;
            this.profile_second.TextChanged += new System.EventHandler(this.profile_second_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Профиль тетивы 2";
            // 
            // txt_h2
            // 
            this.txt_h2.Location = new System.Drawing.Point(135, 183);
            this.txt_h2.Name = "txt_h2";
            this.txt_h2.Size = new System.Drawing.Size(207, 20);
            this.txt_h2.TabIndex = 18;
            this.txt_h2.Text = "-1";
            this.txt_h2.TextChanged += new System.EventHandler(this.txt_h2_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "h2";
            // 
            // txt_dx
            // 
            this.txt_dx.Location = new System.Drawing.Point(135, 235);
            this.txt_dx.Name = "txt_dx";
            this.txt_dx.Size = new System.Drawing.Size(206, 20);
            this.txt_dx.TabIndex = 20;
            this.txt_dx.Text = "-1";
            this.txt_dx.TextChanged += new System.EventHandler(this.txt_dx_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 238);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "dx";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 3);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 21;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(139, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 22;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(267, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Sv";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(313, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(29, 23);
            this.button2.TabIndex = 24;
            this.button2.Text = "Ld";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Ladder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 292);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.txt_dx);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_h2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_second);
            this.Controls.Add(this.profile_second);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_width);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chk_AutoStep);
            this.Controls.Add(this.txt_h3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_h1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_create);
            this.Controls.Add(this.btn_step);
            this.Controls.Add(this.profile_step);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_main);
            this.Controls.Add(this.profile_main);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Ladder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ladder-L";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Ladder_FormClosing);
            this.Load += new System.EventHandler(this.Ladder_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox profile_main;
        private System.Windows.Forms.Button btn_main;
        private System.Windows.Forms.Button btn_step;
        private System.Windows.Forms.TextBox profile_step;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_create;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_h1;
        private System.Windows.Forms.TextBox txt_h3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chk_AutoStep;
        private System.Windows.Forms.TextBox txt_width;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_second;
        private System.Windows.Forms.TextBox profile_second;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_h2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_dx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}