﻿using System;
using System.Collections.Generic;

using Tekla.Structures.Model;
using UI = Tekla.Structures.Model.UI;
using tsg = Tekla.Structures.Geometry3d;

namespace ark_TeklaTools
{
	public class xSuhari
	{
		public xSuhari()
		{
			Model model = new Model();
			ModelObjectEnumerator moe = new UI.ModelObjectSelector().GetSelectedObjects();
			while (moe.MoveNext())
			{
				BoltGroup bg = moe.Current as BoltGroup;
				//
				if (bg.SlottedHoleX > 0)
				{
					bg.Nut1 = true;
					bg.Nut2 = true;
					bg.Washer1 = true;
					bg.Washer3 = true;
					bg.ExtraLength = 1;
					bg.SetUserProperty("Comment", "oval");
					bg.Modify();
					continue;
				}
				else if (bg.BoltStandard == "Bolt_HighStrength")
				{
					bg.SetUserProperty("Comment", "high");
					continue;
				}
				else
				{
					string komment = ""; bg.GetUserProperty("Comment", ref komment);

					bg.Washer1 = true;
					bg.Washer2 = true;
					bg.Washer3 = false;
					bg.Nut1 = true;
					bg.Nut2 = false;
					if (komment != "Splice")
					{
						bg.SetUserProperty("Comment", "grover");
					}

					if (bg.BoltSize == 16)
					{
						bg.ExtraLength = 15;
					}
					else
					{
						bg.ExtraLength = 19;
					}
					bg.Modify();
				}
			}
		}
		public void xSuhari2()
		{
			Model model = new Model();
			ModelObjectEnumerator moe = new UI.ModelObjectSelector().GetSelectedObjects();
			while (moe.MoveNext())
			{
				Part p = moe.Current as Part;
				string f = p.Name;

				if (p.Identifier.ID == p.GetAssembly().GetMainPart().Identifier.ID)
				{

					if (f.Contains("BRACE")) p.Name = "Связь";
					if (f.Contains("COLUMN")) p.Name = "Колонна";
					if (f.Contains("BEAM")) p.Name = "Балка";
				}
				else {
					p.Name = "Деталь";
				}
				p.Modify();

			}
		}
	}
}
