﻿using System;
using Tekla.Structures.Geometry3d;

namespace ArkLib.ark_Tekla.Geometry
{
    [Serializable]
    public class ark_Point
    {
        public double x;
        public double y;
        public double z;

        public Point Tekla
        {
            get
            {
                return new Point(x, y, z);
            }
        }

        public ark_Point()
        {
            x = 0;
            y = 0;
            z = 0;
        }
        public ark_Point(double _x, double _y, double _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }
        public ark_Point(double _x, double _y)
        {
            x = _x;
            y = _y;
            z = 0;
        }
        public ark_Point(double _x)
        {
            x = _x;
            y = 0;
            z = 0;
        }
        public ark_Point(ark_Point P)
        {
            x = P.x;
            y = P.y;
            z = P.z;
        }
        public ark_Point(Point P)
        {
            x = P.X;
            y = P.Y;
            z = P.Z;
        }
        public override string ToString()
        {
            return (x + " " + y + " " + z);
        }
    }
}
