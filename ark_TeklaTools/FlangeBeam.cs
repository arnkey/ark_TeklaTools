﻿
using System;
using Tekla.Structures.Model;
using Weld = ArkLib.ark_Tekla.MyAlgoritms.Plugins.Helper.Weld;

/* 
 * TODO: Сделать в зависимости от высоты и положения относительно друг друга 
 * TODO: Сделать разделку кромок относительно толщины!
 */

namespace ArkLib.ark_Tekla.MyAlgoritms.Plugins
{
    public class FlangeBeam
    {

        public FlangeBeam(ModelObject PrimaryBeam, ModelObject SecondaryBeam, Weld w)
        {
            try
            {
                PrimaryBeam.Select();
                SecondaryBeam.Select();

                double h1 = 0; PrimaryBeam.GetReportProperty("PROFILE.HEIGHT", ref h1);
                double h2 = 0; SecondaryBeam.GetReportProperty("PROFILE.HEIGHT", ref h2);
                double r1 = 0; PrimaryBeam.GetReportProperty("PROFILE.ROUNDING_RADIUS_1", ref r1);
                r1 = MyAlgoritms.Differents.Round(r1, 5);

                double f1_up = r1;
                double f1_down = (h2 == h1 ? r1 : int.MinValue);

                Component cnt = new Component();
                cnt.Name = "x_FlangeBeam_Welded";
                cnt.Number = Component.PLUGIN_OBJECT_NUMBER;

                ComponentInput ci = new ComponentInput();
                ci.AddInputObject(PrimaryBeam);
                ci.AddInputObject(SecondaryBeam);

                //ci.AddInputObjects(new System.Collections.ArrayList() { PrimaryBeam, SecondaryBeam });

                cnt.SetComponentInput(ci);
 //               System.Windows.Forms.MessageBox.Show(b.ToString());
                //cnt.SetPrimaryObject(PrimaryBeam);
                //cnt.SetSecondaryObject(SecondaryBeam);

                cnt.LoadAttributesFromFile("standard");

                #region Настройки

                // Общие

                cnt.SetAttribute("ft_gap", 1);
                cnt.SetAttribute("bc_nt_dimX_t", 1);
                cnt.SetAttribute("bc_nt_dimY_t", 1);
                cnt.SetAttribute("bc_tr_radius", 0);
                cnt.SetAttribute("bc_nt_dimX_b", 1);
                cnt.SetAttribute("bc_nt_dimY_b", 1);
                cnt.SetAttribute("bc_nt_dimY_t", 1);
                cnt.SetAttribute("bc_nt_typeY_t", 2);
                cnt.SetAttribute("bc_nt_typeY_b", 2);
                cnt.SetAttribute("con_cutWeb", 1);
                cnt.SetAttribute("bc_nt_create", 2);
                cnt.SetAttribute("bc_nt_radius", 0);
                cnt.SetAttribute("ch_distX_T", f1_up);
                cnt.SetAttribute("ch_distY_T", f1_up);
                cnt.SetAttribute("ch_distY_T", f1_down);
                cnt.SetAttribute("ch_distX_T", f1_down);

                double w_temp = (w == Weld.Shop ? 1 : 0);

                 //Сварка
                //cnt.SetAttribute("w1_SizeAbove", 1); // катет
                //cnt.SetAttribute("w1_TypeAbove", 10);
                //cnt.SetAttribute("w1_TypeBelow", 0);
                //cnt.SetAttribute("w1_ContourAbove", 0);
                //cnt.SetAttribute("w1_ContourBelow", 0);
                //cnt.SetAttribute("w1_FinishAbove", 0);
                //cnt.SetAttribute("w1_FinishBelow", 0);
                //cnt.SetAttribute("w1_Around", 0);
                //cnt.SetAttribute("w1_Shop", w_temp);
                //cnt.SetAttribute("w1_Stich", 0);
                //cnt.SetAttribute("w1_SubAss", 0);
                //cnt.SetAttribute("w1_Change", 0);

                //cnt.SetAttribute("w2_SizeAbove", 1); // катет
                //cnt.SetAttribute("w2_TypeAbove", 10);
                //cnt.SetAttribute("w2_TypeBelow", 0);
                //cnt.SetAttribute("w2_ContourAbove", 0);
                //cnt.SetAttribute("w2_ContourBelow", 0);
                //cnt.SetAttribute("w2_FinishAbove", 0);
                //cnt.SetAttribute("w2_FinishBelow", 0);
                //cnt.SetAttribute("w2_Around", 0);
                //cnt.SetAttribute("w2_Shop", w_temp);
                //cnt.SetAttribute("w2_Stich", 0);
                //cnt.SetAttribute("w2_SubAss", 0);
                //cnt.SetAttribute("w2_Change", 0);

                //cnt.SetAttribute("w3_SizeAbove", 1); // катет
                //cnt.SetAttribute("w3_TypeAbove", 10);
                //cnt.SetAttribute("w3_TypeBelow", 0);
                //cnt.SetAttribute("w3_ContourAbove", 0);
                //cnt.SetAttribute("w3_ContourBelow", 0);
                //cnt.SetAttribute("w3_FinishAbove", 0);
                //cnt.SetAttribute("w3_FinishBelow", 0);
                //cnt.SetAttribute("w3_Around", 0);
                //cnt.SetAttribute("w3_Shop", w_temp);
                //cnt.SetAttribute("w3_Stich", 0);
                //cnt.SetAttribute("w3_SubAss", 0);
                //cnt.SetAttribute("w3_Change", 0);

                #endregion

                //bool b = cnt.Modify();


                bool b = cnt.Insert();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
        }
    }
}
