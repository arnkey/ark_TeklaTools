﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Tekla.Structures.Model;
using Tekla.Structures.Geometry3d;
using ui = Tekla.Structures.Model.UI;
using me = ArkLib.MarkElements;
using x = ArkLib.x_algoritms;
using ArkLib.ark_Tekla;
using ArkLib;
using System.IO;


using System.Runtime.Serialization.Formatters.Binary;

// TODO: сохранение параметров

namespace Components_Tekla
{
    public partial class Ladder : Form
    {
        public Ladder()
        {
            InitializeComponent();

            ls.MainPart = new me.ark_Profile();
            ls.SecondPart = new me.ark_Profile();
            ls.Step = new me.ark_Profile();

            ls.MainPart = new me.ark_Profile();
            ls.MainPart.GetPosition = ArkLib.ark_Tekla.Common.ark_Position.ReturnPositionLadder_L();
            ls.MainPart.Profile = "L75*75*5";
            ls.MainPart.Material = "С235";
            ls.MainPart.ThisName = "Стремянка";
            ls.SecondPart = new me.ark_Profile();
            ls.SecondPart.GetPosition = ArkLib.ark_Tekla.Common.ark_Position.ReturnPositionLadder_L(true);
            ls.SecondPart.Profile = "L75*75*5";
            ls.SecondPart.Material = "С235";
            ls.SecondPart.ThisName = "Стремянка";
            ls.Step = new me.ark_Profile();
            ls.Step.ThisName = "Ступень";
            ls.Step.Material = "С235";
            ls.Step.Profile = "D10";
            ls.Step.GetPosition.Depth = Position.DepthEnum.MIDDLE;
            ls.Step.GetPosition.Rotation = Position.RotationEnum.TOP;
            ls.Step.GetPosition.Plane = Position.PlaneEnum.LEFT;
        }



        void mes(string str)
        {
            MessageBox.Show(str);
        }


        Ladder_ser ls = new Ladder_ser();

        private void btn_create_Click(object sender, EventArgs e)
        {

            ls.width = ArkLib.ark_Tekla.Common.CommonFunctions.ReturnCorrectDouble(txt_width.Text);
            ls.h1 = ArkLib.ark_Tekla.Common.CommonFunctions.ReturnCorrectDouble(txt_h1.Text);
            ls.h2 = ArkLib.ark_Tekla.Common.CommonFunctions.ReturnCorrectDouble(txt_h2.Text);
            ls.h3 = ArkLib.ark_Tekla.Common.CommonFunctions.ReturnCorrectDouble(txt_h3.Text);
            ls.dx = ArkLib.ark_Tekla.Common.CommonFunctions.ReturnCorrectDouble(txt_dx.Text);
            // profile:
            double t = 0;
            double r1 = 0;
            double w = 0;

            List<double> steps = ArkLib.ark_Tekla.Common.TeklaHelper.Parsers.DistanceListToList(txt_h3.Text);

            #region fields

            Model model = new Model();
            WorkPlaneHandler wph = model.GetWorkPlaneHandler();
            TransformationPlane CurrentPlane = wph.GetCurrentTransformationPlane();
           
            #endregion
            ui.Picker picker = new ui.Picker();

            Point Origin = picker.PickPoint("Точка центра");
            Point Direction = picker.PickPoint("Точка направления");

            //wph.SetCurrentTransformationPlane(new TransformationPlane());
            TransformationPlane MyPlane = ArkLib.ark_Tekla.Common.CommonFunctions.GetTransformationPlane(Origin, Direction, ArkLib.ark_Tekla.Common.CommonFunctions.TDirection.z);

            wph.SetCurrentTransformationPlane(MyPlane);

            Beam fl = new Beam();
            Beam sl = new Beam();

            ls.MainPart.ReturnBeam(ref fl);
            ls.SecondPart.ReturnBeam(ref sl);

            if (ls.h2 == -1) ls.h2 = 1250.0;

            #region Тетива



            fl.StartPoint = new Point(0, ls.width * 0.5, ls.h1 + ls.h2);
            fl.EndPoint = new Point(0, ls.width * 0.5, 0);
            fl.Insert();

            sl.StartPoint = new Point(0, -1 * ls.width * 0.5, ls.h1 + ls.h2);
            sl.EndPoint = new Point(0, -1 * ls.width * 0.5, 0);
            sl.Insert();

            CreateWeld(fl, sl);

            #endregion

            #region Ступени
            Beam s = new Beam();
            ls.Step.ReturnBeam(ref s);

            #region Default
            fl.Select();
            // TODO толщина уголка!!! (t);
            fl.GetReportProperty("PROFILE.FLANGE_THICKNESS_1", ref t);
            fl.GetReportProperty("PROFILE.ROUNDING_RADIUS_1", ref r1);
            fl.GetReportProperty("PROFILE.WIDTH", ref w);
            if (w == 0) fl.GetReportProperty("PROFILE.HEIGHT", ref w);

            //mes(t + " " + r1 + " " + w + " ");
            //return;
            #endregion

            s.StartPoint = new Point(t, ls.width * 0.5 - (ls.dx == -1 ? (t + r1) : (w - ls.dx)), ls.h1);
            s.EndPoint = new Point(t, -1 * (ls.width * 0.5 - (ls.dx == -1 ? (t + r1) : (w - ls.dx))), ls.h1);

            if (chk_AutoStep.Checked)
            {
                double st = steps[0];
                double len = 0;
                int step = 0;
                while (true)
                {
                    if (len > ls.h1)
                        break;
                    else
                        step++;
                    len += 300;
                }
                for (int i = 0; i < step; i++)
                {
                    s.Insert();
                    CreateWeld(fl, s);
                    len -= st;
                    s.StartPoint.Z -= st;
                    s.EndPoint.Z -= st;
                }
            }
            else
            {
                s.Insert();
                CreateWeld(fl, s);
                foreach (double st in steps)
                {
                    s.StartPoint.Z -= st;
                    s.EndPoint.Z -= st;
                    s.Insert();
                    CreateWeld(fl, s);
                }
            }
            #endregion

            wph.SetCurrentTransformationPlane(CurrentPlane);
            model.CommitChanges("");
        }


	// TODO: Изменить сварку после создания алгоритма для модели.
        void CreateWeld(Beam mb, Beam sb)
        {
            Weld w = new Weld();
            w.MainObject = mb;
            w.SecondaryObject = sb;
            w.ShopWeld = true;
            w.Insert();
        }

        private void btn_main_Click(object sender, EventArgs e)
        {
            ark_Profile_Form prof = (ls.MainPart == null ? new ark_Profile_Form() : new ark_Profile_Form(ls.MainPart));
            if (prof.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ls.MainPart = prof.ark_profile;
                profile_main.Text = ls.MainPart.Profile;
            }
        }

        private void btn_step_Click(object sender, EventArgs e)
        {
            ark_Profile_Form prof = (ls.Step == null ? new ark_Profile_Form() : new ark_Profile_Form(ls.Step));
            if (prof.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ls.Step = prof.ark_profile;
                profile_step.Text = ls.Step.Profile;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ark_Profile_Form prof = (ls.SecondPart == null ? new ark_Profile_Form() : new ark_Profile_Form(ls.SecondPart));
            if (prof.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ls.SecondPart = prof.ark_profile;
                profile_second.Text = ls.SecondPart.Profile;
            }
        }

        private void chk_AutoStep_CheckedChanged(object sender, EventArgs e)
        {

        }


        #region field config

        string path = "Data";
        string name_file = "";
        string ext = ".arklds.cfg";

        #endregion

        private void Ladder_Load(object sender, EventArgs e)
        {
            if (Directory.Exists(path))
            {
                foreach (string fi in Directory.GetFiles(path, "*" + ext))
                {
                    comboBox1.Items.Add(fi.Replace(ext, "").Replace(@"Data\", ""));
                }
            }
            if (comboBox1.Items.Contains("standard"))
            {
                LoadSettings();
            }
            this.TopMost = true;
        }


        void SaveSettings()
        {
            try
            {
                toGet();
                string fl = textBox1.Text.ToString();
                string fl0 = Path.Combine(path, fl + ext);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                if (File.Exists(fl0))
                {
                    File.Move(fl0, fl0 + ".bak");
                }
                BinaryFormatter binFormat = new BinaryFormatter();
                using (Stream fStream = new FileStream(fl0,
          FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    binFormat.Serialize(fStream, ls);
                }
            }
            catch { }
        }


        void LoadSettings()
        {
            try
            {
                string fl = comboBox1.SelectedItem.ToString();
                string fl0 = Path.Combine(path, fl + ext);

                if (File.Exists(fl0))
                {
                    BinaryFormatter binFormat = new BinaryFormatter();
                    using (Stream fStream = File.OpenRead(fl0))
                    {
                        ls = (Ladder_ser)binFormat.Deserialize(fStream);
                    }
                }
                toSet();
            }
            catch { }
        }

        void toSet()
        {
            txt_dx.Text = ls.dx.ToString();
            txt_h1.Text = ls.h1.ToString();
            txt_h2.Text = ls.h2.ToString();
            txt_h3.Text = ls.h3.ToString();
            txt_width.Text = ls.width.ToString();
            profile_main.Text = ls.MainPart.Profile;
            profile_second.Text = ls.SecondPart.Profile;
            profile_step.Text = ls.Step.Profile;
            textBox1.Text = comboBox1.SelectedItem.ToString();
        }

        void toGet()
        {
            ls.dx = Convert.ToDouble(txt_dx.Text);
            ls.h1 = Convert.ToDouble(txt_h1.Text);
            ls.h2 = Convert.ToDouble(txt_h2.Text);
            ls.h3 = Convert.ToDouble(txt_h3.Text);
            ls.width = Convert.ToDouble(txt_width.Text);
        }

        private void Ladder_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void profile_main_TextChanged(object sender, EventArgs e)
        {
        }

        private void profile_second_TextChanged(object sender, EventArgs e)
        {

        }

        private void profile_step_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_h1_TextChanged(object sender, EventArgs e)
        {
                        try
            {
            ls.h1 = Convert.ToDouble(txt_h1.Text);
                                        }
            catch { }
        }

        private void txt_width_TextChanged(object sender, EventArgs e)
        {
                        try
            {
            ls.width = Convert.ToDouble(txt_width.Text);
                                        }
            catch { }
        }

        private void txt_h2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ls.h2 = Convert.ToDouble(txt_h2.Text);
            }
            catch { }
        }

        private void txt_h3_TextChanged(object sender, EventArgs e)
        {
                        try
            {
            ls.h3 = Convert.ToDouble(txt_h3.Text);
                                        }
            catch { }
        }

        private void txt_dx_TextChanged(object sender, EventArgs e)
        {
                        try
            {
            ls.dx = Convert.ToDouble(txt_dx.Text);
            }
                        catch { }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SaveSettings();
            if (comboBox1.Items.Contains(name_file) == false)
            {
                comboBox1.Items.Add(name_file);
                comboBox1.SelectedItem = name_file;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            name_file = textBox1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoadSettings();
            if (comboBox1.Items.Contains(name_file) == false)
            {
                comboBox1.Items.Add(name_file);
                comboBox1.SelectedItem = name_file;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = comboBox1.SelectedItem.ToString();
        }
    }
}
