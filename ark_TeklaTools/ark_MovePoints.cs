﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;
using ArkLib.ark_Tekla.MyAlgoritms;

namespace ark_TeklaTools
{
    public partial class ark_MovePoints : Form
    {
        public ark_MovePoints()
        {
            InitializeComponent();
        }

        private void ark_MovePoints_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
        }

        private void btn_modify_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
            if (model.GetConnectionStatus() == false) return;
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mos.GetSelectedObjects();
            while (moe.MoveNext())
            {
                if (moe.Current is tsm.Beam)
                {
                    tsm.Beam b = moe.Current as tsm.Beam;
                    double d = Convert.ToDouble(txt_value.Text);

                    Differents.OperationPoint.Position ps = (rb_bottom.Checked == true ? Differents.OperationPoint.Position.Bottom : Differents.OperationPoint.Position.Top);

                    Differents.OperationPoint.Relative rv = Differents.OperationPoint.Relative.dz;

                    if (rb_dx.Checked) rv = Differents.OperationPoint.Relative.dx;
                    if (rb_dy.Checked) rv = Differents.OperationPoint.Relative.dy;
                    if (rb_dz.Checked) rv = Differents.OperationPoint.Relative.dz;

                    tsg.Point point = Differents.OperationPoint.ReturnPoint(
                        b.StartPoint,
                        b.EndPoint,
                        ps,
                        rv);

                    if (point == null)
                    {
                        b.StartPoint.Z += d;
                        b.EndPoint.Z += d;
                    }
                    if (b.StartPoint == point)
                        b.StartPoint.Z += d;
                    else if (b.EndPoint == point)
                        b.EndPoint.Z += d;
                    b.Modify();
                }
            }
            model.CommitChanges();
        }
    }
}
