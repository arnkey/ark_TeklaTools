﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ArkLib.ark_Tekla.Common
{
    public static class TeklaHelper
    {
        /// <summary>
        /// From TS
        /// </summary>
        public static class Parsers
        {
            /// <summary>
            /// Stores the Tekla numberFormatinfo
            /// </summary>
            private static NumberFormatInfo m_TeklaNumberFormat;
            private const string DecimalSeparator = ".";
            private const string GroupSeparator = ",";

            /// <summary>
            /// Initializes the class so we have the "Tekla" numberFormatInfo available
            /// </summary>
            static Parsers()
            {
                m_TeklaNumberFormat = new NumberFormatInfo();
                m_TeklaNumberFormat.NumberDecimalSeparator = DecimalSeparator;
                m_TeklaNumberFormat.NumberGroupSeparator = GroupSeparator;
                m_TeklaNumberFormat.NumberDecimalSeparator = DecimalSeparator;
                m_TeklaNumberFormat.NumberGroupSeparator = GroupSeparator;
                m_TeklaNumberFormat.CurrencyDecimalSeparator = DecimalSeparator;
                m_TeklaNumberFormat.CurrencyGroupSeparator = GroupSeparator;
                m_TeklaNumberFormat.PercentDecimalSeparator = DecimalSeparator;
                m_TeklaNumberFormat.PercentGroupSeparator = GroupSeparator;
            }

            public static List<double> DistanceListToList(string text)
            {
                string[] chunks = text.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                List<double> distList = new List<double>();
                for (int i = 0; i < chunks.GetLength(0); i++)
                {
                    if (chunks[i].IndexOf("*") > -1)
                    {
                        int multiplier = ParseInt(chunks[i].Split(new char[] { '*' })[0], 1);
                        double distance = ParseDouble(chunks[i].Split(new char[] { '*' })[1]);
                        for (int j = 0; j < multiplier; j++)
                        {
                            distList.Add(distance);
                        }
                    }
                    else
                    {
                        double val = ParseDouble(chunks[i], double.MinValue);
                        if (val != double.MinValue)
                            distList.Add(val);
                    }
                }
                return distList;
            }

            public static double[] DistanceListToArray(string text)
            {
                return DistanceListToList(text).ToArray();
            }

            public static double ParseDouble(string text)
            {
                return ParseDouble(text, 0);
            }

            public static double ParseDouble(string text, double def)
            {
                try
                {
                    return double.Parse(text, m_TeklaNumberFormat);
                }
                catch
                {
                    return def;
                }
            }

            public static int ParseInt(string text)
            {
                return ParseInt(text, 0);
            }

            public static int ParseInt(string text, int def)
            {
                try
                {
                    return int.Parse(text, m_TeklaNumberFormat);
                }
                catch
                {
                    return def;
                }
            }
        }
    }
}
