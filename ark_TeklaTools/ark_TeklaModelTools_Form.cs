﻿using System;
using System.Windows.Forms;

using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;
using ui = Tekla.Structures.Model.UI;

using com = ArkLib.ark_Tekla.Common;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

namespace ark_TeklaTools
{
    public partial class ark_TeklaModelTools_Form : Form
    {
        #region messages
        void mes()
        {
            mes("Готово!");
        }
        void mes(string m)
        {
            MessageBox.Show(m);
        }
        void mes(int s)
        {
            mes(s.ToString());
        }
        void mes(double s)
        {
            mes(s.ToString());
        }
        void mes(bool b)
        {
            mes(b.ToString());
        }
        #endregion

        public ark_TeklaModelTools_Form()
        {
            InitializeComponent();

        }

        private void ark_TeklaTools_Form_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
		   // tabControl1.TabPages.Remove(tabControl1.TabPages["debug"]);
           // tabControl1.TabPages.Remove(tabControl1.TabPages["prj"]);
           // tabControl1.TabPages.Remove(tabControl1.TabPages["SubProgramms"]);

           // SubProgramms
            button4.Text = "Проекция болтов";
        }

        private void ark_BeamToTop_button_Click(object sender, EventArgs e)
        {
            ark_BeamToTop.ark_BeamToTop_Form f = new ark_BeamToTop.ark_BeamToTop_Form();
            f.Show();
        }


        public static tsm.ModelObjectEnumerator GetModelObjects
        {
            get
            {
                tsm.Model model = new tsm.Model();
                ui.ModelObjectSelector mos = new ui.ModelObjectSelector();
                return mos.GetSelectedObjects();
            }
        }


        private void prjOnPart_Click(object sender, EventArgs e)
        {
            tsm.ModelObjectEnumerator moe = GetModelObjects;
            
            tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            tsm.TransformationPlane tp = ArkLib.ark_Tekla.Geometry.ark_New.TP_GLOBAL;
            wph.SetCurrentTransformationPlane(tp);
            tsm.UI.Picker picker = new tsm.UI.Picker();
            tsm.Part b1 = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_PART) as tsm.Part;
			tsg.LineSegment ls1 = new tsg.LineSegment(b1.GetReferenceLine(false)[0] as tsg.Point, b1.GetReferenceLine(false)[b1.GetReferenceLine(false).Count - 1] as tsg.Point);
            while (moe.MoveNext())
            {
				tsg.Line bls1 = new tsg.Line(ls1.Point1, ls1.Point2);

				if (moe.Current is tsm.PolyBeam)
				{
					tsm.PolyBeam pb = moe.Current as tsm.PolyBeam;
					for (int i = 0; i < pb.Contour.ContourPoints.Count; i++)
					{
						tsm.ContourPoint cp = pb.Contour.ContourPoints[i] as tsm.ContourPoint;

						tsg.Point ip1 = new tsg.Point(cp.X, cp.Y, cp.Z);
						tsg.Point p1 = new tsg.Point(cp.X, cp.Y, cp.Z + 1000);
						tsg.Line l1 = new tsg.Line(ip1, p1);

						tsg.Point result = tsg.Intersection.LineToLine(l1, bls1).Point1;

						(pb.Contour.ContourPoints[i] as tsm.ContourPoint).X = result.X;
						(pb.Contour.ContourPoints[i] as tsm.ContourPoint).Y = result.Y;
						(pb.Contour.ContourPoints[i] as tsm.ContourPoint).Z = result.Z;
					}
					pb.Modify();
					continue;
				}


				if (moe.Current is tsm.Beam)
				{
					tsm.Beam bm = moe.Current as tsm.Beam;
					tsg.Point bm1 = new tsg.Point((bm.GetReferenceLine(false)[0] as tsg.Point)); bm1.Z += 1000;
					tsg.Point bm2 = new tsg.Point((bm.GetReferenceLine(false)[bm.GetReferenceLine(false).Count - 1] as tsg.Point)); bm2.Z += 1000;
					tsg.Line beam1 = new tsg.Line((bm.GetReferenceLine(false)[0] as tsg.Point), bm1);
					tsg.Line beam2 = new tsg.Line((bm.GetReferenceLine(false)[bm.GetReferenceLine(false).Count - 1] as tsg.Point), bm2);

					bm.StartPoint.X = tsg.Intersection.LineToLine(beam1, bls1).Point1.X;
					bm.StartPoint.Y = tsg.Intersection.LineToLine(beam1, bls1).Point1.Y;
					bm.StartPoint.Z = tsg.Intersection.LineToLine(beam1, bls1).Point1.Z;


					bm.EndPoint.X = tsg.Intersection.LineToLine(beam2, bls1).Point1.X;
					bm.EndPoint.Y = tsg.Intersection.LineToLine(beam2, bls1).Point1.Y;
					bm.EndPoint.Z = tsg.Intersection.LineToLine(beam2, bls1).Point1.Z;
					bm.Modify();
				}


                if (moe.Current is tsm.CustomPart)
                {
                    tsm.CustomPart cp = moe.Current as tsm.CustomPart;

                    tsg.Point cp1 = new tsg.Point();
                    tsg.Point cp2 = new tsg.Point();

                    cp.GetStartAndEndPositions(ref cp1, ref cp2);

                    tsg.Point p1 = new tsg.Point(cp1);
                    tsg.Point p2 = new tsg.Point(cp2);

                    p1.Z += 1000; p2.Z += 1000;

                    tsg.Line l1 = new tsg.Line(cp1, p1);
                    tsg.Line l2 = new tsg.Line(cp2, p2);

                    tsg.Point prj1 = tsg.Intersection.LineToLine(l1, bls1).Point1;
                    tsg.Point prj2 = tsg.Intersection.LineToLine(l2, bls1).Point1;

                    cp.SetInputPositions(prj1, prj2);

                    cp.Modify();
                }

            }
            wph.SetCurrentTransformationPlane(ctp);
            //com.FastFunctions.ModelCommitChanges();
            new tsm.Model().CommitChanges();
            mes();
        }
        
        private void SetMainPart_Click(object sender, EventArgs e)
        {
            tsm.ModelObjectEnumerator moe = GetModelObjects;
            while (moe.MoveNext())
            {
                if (moe.Current is tsm.Part)
                {
                    tsm.Part part = moe.Current as tsm.Part;
                    tsm.Assembly asm = part.GetAssembly();
                    asm.SetMainPart(part);
                    asm.Modify();
                }
            }
            new tsm.Model().CommitChanges();
        }

        private void SetHandRail_Click(object sender, EventArgs e)
        {
            tsm.UI.Picker picker = new tsm.UI.Picker();
            tsm.Model model = new tsm.Model();
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mos.GetSelectedObjects();
            if (moe.GetSize() > 0)
            {
                while (moe.MoveNext())
                {
                    if (moe.Current is tsm.Beam)
                    {
                        tsm.Beam Beam = moe.Current as tsm.Beam;
                        ArkLib.ark_Tekla.MyAlgoritms.Plugins.x_Handrail hand = new ArkLib.ark_Tekla.MyAlgoritms.Plugins.x_Handrail(Beam);
                    }
                }
            }
            else
            {
                tsm.Beam Beam = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_PART, "") as tsm.Beam;

                ArkLib.ark_Tekla.MyAlgoritms.Plugins.x_Handrail hand = new ArkLib.ark_Tekla.MyAlgoritms.Plugins.x_Handrail(Beam);
            }
            model.CommitChanges();
        }

        private void btn_SelectSingleAssembly_Click(object sender, EventArgs e)
        {
            ArrayList al = new ArrayList();
            tsm.Model model = new tsm.Model();
            if (model.GetConnectionStatus() == false) return;
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mos.GetSelectedObjects();
            while (moe.MoveNext())
            {
                if (moe.Current is tsm.Assembly)
                {
                    tsm.Assembly asm = moe.Current as tsm.Assembly;
                    if (asm.GetSecondaries().Count == 0) al.Add(asm);
                }
            }
            mos.Select(al);
        }

        private void ark_MoveObjects_button_Click(object sender, EventArgs e)
        {
            // TODO: FIX
            //ark_BeamToTop.ark_BeamToTop_Form f = new ark_BeamToTop.ark_BeamToTop_Form();
            //ark_MoveObjects.Form1 f = new ark_MoveObjects.Form1();
            //f.Show();
            MessageBox.Show("Функция временно отключена");
        }

        private void btn_SwapHandles_Click(object sender, EventArgs e)
        {
            SwapHandles sh = new SwapHandles();
        }



        private void btn_PartOffset_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
            if (model.GetConnectionStatus() == false) return;
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mos.GetSelectedObjects();
            while (moe.MoveNext())
            {
                if (moe.Current is tsm.Part)
                {
                    tsm.ModelObject mobj = ArkLib.ark_Tekla.MyAlgoritms.Offset.Start(moe.Current,
                        Convert.ToDouble(txt_PartOffset.Text));
                    mobj.Modify();
                }
            }
            model.CommitChanges();
        }

        private void btn_PartUpBottomPoint_Click(object sender, EventArgs e)
        {
             
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //TODO FIX
            //ark_MovePoints mp = new ark_MovePoints();
            //mp.Show();
            MessageBox.Show("Функция временно отключена.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ark_Joints aj = new ark_Joints();
            aj.Show();
        }

        private void btn_StepComponent_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
            if (model.GetConnectionStatus() == false) return;
            tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = mos.GetSelectedObjects();
            while (moe.MoveNext())
            {
                if (moe.Current is tsm.CustomPart)
                {
                    tsm.CustomPart cmp = moe.Current as tsm.CustomPart;
                    cmp.Select();
                    tsg.Point p1 = new tsg.Point();
                    tsg.Point p2 = new tsg.Point();
                    cmp.GetStartAndEndPositions(ref p1, ref p2);
                    double step = 2400;
                    double length = tsg.Distance.PointToPoint(p1, p2);
                    int count = Convert.ToInt32(Math.Floor(length / 2400));
                    double last_step = length - step * count;
                    tsg.Vector v_direct = ArkLib.x_algoritms.x_New.Vector(p1, p2);
                    v_direct.Normalize(step);

                    p2 = new tsg.Point(p1);
                    p2.Translate(v_direct.X, v_direct.Y, v_direct.Z);

                    cmp.SetInputPositions(p1, p2);
                    cmp.Modify();
                    tsm.ModelObject mobj = cmp;
                    mobj.Select();
                    for (int i = 0; i < count; i++)
                    {
                       mobj =  tsm.Operations.Operation.CopyObject(mobj, v_direct);
                       mobj.Select();
                    }
                    tsm.CustomPart cp = mobj as tsm.CustomPart;
                    cp.Select();
                    cp.GetStartAndEndPositions(ref p1, ref p2);
                    v_direct = ArkLib.x_algoritms.x_New.Vector(p1, p2);
                    v_direct.Normalize(last_step);
                    p2 = new tsg.Point(p1);
                    p2.Translate(v_direct.X, v_direct.Y, v_direct.Z);
                    cp.SetInputPositions(p1, p2);
                    cp.Modify();

                    //tsg.Point p3 = new tsg.Point(p1);
                    //tsg.Point p4 = new tsg.Point(p3);
                    //p4.Translate(v_direct.X, v_direct.Y, v_direct.Z);
                    //for(int i = 0; i < count; i++)
                    //{
                    //    tsm.CustomPart cmp1 = cmp;
                    //    cmp1.SetInputPositions(p3, p4);
                    //    cmp1.Insert();
                    //    p3.Translate(v_direct.X, v_direct.Y, v_direct.Z);
                    //    p4.Translate(v_direct.X, v_direct.Y, v_direct.Z);
                    //}
                    //tsm.CustomPart cmp2 = cmp;
                    //p4 = new tsg.Point(p3);
                    //v_direct.Normalize(last_step);
                    //p4.Translate(v_direct.X, v_direct.Y, v_direct.Z);
                    ////p4.Translate()
                    //cmp2.SetInputPositions(p3, p4);
                    //cmp2.Insert();
                    ////MessageBox.Show(p1.ToString() + Environment.NewLine + p3.ToString());
                    //cmp.Delete();
                }
            }
            model.CommitChanges();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
			if (model.GetConnectionStatus() == false) return;
			tsm.UI.ModelObjectSelector mos = new tsm.UI.ModelObjectSelector();
			tsm.UI.Picker picker = new tsm.UI.Picker();


			tsm.ModelObject mobolts = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_BOLTGROUP) as tsm.ModelObject;

			tsm.Part part = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_PART) as tsm.Part;
			tsm.Part part2 = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_PART) as tsm.Part;
			//mes((mbolts is tsm.BoltArray).ToString());

			// tsm.TransformationPlane tp = new TransformationPlane(part.GetCoordinateSystem());
			//tsm.TransformationPlane cur = model.GetWorkPlaneHandler().GetCurrentTransformationPlane();

			//  WorkPlaneHandler wph = model.GetWorkPlaneHandler();

			//wph.SetCurrentTransformationPlane(tp);

			if (mobolts is tsm.BoltArray)
			{
				tsm.BoltArray mbolts = mobolts as tsm.BoltArray;

				double d = 0;
				mbolts.GetReportProperty("LENGTH", ref d);

				tsm.BoltArray ba = new tsm.BoltArray();

				ba.PartToBeBolted = part;
				ba.PartToBoltTo = part;

				tsg.Point p1 = new tsg.Point(tsg.Projection.PointToPlane(mbolts.FirstPosition, new tsg.GeometricPlane(part.GetCoordinateSystem())));
				tsg.Point p2 = new tsg.Point(tsg.Projection.PointToPlane(mbolts.SecondPosition, new tsg.GeometricPlane(part.GetCoordinateSystem())));

				ba.BoltStandard = mbolts.BoltStandard;

				ba.BoltSize = mbolts.BoltSize;
				ba.BoltType = mbolts.BoltType;
				ba.Position = mbolts.Position;
				ba.Length = mbolts.Length;
				ba.ExtraLength = mbolts.ExtraLength;
				ba.ThreadInMaterial = mbolts.ThreadInMaterial;

				ba.Bolt = false;
				ba.Tolerance = mbolts.Tolerance;

				for (int i = 0; i < mbolts.GetBoltDistXCount(); i++)
					ba.AddBoltDistX(mbolts.GetBoltDistX(i));

				for (int i = 0; i < mbolts.GetBoltDistYCount(); i++)
					ba.AddBoltDistY(mbolts.GetBoltDistY(i));
				ba.StartPointOffset = mbolts.StartPointOffset;
				ba.EndPointOffset = mbolts.EndPointOffset;
				ba.Nut1 = mbolts.Nut1;
				ba.Nut2 = mbolts.Nut2;
				ba.Washer1 = mbolts.Washer1;
				ba.Washer2 = mbolts.Washer2;
				ba.Washer3 = mbolts.Washer3;
				ba.FirstPosition = p1;
				ba.SecondPosition = p2;
				ba.Position.Rotation = 0;
				if (ba.Insert() == false)
				{
					MessageBox.Show("false");
					return;
				}

				mbolts.PartToBoltTo = part2;
				mbolts.PartToBeBolted = part2;

				mbolts.Modify();

				while (true)
				{
					double l = 0;
					mbolts.GetReportProperty("LENGTH", ref l);
					if (l != d)
					{
						mbolts.ExtraLength += 1;
						mbolts.Modify();
					}
					else break;
				}
			}
			new tsm.Model().CommitChanges();
        }

        void SoapWriteFile(object objGraph, string fileName)
        {
            //SoapFormatter formatter = new SoapFormatter();
            BinaryFormatter formatter = new BinaryFormatter();

            formatter.FilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

            using (Stream fStream = new FileStream(fileName,
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                formatter.Serialize(fStream, objGraph);
            }

        }

        List<tsm.ModelObject> SoapLoadFile()
        {
            //Opens file "data.xml" and deserializes the object from it.
            Stream stream = File.Open("all_model.txt", FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();

            //formatter = new BinaryFormatter();

            List<tsm.ModelObject> obj = (List<tsm.ModelObject>)formatter.Deserialize(stream);
            stream.Close();
            return obj;
        }

        private void button5_Click(object sender, EventArgs e)
        {
			//xSuhari x = new xSuhari();
			DimensionPartToGrid dptg = new DimensionPartToGrid();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
            tsm.UI.ModelObjectSelector mobs = new tsm.UI.ModelObjectSelector();
            tsm.ModelObjectEnumerator moe = model.GetModelObjectSelector().GetAllObjectsWithType(new Type[] { typeof(tsm.Part) });
            
            //mobs.GetSelectedObjects();

            //int i = 0;

            while (moe.MoveNext())
            {
                if (moe.Current is tsm.Part)
                {
                    try
                    {
                        tsm.Part p = moe.Current as tsm.Part;
                        if (p.Material.MaterialString != "AISI 316L" || p.Finish != "")
                        {
                            p.Material.MaterialString = "AISI 316L";
                            p.Finish = "";
                            p.Modify();
                        }
                        if (p.AssemblyNumber.Prefix != "ВХ" || p.PartNumber.Prefix != "")
                        {
                            p.AssemblyNumber.Prefix = "ВХ";
                            p.PartNumber.Prefix = "";
                            p.Modify();
                        }
                    }
                    catch { }
                }
                //SoapWriteFile(moe.Current as tsm.Part, "test " + i.ToString() + ".txt");
            }
            MessageBox.Show("done");
        }

        private void btn_CutBeamRelativeBeam_Click(object sender, EventArgs e)
        {
            tsm.Model model = new tsm.Model();
            tsm.UI.ModelObjectSelector mobs = new tsm.UI.ModelObjectSelector();

            tsm.UI.Picker picker = new tsm.UI.Picker();

            tsm.Part Primary = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_PART) as tsm.Part;

            tsg.Point p1 = Primary.GetReferenceLine(false)[0] as tsg.Point;
            tsg.Point p2 = Primary.GetReferenceLine(false)[1] as tsg.Point;

            tsg.Line ls = new tsg.Line(p1, p2);

            tsm.ModelObjectEnumerator moe = mobs.GetSelectedObjects();
            while (moe.MoveNext())
            {
                //if (moe.Current is tsm.Weld)
                //{
                //    tsm.Weld w = moe.Current as tsm.Weld;
                //    MessageBox.Show(w.ContourAbove.ToString());
                //}


                if (moe.Current is tsm.Beam)
                {
                    tsm.Beam p = moe.Current as tsm.Beam;
                    tsg.Point pp1 = p.StartPoint;
                    tsg.Point pp2 = p.EndPoint;

                    tsg.Line ls2 = new tsg.Line(pp1, pp2);

                    tsg.LineSegment ls0 = tsg.Intersection.LineToLine(ls, ls2);

                    if (tsg.Distance.PointToPoint(pp1, ls0.Point1) > tsg.Distance.PointToPoint(pp2, ls0.Point1))
                    {
                        p.EndPoint = ls0.Point1;
                    }
                    else
                        p.StartPoint = ls0.Point1;
                    p.Modify();
                    
                }
            }

            model.CommitChanges();
        }

        private void ark_Ladder_Show_Click(object sender, EventArgs e)
        {
            Components_Tekla.Ladder ld = new Components_Tekla.Ladder();
            ld.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ark_WeldsFromAssemblies wfa = new ark_WeldsFromAssemblies();
            wfa.Show();
        }

		private void btn_SinglePartCurved_Click(object sender, EventArgs e)
		{
			//SinglePart_Curved sp = new SinglePart_Curved();
		}

        private void cmp_button_p1_Click(object sender, EventArgs e)
        {
            ark_Functions.WiToPlate.Run();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void btn_Profile_I_to_Plate_Click(object sender, EventArgs e)
        {
            ark_Functions.ItoPlate.Run();
        }
    }
}
