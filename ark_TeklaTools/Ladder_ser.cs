﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using me = ArkLib.MarkElements;

namespace Components_Tekla
{

    [Serializable]
    public class Ladder_ser
    {
       public me.ark_Profile MainPart = new me.ark_Profile();
       public me.ark_Profile SecondPart = new me.ark_Profile();
       public me.ark_Profile Step = new me.ark_Profile();
       public double width = 700;
       public double h1 = 1000;
       public double h2 = -1;
       public double h3 = 300;
       public double dx = -1;
       public List<double> steps = new List<double>();
    }
}
