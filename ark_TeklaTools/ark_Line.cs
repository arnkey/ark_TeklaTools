﻿
using System;
namespace ArkLib.ark_Tekla.Geometry
{
    [Serializable]
    public class ark_Line
    {
        public ark_Point StartPoint;
        public ark_Point EndPoint;

        public ark_Line(ark_Point p1, ark_Point p2)
        {
            StartPoint = new ark_Point(p1);
            EndPoint = new ark_Point(p2);
        }

        public override string ToString()
        {
            return (StartPoint.ToString() + "\n" + EndPoint.ToString());
        }
    }
}
