﻿using System;
using System.Windows.Forms;

using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;

using com = ArkLib.Common;

namespace ark_TeklaTools
{
    public partial class ark_TeklaTools_Form : Form
    {
        #region messages
        void mes()
        {
            mes("Готово!");
        }
        void mes(string m)
        {
            MessageBox.Show(m);
        }
        void mes(int s)
        {
            mes(s.ToString());
        }
        void mes(double s)
        {
            mes(s.ToString());
        }
        void mes(bool b)
        {
            mes(b.ToString());
        }
        #endregion

        public ark_TeklaTools_Form()
        {
            InitializeComponent();
        }

        private void ark_TeklaTools_Form_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
        }

        private void ark_BeamToTop_button_Click(object sender, EventArgs e)
        {
            BeamToTop.ark_BeamToTop_Form f = new BeamToTop.ark_BeamToTop_Form();
            f.ShowDialog();
        }

        private void prjOnPart_Click(object sender, EventArgs e)
        {
            tsm.ModelObjectEnumerator moe = com.FastFunctions.GetModelObjects;
            
            tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();
            tsm.TransformationPlane ctp = wph.GetCurrentTransformationPlane();
            tsm.TransformationPlane tp = ArkLib.ark_Tekla.ark_New.TP_GLOBAL;
            wph.SetCurrentTransformationPlane(tp);
            tsm.UI.Picker picker = new tsm.UI.Picker();
            tsm.Beam b1 = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_PART) as tsm.Beam;
            tsg.LineSegment ls1 = new tsg.LineSegment(b1.StartPoint, b1.EndPoint);
            while (moe.MoveNext())
            {
                if (moe.Current is tsm.Beam)
                {
                    tsm.Beam bm = moe.Current as tsm.Beam;
                    tsg.Point bm1 = new tsg.Point(bm.StartPoint); bm1.Z += 1000;
                    tsg.Point bm2 = new tsg.Point(bm.EndPoint); bm2.Z += 1000;
                    tsg.Line beam1 = new tsg.Line(bm.StartPoint, bm1);
                    tsg.Line beam2 = new tsg.Line(bm.EndPoint, bm2);
                    tsg.Line bls1 = new tsg.Line(ls1.Point1, ls1.Point2);
                    bm.StartPoint = tsg.Intersection.LineToLine(beam1, bls1).Point1;
                    bm.EndPoint = tsg.Intersection.LineToLine(beam2, bls1).Point1;
                    bm.Modify();
                }
            }
            wph.SetCurrentTransformationPlane(ctp);
            com.FastFunctions.ModelCommitChanges();
            mes();
        }
        
        private void SetMainPart_Click(object sender, EventArgs e)
        {
            tsm.ModelObjectEnumerator moe = com.FastFunctions.GetModelObjects;
            while (moe.MoveNext())
            {
                if (moe.Current is tsm.Part)
                {
                    tsm.Part part = moe.Current as tsm.Part;
                    tsm.Assembly asm = part.GetAssembly();
                    asm.SetMainPart(part);
                    asm.Modify();
                }
            }
            com.FastFunctions.ModelCommitChanges();
        }

        private void SetHandRail_Click(object sender, EventArgs e)
        {
            //tsm.UI.Picker picker = new tsm.UI.Picker();
            //tsm.ModelObject mobj = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_OBJECT, "");

            //tsm.Component cp = mobj as tsm.Component;
            //mes(cp.Name + " " + cp.Number);

            tsm.Model model = new tsm.Model();
            tsm.UI.Picker picker = new tsm.UI.Picker();
            tsm.Beam Beam = picker.PickObject(tsm.UI.Picker.PickObjectEnum.PICK_ONE_PART, "") as tsm.Beam;

            string attributes = "standard";

            double dx1 = 0;
            double dx2 = 0;
            double step = 0;
            double cCol = 0; // count of column
            int side = 0;

            double length = tsg.Distance.PointToPoint(Beam.StartPoint, Beam.EndPoint);
            double width = 0; Beam.GetReportProperty("PROFILE.WIDTH", ref width);

            ArkLib.SettingsPlugins.x_Handrail hand = new ArkLib.SettingsPlugins.x_Handrail(Beam);
            //tsm.CustomPart cp = new tsm.CustomPart();
            //cp.SetInputPositions(Beam.GetReferenceLine(true)[0] as tsg.Point, Beam.GetReferenceLine(true)[1] as tsg.Point);
            //cp.Name = "x_Handrail_Long";
            //cp.Number = -100000;
            //cp.LoadAttributesFromFile("ОП");
            //cp.Insert();
            //cmp.Insert();
            model.CommitChanges();
        }
    }
}
