﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model;
using ui = Tekla.Structures.Model.UI;

namespace ArkLib.ark_Tekla.MyAlgoritms
{
    public static class Differents
    {
        /// <summary>
        /// Округление числа кратное d в большую сторону
        /// </summary>
        /// <param name="num"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static double Round(double num, int d)
        {
            num = Math.Floor(num);
            int i;
            i = Convert.ToInt32(num);
            if (i % d != 0) i = (i / d) * d + d;
            return (Convert.ToDouble(i));
        }

        public static class OperationPoint
        {
            public enum Relative
            {
                dx,
                dy,
                dz
            }

            public enum Position
            {
                Top,
                Bottom
            }

            public static Point ReturnPoint(Point p1, Point p2, Position ps = Position.Top, Relative rv = Relative.dz)
            {
                if (ps == Position.Top)
                    return MaxPoint(p1, p2, rv);
                else if (ps == Position.Bottom)
                    return MinPoint(p1, p2, rv);
                else
                    return null;
            }

            private static Point MinPoint(Point p1, Point p2, Relative rv = Relative.dz)
            {
                if (rv == Relative.dx)
                    return MinPointX(p1, p2);
                else if (rv == Relative.dy)
                    return MinPointY(p1, p2);
                else if (rv == Relative.dz)
                    return MinPointZ(p1, p2);
                else
                    return null;
            }

            private static Point MaxPoint(Point p1, Point p2, Relative rv = Relative.dz)
            {
                if (rv == Relative.dx)
                    return MaxPointX(p1, p2);
                else if (rv == Relative.dy)
                    return MaxPointY(p1, p2);
                else if (rv == Relative.dz)
                    return MaxPointZ(p1, p2);
                else
                    return null;
            }

            #region Minimum
            private static Point MinPointX(Point p1, Point p2)
            {
                if (Point.AreEqual(p1, p2)) return null;
                return new Point(p1.X <= p2.X ? p1 : p2);
            }
            private static Point MinPointY(Point p1, Point p2)
            {
                if (Point.AreEqual(p1, p2)) return null;
                return new Point(p1.Y <= p2.Y ? p1 : p2);
            }
            private static Point MinPointZ(Point p1, Point p2)
            {
                if (Point.AreEqual(p1, p2)) return null;
                return new Point(p1.Z <= p2.Z ? p1 : p2);
            }
            #endregion
            #region Maximum
            private static Point MaxPointX(Point p1, Point p2)
            {
                if (Point.AreEqual(p1, p2)) return null;
                return new Point(p1.X >= p2.X ? p1 : p2);
            }
            private static Point MaxPointY(Point p1, Point p2)
            {
                if (Point.AreEqual(p1, p2)) return null;
                return new Point(p1.Y >= p2.Y ? p1 : p2);
            }
            private static Point MaxPointZ(Point p1, Point p2)
            {
                if (Point.AreEqual(p1, p2)) return null;
                return new Point(p1.Z >= p2.Z ? p1 : p2);
            }

            #endregion
        }



    }
}
