﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//#error Исправить UDA Для int и Double, + конструктор для удобства

namespace ArkLib.MarkElements
{
    [Serializable]
    public class UDA
    {
        public enum Type
        {
            INT,
            DOUBLE,
            STRING
        }

        public string Name = "";
        private string StringValue = "";
        private int IntValue = int.MinValue;
        private double DoubleValue = double.MinValue;

        public UDA(Type t, string name, string value)
        {
            if (t == Type.INT)
            {
                Name = name;
                IntValue = Convert.ToInt32(value);
            }
            if (t == Type.DOUBLE)
            {
                Name = name;
                DoubleValue = Convert.ToDouble(value);
            }
            if (t == Type.STRING)
            {
                Name = name;
                StringValue = value;
            }
        }

        public int GetIntValue()
        {
            return IntValue;
        }
        public double GetDoubleValue()
        {
            return DoubleValue;
        }
        public string GetStringValue()
        {
            return StringValue;
        }
    }
}
