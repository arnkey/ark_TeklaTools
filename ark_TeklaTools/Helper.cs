﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Tekla.Structures;
using Tekla.Structures.Model;
using ui = Tekla.Structures.Model.UI;
using dif = ArkLib.ark_Tekla.MyAlgoritms.Differents;

namespace ArkLib.ark_Tekla.MyAlgoritms.Plugins
{
    public static class Helper
    {
        public enum NumObjects
        {
            one,
            two,
            many
        }


        public static Tuple<ModelObject, List<ModelObject>> SendObject(NumObjects n)
        {
            Tuple<ModelObject, List<ModelObject>> t = null;
            ModelObject mobj = null;
            List<ModelObject> mobj2 = new List<ModelObject>();
            ModelObject mobj3 = null;
            if (n == NumObjects.one)
            {
                PickObject(ref mobj);
                t = new Tuple<ModelObject, List<ModelObject>>(
                    mobj, null);
            }
            else if (n == NumObjects.two)
            {
                PickObjects(ref mobj, ref mobj3);
                t = new Tuple<ModelObject, List<ModelObject>>(
    mobj, new List<ModelObject> { mobj3 } );
            }
            else if (n == NumObjects.many)
            {
                PickObjects(ref mobj, ref mobj2);
                t = new Tuple<ModelObject, List<ModelObject>>(
mobj, mobj2);
            }
            return t;
        }

        public static void PickObject(ref Tekla.Structures.Model.ModelObject mobj)
        {
            try
            {
                mobj = PickObject("Главная деталь");
            }
            catch { }
        }

        public static void PickObjects(ref Tekla.Structures.Model.ModelObject mobj, ref List<Tekla.Structures.Model.ModelObject> mobj2)
        {
            try
            {
                mobj = PickObject("Главная деталь");
                mobj2 = PickObjects("Второстепенная деталь");
            }
            catch { }
        }

        public static void PickObjects(ref ModelObject mobj, ref ModelObject mobj2)
        {
            try
            {
                mobj = PickObject("Главная деталь");
                mobj2 = PickObject("Второстепенная деталь");
            }
            catch { }
        }

        public static List<ModelObject> PickObjects(string mes = "")
        {
            ui.Picker picker = new ui.Picker();
            List<ModelObject> objects = new List<ModelObject>();
            while (true)
            {
                try
                {
                    objects.Add(picker.PickObject(ui.Picker.PickObjectEnum.PICK_ONE_PART, mes));
                }
                catch
                {
                    break;
                }
            }
            return objects;
        }

        public static ModelObject PickObject(string mes = "")
        {
            ui.Picker picker = new ui.Picker();
            try
            {
                return picker.PickObject(ui.Picker.PickObjectEnum.PICK_ONE_PART, mes);
            }
            catch
            {
                return null;
            }
        }


        public enum Weld
        {
            Shop,
            Erection
        }
    }


    public static class AlgoHelper
    {

        public static double MinThick(Part p1, Part p2)
        {
            return Math.Min(
                x_algoritms.x_Profile.GetThickness(p1, x_algoritms.x_Profile.GetProfileType(p1)),
                x_algoritms.x_Profile.GetThickness(p1, x_algoritms.x_Profile.GetProfileType(p1)));

        }

    }
}
