﻿// Type: x_algor.x_Profile
// Assembly: WeldApp, Version=1.15.12.29, Culture=neutral, PublicKeyToken=null
// MVID: 8148BD28-E7ED-4BDF-BDEA-0ED2A6A16AE0
// Assembly location: C:\TeklaStructures\20.0\Environments\Common\macros\drawings\WeldApp.exe

using System;
using Tekla.Structures.Model;

namespace ArkLib.x_algoritms
{
  public static class x_Profile
  {
    public static x_Profile.Type GetProfileType(Part part)
    {
      string str = "";
      part.GetReportProperty("PROFILE_TYPE", ref str);
      if (str == "I")
        return x_Profile.Type.I;
      if (str == "L")
        return x_Profile.Type.L;
      if (str == "U")
        return x_Profile.Type.U;
      if (str == "T")
        return x_Profile.Type.T;
      if (str == "B")
        return x_Profile.Type.PL;
      if (str == "RO")
        return x_Profile.Type.CHS;
      if (str == "RU")
        return x_Profile.Type.D;
      if (str == "M")
        return x_Profile.Type.RHS;
      if (str == "C")
        return x_Profile.Type.G;
      System.Windows.Forms.MessageBox.Show(str);
      return str == "Z" ? x_Profile.Type.Z : x_Profile.Type.Unknow;
    }

    public static double GetThickness(Part part, x_Profile.Type profType)
    {
      double num = 0.0;
      switch (profType)
      {
        case x_Profile.Type.PL:
          num = x_Profile.GetPlateThickness(part);
          break;
        case x_Profile.Type.L:
          part.GetReportProperty("PROFILE.FLANGE_THICKNESS_1", ref num);
          break;
        case x_Profile.Type.I:
        case x_Profile.Type.U:
        case x_Profile.Type.T:
          double val1 = 0.0;
          double val2 = 0.0;
          part.GetReportProperty("PROFILE.FLANGE_THICKNESS", ref val1);
          part.GetReportProperty("PROFILE.WEB_THICKNESS", ref val2);
          num = Math.Min(val1, val2);
          if (num == 0.0)
          {
            part.GetReportProperty("PROFILE.PLATE_THICKNESS", ref num);
            break;
          }
          else
            break;
        case x_Profile.Type.RHS:
        case x_Profile.Type.CHS:
          part.GetReportProperty("PROFILE.PLATE_THICKNESS", ref num);
          break;
        default:
          part.GetReportProperty("PROFILE.FLANGE_THICKNESS", ref num);
          break;
      }
      return num;
    }

    public static double GetPlateThickness(Part part)
    {
      if (part == null)
        return 0.0;
      double num1 = 0.0;
      double num2 = 0.0;
      part.GetReportProperty("PROFILE.HEIGHT", ref num1);
      part.GetReportProperty("PROFILE.WIDTH", ref num2);
      if (num1 < num2)
        return num1;
      else
        return num2;
    }

    public enum Type
    {
      PL,
      L,
      I,
      RHS,
      CHS,
      D,
      U,
      T,
      Z,
      G,
      Unknow,
    }
  }
}
