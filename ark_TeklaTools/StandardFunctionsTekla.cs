﻿/*using System;
using System.Collections.Generic;
using System.Globalization;
using Tekla.Structures.Model;

using ui = Tekla.Structures.Model.UI;


namespace iArkLib.ark_Tekla.Common
{



    public static class FastFunctions
    {

        public static Model Mod()
        {
            return new Model();
        }
        public static void ModelCommitChanges()
        {
            Model _model = Mod();
            _model.CommitChanges();
        }

        public static ModelObjectEnumerator GetModelObjects
        {
            get
            {
                Model model = new Model();
                ui.ModelObjectSelector mos = new ui.ModelObjectSelector();
                return mos.GetSelectedObjects();
            }
        }

    }

    public static class StandardFunctionsTekla
    {
        public static void ZoomObject()
        {
            string str = "akit.Callback(\"acmdZoomToSelected\", \"\", \"main_frame\");";
            macros.WriteMacrosAndRun(str);
        }

        /// <summary>
        /// Обновить пространство модели
        /// </summary>
        public static void RefreshModel()
        {
            string str = "akit.Callback(\"acmd_redraw_selected_view\", \"\", \"View_01 window_1\");";
            macros.WriteMacrosAndRun(str);
        }

        /// <summary>
        /// Полностью уместить модель в рабочую область
        /// </summary>
        public static void FitWorkArea()
        {
            string str = "akit.Callback(\"acmd_fit_workarea\", \"\", \"View_01 window_1\");";
            macros.WriteMacrosAndRun(str);
        }

        public static void Copy(string type, string num, string di)
        {
            string str = "akit.CommandStart(\"ail_copy_translate\", \"\", \"main_frame\");" +
                         "akit.PushButton(\"dia_copy_apply_1\", \"Copy\");" +
                         "akit.ValueChange(\"Copy\", \"no_of_copies\",\"" + num + "\");" +
                         "akit.ValueChange(\"Copy\",\"" + type + "\",\"" + di + "\");" +
                         "akit.PushButton(\"dia_copy_apply\", \"Copy\");" +
                         "akit.PushButton(\"dia_copy_cancel\", \"Copy\");" +
                         "akit.CommandEnd();" +
                         "akit.Callback(\"acmd_interrupt\", \"\", \"main_frame\");";
            macros.WriteMacrosAndRun(str);
        }
        public static void Move(string type, string di)
        {
            string str = "akit.CommandStart(\"ail_move_translate\", \"\", \"main_frame\");"+
                        "akit.PushButton(\"dia_move_clear\", \"Move\");"+
                        "akit.ValueChange(\"Move\",\""+ type+"\",\""+ di + "\");"+
                        "akit.PushButton(\"dia_move_apply\", \"Move\");"+
                        "akit.PushButton(\"dia_move_cancel\", \"Move\");"+
			            "akit.CommandEnd();"+
                        "akit.Callback(\"acmd_interrupt\", \"\", \"main_frame\");";
            macros.WriteMacrosAndRun(str);
        }


        /// <summary>
        /// From TS
        /// </summary>
        public static class Parsers
        {
            /// <summary>
            /// Stores the Tekla numberFormatinfo
            /// </summary>
            private static NumberFormatInfo m_TeklaNumberFormat;
            private const string DecimalSeparator = ".";
            private const string GroupSeparator = ",";

            /// <summary>
            /// Initializes the class so we have the "Tekla" numberFormatInfo available
            /// </summary>
            static Parsers()
            {
                m_TeklaNumberFormat = new NumberFormatInfo();
                m_TeklaNumberFormat.NumberDecimalSeparator = DecimalSeparator;
                m_TeklaNumberFormat.NumberGroupSeparator = GroupSeparator;
                m_TeklaNumberFormat.NumberDecimalSeparator = DecimalSeparator;
                m_TeklaNumberFormat.NumberGroupSeparator = GroupSeparator;
                m_TeklaNumberFormat.CurrencyDecimalSeparator = DecimalSeparator;
                m_TeklaNumberFormat.CurrencyGroupSeparator = GroupSeparator;
                m_TeklaNumberFormat.PercentDecimalSeparator = DecimalSeparator;
                m_TeklaNumberFormat.PercentGroupSeparator = GroupSeparator;
            }

            public static List<double> DistanceListToList(string text)
            {
                string[] chunks = text.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                List<double> distList = new List<double>();
                for (int i = 0; i < chunks.GetLength(0); i++)
                {
                    if (chunks[i].IndexOf("*") > -1)
                    {
                        int multiplier = ParseInt(chunks[i].Split(new char[] { '*' })[0], 1);
                        double distance = ParseDouble(chunks[i].Split(new char[] { '*' })[1]);
                        for (int j = 0; j < multiplier; j++)
                        {
                            distList.Add(distance);
                        }
                    }
                    else
                    {
                        double val = ParseDouble(chunks[i], double.MinValue);
                        if (val != double.MinValue)
                            distList.Add(val);
                    }
                }
                return distList;
            }

            public static double[] DistanceListToArray(string text)
            {
                return DistanceListToList(text).ToArray();
            }

            public static double ParseDouble(string text)
            {
                return ParseDouble(text, 0);
            }

            public static double ParseDouble(string text, double def)
            {
                try
                {
                    return double.Parse(text, m_TeklaNumberFormat);
                }
                catch
                {
                    return def;
                }
            }

            public static int ParseInt(string text)
            {
                return ParseInt(text, 0);
            }

            public static int ParseInt(string text, int def)
            {
                try
                {
                    return int.Parse(text, m_TeklaNumberFormat);
                }
                catch
                {
                    return def;
                }
            }
        }


    }
}
*/