﻿namespace ark_TeklaTools
{
    public class ark_TeklaTools : IPlugin_arnkey
    {
        public string Name
        {
            get { return "ark_TeklaTools"; }
        }

        public string Description
        {
            get { return "Набор инструментов при работе с моделью"; }
        }
        public void Run()
        {
            /* Исполняемый код */
            ark_TeklaModelTools_Form f1 = new ark_TeklaModelTools_Form();
            f1.ShowDialog();
        }

    }
}