﻿using System;
using Tekla.Structures.Drawing;
using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;
using tsd = Tekla.Structures.Drawing;
using System.Collections.Generic;

namespace ark_TeklaDrawingTools
{
	public class SinglePartTube
	{
		public SinglePartTube()
		{
			tsg.Vector dirX = new tsg.Vector(1, 0, 0);
			tsg.Vector dirY = new tsg.Vector(0, 1, 0);
			tsg.Vector dirXY = new tsg.Vector(1, 1, 0);


			/*tsm.ModelObjectEnumerator moe = new tsm.UI.ModelObjectSelector().GetSelectedObjects();
			while (moe.MoveNext())
			{
				if (moe.Current is tsm.Part)
				{
					foreach (tsg.LineSegment ls in GetLinesFromBools(moe.Current as tsm.Part))
					{
						tsm.ControlLine cl = new tsm.ControlLine(ls, false);
						//cl.Insert();
					}

				}

			}

			new tsm.Model().CommitChanges();
			//Console.ReadKey();*/

			DrawingHandler dh = new DrawingHandler();
			if (dh.GetConnectionStatus() == true)
			{
				Drawing draw = dh.GetActiveDrawing();
				tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
				ViewBase vb = null;

				while (doe.MoveNext())
				{
					if (doe.Current is tsd.ViewBase)
					{
						vb = doe.Current as tsd.ViewBase;
						break;
					}
				}
				if (vb == null) return;

				tsd.View v = vb as tsd.View;
				tsg.Matrix m = tsg.MatrixFactory.ToCoordinateSystem(v.DisplayCoordinateSystem);

				tsm.Part part =
					new tsm.Model().SelectModelObject((draw as tsd.SinglePartDrawing).PartIdentifier) as tsm.Part;

				double w = 0; part.GetReportProperty("PROFILE.HEIGHT", ref w);

				List<tsg.LineSegment> fittings = GetLinesFromFittins(part);
				List<tsg.LineSegment> bools = GetLinesFromBools(part);

				tsd.StraightDimensionSetHandler sds = new tsd.StraightDimensionSetHandler();

				GetLinesFromBools(part, vb, m, true);

				// fittings
				PointList pxF = new PointList();
				// bools
				PointList pxB = new PointList();


				foreach (tsg.LineSegment ls in fittings)
				{

					tsg.Point ip = tsg.Intersection.LineToLine(
						new tsg.Line(ls), new tsg.Line(
							part.GetCenterLine(true)[0] as tsg.Point,
							part.GetCenterLine(true)[1] as tsg.Point)).Point1;

					//PyF.Add(ip);

					tsg.Vector vr = newV(rp(ls.Point2, m), rp(ls.Point1, m));

					double angle = vr.GetAngleBetween(newV(GetCenterLine(part))) * Math.PI / 180;
					if (Math.Round(angle,0) != 90.0)
					{

						PointList xy = new PointList();
						xy.Add(rp(ls.Point1, m)); xy.Add(rp(ls.Point2, m));

						vr = VectorNew(vr, 90, new tsg.Vector(0, 0, 1));

						//Console.WriteLine(vr.ToString());
						sds.CreateDimensionSet(vb, xy, vr, 10 * v.Attributes.Scale);

						tsg.Point prj = tsg.Projection.PointToLine(ls.Point1, new tsg.Line(GetCenterLine(part)));

						tsd.AngleDimension iangle = new tsd.AngleDimension(
							vb, rp(ls.Point1, m), rp(prj, m), rp(ip, m), 10 * v.Attributes.Scale);

						iangle.Insert();
					}
				}

				foreach (tsg.Point p in LStoPoint(fittings)) pxF.Add(rp(p, m));

				foreach (tsg.Point p in LStoPoint(bools)) pxB.Add(rp(p, m));



				//pyF.Add(rp(part.GetCenterLine(true)[0] as tsg.Point, m));

				if (pxF.Count >= 2) sds.CreateDimensionSet(vb, pxF, dirY, -1 * w * 0.5 - 10 * v.Attributes.Scale);
				if (pxB.Count >= 2) sds.CreateDimensionSet(vb, pxB, dirY, 1 * w * 0.5 + 10 * v.Attributes.Scale);

			}


			////Console.ReadKey();
		}

		private static tsg.LineSegment GetCenterLine(tsm.Part part)
		{
			return (new tsg.LineSegment(part.GetCenterLine(true)[0] as tsg.Point, part.GetCenterLine(true)[1] as tsg.Point));
		}
		public static Tekla.Structures.Geometry3d.Vector VectorNew(Tekla.Structures.Geometry3d.Vector vector, double angle, Tekla.Structures.Geometry3d.Vector rotateAxis)
		{
			angle = angle * 0.0174533;
			tsg.Matrix matrix = tsg.MatrixFactory.Rotate(angle, rotateAxis);
			return VectorNew(vector, matrix);
		}
		public static Tekla.Structures.Geometry3d.Vector VectorNew(Tekla.Structures.Geometry3d.Vector vector, tsg.Matrix matrix)
		{
			tsg.Point p = new tsg.Point(vector.X, vector.Y, vector.Z);
			tsg.Point point = matrix.Transform(p);
			return new Tekla.Structures.Geometry3d.Vector(point.X, point.Y, point.Z);
		}

		private static tsg.Vector newV(tsg.LineSegment ls)
		{
			return newV(ls.Point1, ls.Point2);
		}

		private static tsg.Vector newV(tsg.Point p1, tsg.Point p2)
		{
			return new tsg.Vector(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
		}

		private static tsg.Point rp(tsg.Point p, tsg.Matrix m)
		{
			return (m.Transform(p));
		}

		private static tsd.StraightDimensionSet.StraightDimensionSetAttributes attr(string text)
		{
			tsd.StraightDimensionSet.StraightDimensionSetAttributes attG = new StraightDimensionSet.StraightDimensionSetAttributes();
			TextElement te = new TextElement(text);
			te.Font.Name = "GOST 2.304 type A";
			te.Font.Height = 3.5;
			attG.RightUpperTag.Add(te);

			return attG;
		}

		private static void InsertText(ViewBase vb, string text, tsg.Point p)
		{
			tsd.Text t = new Text(vb, p, text);
			t.Attributes.Font.Name = "GOST 2.304 type A";
			t.Attributes.Font.Height = 3.5;
			t.Attributes.ArrowHead.ArrowPosition = ArrowheadPositions.Start;
			t.Attributes.ArrowHead.Head = ArrowheadTypes.FilledArrow;
			t.Attributes.PlacingAttributes.PlacingDistance.MinimalDistance = 10;
			t.Attributes.Angle = 0;
			t.Attributes.PreferredPlacing = PreferredTextPlacingTypes.LeaderLinePlacingType();
			t.Attributes.Alignment = TextAlignment.Left;
			t.Attributes.Frame.Type = FrameTypes.Line;
			t.Insert();
		}

		private static List<tsg.Point> LStoPoint(List<tsg.LineSegment> ls)
		{
			List<tsg.Point> points = new List<tsg.Point>();
			foreach (tsg.LineSegment ls0 in ls)
			{
				points.Add(ls0.Point1);
				points.Add(ls0.Point2);
			}
			return points;
		}

		private static List<tsg.LineSegment> GetLinesFromFittins(tsm.Part part)
		{
			List<tsg.LineSegment> ls = new List<tsg.LineSegment>();
			tsm.ModelObjectEnumerator bools = (part.GetChildren());
			while (bools.MoveNext())
			{
				if (bools.Current is tsm.Fitting)
				{
					tsm.Fitting fit = bools.Current as tsm.Fitting;
					tsg.Line ls0 = new tsg.Line(fit.Plane.Origin, fit.Plane.AxisX);

					ls.Add(newOBB(part as tsm.Beam).IntersectionWith(ls0));

				}

			}

			foreach (tsg.LineSegment ls0 in ls)
			{
				tsm.ControlLine cl = new tsm.ControlLine(newOBB(part as tsm.Beam).IntersectionWith(ls0), false);
				//cl.Insert();
			}

			return ls;
		}

		private static List<tsg.LineSegment> GetLinesFromBools(tsm.Part part)
		{
			List<tsg.LineSegment> ls = new List<tsg.LineSegment>();
			//tsm.Part part = moe.Current as tsm.Part;
			tsm.ModelObjectEnumerator bools = part.GetChildren();
			while (bools.MoveNext())
			{
				if (bools.Current is tsm.BooleanPart)
				{
					tsm.Part cp = (bools.Current as tsm.BooleanPart).OperativePart;
					// имеют пересечения
					if (newOBB(part as tsm.Beam).Intersects(newOBB(cp)))
					{
						// generate line segment
						for (int i = 0; i < cp.GetReferenceLine(true).Count; i++)
						{
							tsg.LineSegment ls0 =
								new tsg.LineSegment(cp.GetReferenceLine(true)[i] as tsg.Point,
													cp.GetReferenceLine(true)[i == cp.GetReferenceLine(true).Count - 1 ? 0 : i + 1] as tsg.Point);
							// debug
							tsg.LineSegment result = newOBB(part as tsm.Beam).IntersectionWith(ls0);
							if (result != null) ls.Add(result);

							//tsm.ControlLine cl = new tsm.ControlLine(newOBB(part as tsm.Beam).IntersectionWith(ls0), false);
							//cl.Insert();
						}
					}
				}
			}

			return ls;
		}

		private static tsg.Point icp(tsm.ContourPoint c)
		{
			return new tsg.Point(c.X, c.Y, c.Z);
		}

		// Для расстановки по y
		private static void GetLinesFromBools(tsm.Part part, tsd.ViewBase vb, tsg.Matrix m, bool text = false)
		{

			tsm.ModelObjectEnumerator bools = part.GetChildren();
			while (bools.MoveNext())
			{
				if (bools.Current is tsm.BooleanPart)
				{
					tsm.Part cp = (bools.Current as tsm.BooleanPart).OperativePart;

					if (text && cp is tsm.ContourPlate)
					{
						foreach (tsm.ContourPoint cp00 in (cp as tsm.ContourPlate).Contour.ContourPoints)
						{
							if (cp00.Chamfer.Type != tsm.Chamfer.ChamferTypeEnum.CHAMFER_NONE &&
								(cp00.Chamfer.X != 0 || cp00.Chamfer.Y != 0))
							{

								string itext = "R" + (cp00.Chamfer.X == 0 ? cp00.Chamfer.Y : cp00.Chamfer.X).ToString();
								//InsertText(vb, itext, rp(icp(cp00), m));
							}

						}
					}

					// имеют пересечения
					if (newOBB(part as tsm.Beam).Intersects(newOBB(cp)))
					{
						// generate line segment
						PointList pyB = new PointList();
						for (int i = 0; i < cp.GetReferenceLine(true).Count; i++)
						{
							tsg.LineSegment ls0 =
								new tsg.LineSegment(cp.GetReferenceLine(true)[i] as tsg.Point,
													cp.GetReferenceLine(true)[i == cp.GetReferenceLine(true).Count - 1 ? 0 : i + 1] as tsg.Point);
							// debug
							tsg.LineSegment result = newOBB(part as tsm.Beam).IntersectionWith(ls0);
							if (result != null)
							{
								tsd.Line l = new tsd.Line(vb, rp(result.Point1, m), rp(result.Point2, m), 0);
								l.Insert();

								if (tsg.Parallel.LineSegmentToLineSegment(result, GetCenterLine(part)))
								{

									//pyB.Clear();
									pyB.Add(rp(result.Point1, m)); pyB.Add(rp(result.Point2, m));

									tsg.Point p = rp(tsg.Projection.PointToLine(result.Point1, new tsg.Line(GetCenterLine(part))), m);

									pyB.Add(p);


								}
								if (text && cp is tsm.ContourPlate)
								{
									List<tsm.ContourPoint> points = new List<tsm.ContourPoint>();
									foreach (tsm.ContourPoint cpi in (cp as tsm.ContourPlate).Contour.ContourPoints)
									{
										if (points.Count == 2) break;
										if ((result.Point1.X == icp(cpi).X && result.Point1.Y == icp(cpi).Y && result.Point1.Z == icp(cpi).Z) ||
											(result.Point2.X == icp(cpi).X && result.Point2.Y == icp(cpi).Y && result.Point2.Z == icp(cpi).Z))
										{
											points.Add(cpi);
										}
									}
									if (points.Count > 1)
									{
										Dictionary<tsg.Point, double> dic = LineRad.Get(points[0], points[1]);
										foreach (KeyValuePair<tsg.Point, double> k in dic)
										{
											InsertText(vb, "R" + k.Value.ToString(), rp(k.Key, m));
										}
									}
									else if (points.Count == 1)
									{
										string itext = "R" + (points[0].Chamfer.X == 0 ? points[0].Chamfer.Y : points[0].Chamfer.X).ToString();
										InsertText(vb, itext, rp(icp(points[0]), m));
									}

								}
							}
						}
						tsd.StraightDimensionSetHandler sds = new tsd.StraightDimensionSetHandler();
						sds.CreateDimensionSet(vb, pyB, new tsg.Vector(1, 0, 0), (10 * (vb as tsd.View).Attributes.Scale));
					}
				}
			}


		}


		private static class LineRad
		{

			public static Dictionary<tsg.Point, double> Get(tsm.ContourPoint cp1, tsm.ContourPoint cp2)
			{
				Dictionary<tsg.Point, double> dic = new Dictionary<tsg.Point, double>();
				double R1 = 0;
				double R2 = 0;

				if (cp1.Chamfer.Type != tsm.Chamfer.ChamferTypeEnum.CHAMFER_NONE && (cp1.Chamfer.X != 0 || cp1.Chamfer.Y != 0))
					R1 = (cp1.Chamfer.X == 0 ? cp1.Chamfer.Y : cp1.Chamfer.X);
				if (cp2.Chamfer.Type != tsm.Chamfer.ChamferTypeEnum.CHAMFER_NONE && (cp2.Chamfer.X != 0 || cp2.Chamfer.Y != 0))
					R2 = (cp2.Chamfer.X == 0 ? cp2.Chamfer.Y : cp2.Chamfer.X);

				if (R1 == R2 && R1 > 0.001 && tsg.Distance.PointToPoint(icp(cp1), icp(cp2)) == R1 * 2)
				{
					dic.Add(middle(cp1, cp2), R1);
				}
				else {
					if (R1 > 0.001) dic.Add(icp(cp1), R1);
					if (R2 > 0.001) dic.Add(icp(cp2), R2);
				}
				return dic;
			}

			private static tsg.Point middle(tsm.ContourPoint cp1, tsm.ContourPoint cp2)
			{
				return new tsg.Point(
					(icp(cp1).X + icp(cp2).X) * 0.5,
					(icp(cp1).Y + icp(cp2).Y) * 0.5,
					(icp(cp1).Z + icp(cp2).Z) * 0.5);
			}
			private static tsg.Point icp(tsm.ContourPoint c)
			{
				return new tsg.Point(c.X, c.Y, c.Z);
			}
		}

		private static tsg.Point CalculateCenterPoint(tsg.Point min, tsg.Point max)
		{
			double x = min.X + ((max.X - min.X) / 2);
			double y = min.Y + ((max.Y - min.Y) / 2);
			double z = min.Z + ((max.Z - min.Z) / 2);

			return new tsg.Point(x, y, z);
		}

		private static tsg.OBB newOBB(tsm.Part beam)
		{
			tsg.OBB obb = null;

			if (beam != null)
			{

				tsm.WorkPlaneHandler workPlaneHandler = new tsm.Model().GetWorkPlaneHandler();
				tsm.TransformationPlane originalTransformationPlane = workPlaneHandler.GetCurrentTransformationPlane();

				tsm.Solid solid = beam.GetSolid();
				tsg.Point minPointInCurrentPlane = solid.MinimumPoint;
				tsg.Point maxPointInCurrentPlane = solid.MaximumPoint;

				tsg.Point centerPoint = CalculateCenterPoint(minPointInCurrentPlane, maxPointInCurrentPlane);

				tsg.CoordinateSystem coordSys = beam.GetCoordinateSystem();
				tsm.TransformationPlane localTransformationPlane = new tsm.TransformationPlane(coordSys);
				workPlaneHandler.SetCurrentTransformationPlane(localTransformationPlane);

				solid = beam.GetSolid();
				tsg.Point minPoint = solid.MinimumPoint;
				tsg.Point maxPoint = solid.MaximumPoint;
				double extent0 = (maxPoint.X - minPoint.X) / 2;
				double extent1 = (maxPoint.Y - minPoint.Y) / 2;
				double extent2 = (maxPoint.Z - minPoint.Z) / 2;

				workPlaneHandler.SetCurrentTransformationPlane(originalTransformationPlane);

				obb = new tsg.OBB(centerPoint, coordSys.AxisX, coordSys.AxisY,
								coordSys.AxisX.Cross(coordSys.AxisY), extent0, extent1, extent2);
			}

			return obb;
		}
	}
}
