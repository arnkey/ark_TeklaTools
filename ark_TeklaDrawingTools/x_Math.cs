﻿// Type: x_algor.x_Math
// Assembly: WeldApp, Version=1.15.12.29, Culture=neutral, PublicKeyToken=null
// MVID: 8148BD28-E7ED-4BDF-BDEA-0ED2A6A16AE0
// Assembly location: C:\TeklaStructures\20.0\Environments\Common\macros\drawings\WeldApp.exe

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model;

namespace ArkLib.x_algoritms
{
  public static class x_Math
  {
    public static class Middel
    {
      public static Point Point(Point p1, Point p2)
      {
        return new Point((p1.X + p2.X) / 2.0, (p1.Y + p2.Y) / 2.0, (p1.Z + p2.Z) / 2.0);
      }

      public static GeometricPlane Plane(GeometricPlane plane1, GeometricPlane plane2)
      {
        GeometricPlane geometricPlane = new GeometricPlane();
        Point point = x_Math.Middel.Point(plane1.Origin, plane2.Origin);
        geometricPlane.Origin = point;
        Tekla.Structures.Geometry3d.Vector vector = x_New.Vector(plane1.Normal);
        geometricPlane.Normal = vector;
        return geometricPlane;
      }
    }

    public static class Vector
    {
      public static double Angle_180(Tekla.Structures.Geometry3d.Vector v1, Tekla.Structures.Geometry3d.Vector v2)
      {
        double d = (v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z) / (x_Math.Vector.Length(v1) * x_Math.Vector.Length(v2));
        if (d > 1.0)
          d = 1.0;
        if (d < -1.0)
          d = -1.0;
        return Math.Acos(d);
      }

      public static double Angle_90(Tekla.Structures.Geometry3d.Vector v1, Tekla.Structures.Geometry3d.Vector v2)
      {
        double num = x_Math.Vector.Angle_180(v1, v2);
        if (num > Math.PI / 2.0)
          num = Math.PI - num;
        return num;
      }

      public static double Length(Tekla.Structures.Geometry3d.Vector v)
      {
        return Math.Sqrt(v.X * v.X + v.Y * v.Y + v.Z * v.Z);
      }

      public static bool VectorRules(Model model, Tekla.Structures.Geometry3d.Vector vector)
      {
        Matrix transformationMatrixToGlobal = model.GetWorkPlaneHandler().GetCurrentTransformationPlane().TransformationMatrixToGlobal;
        Point point = new Point(vector.X, vector.Y, vector.Z);
        Point p1 = new Point(0.0, 0.0, 0.0);
        Point begin = transformationMatrixToGlobal.Transform(p1);
        Point p2 = point;
        Point end = transformationMatrixToGlobal.Transform(p2);
        vector = x_New.Vector(begin, end);
        Tekla.Structures.Geometry3d.Vector v2_1 = new Tekla.Structures.Geometry3d.Vector(1.0, 0.0, 0.0);
        Tekla.Structures.Geometry3d.Vector v2_2 = new Tekla.Structures.Geometry3d.Vector(0.0, 1.0, 0.0);
        Tekla.Structures.Geometry3d.Vector v2_3 = new Tekla.Structures.Geometry3d.Vector(0.0, 0.0, 1.0);
        double num1 = x_Math.Vector.Angle_90(vector, v2_1);
        double num2 = x_Math.Vector.Angle_90(vector, v2_2);
        double num3 = x_Math.Vector.Angle_90(vector, v2_3);
        if (num2 < num1)
        {
          num1 = num2;
          v2_1 = v2_2;
        }
        if (num3 < num1)
          v2_1 = v2_3;
        return x_Math.Vector.CoDirectional(vector, v2_1);
      }

      public static void CheckRule(Model model, Tekla.Structures.Geometry3d.Vector vector)
      {
        if (x_Math.Vector.VectorRules(model, vector))
          return;
        x_Math.Opposite.Vector(vector);
      }

      public static bool IsZero(Tekla.Structures.Geometry3d.Vector v)
      {
        return Math.Abs(v.X) <= x_Const.Eps && Math.Abs(v.Y) <= x_Const.Eps && Math.Abs(v.Z) <= x_Const.Eps;
      }

      public static bool IsParallel(Tekla.Structures.Geometry3d.Vector v1, Tekla.Structures.Geometry3d.Vector v2)
      {
        return Parallel.VectorToVector(v1, v2);
      }

      public static bool IsOrtogonal(Tekla.Structures.Geometry3d.Vector v1, Tekla.Structures.Geometry3d.Vector v2)
      {
        return x_Math.Vector.Angle_90(v1, v2) >= Math.PI / 2.0 - x_Const.EpsAng;
      }

      public static bool CoDirectional(Tekla.Structures.Geometry3d.Vector v1, Tekla.Structures.Geometry3d.Vector v2)
      {
        return x_Math.Vector.CoDirectionalStrong(v1, v2, Math.PI / 2.0);
      }

      public static bool CoDirectionalStrong(Tekla.Structures.Geometry3d.Vector v1, Tekla.Structures.Geometry3d.Vector v2)
      {
        return x_Math.Vector.CoDirectionalStrong(v1, v2, Math.PI / 4.0);
      }

      public static bool CoDirectionalStrong(Tekla.Structures.Geometry3d.Vector v1, Tekla.Structures.Geometry3d.Vector v2, double angle)
      {
        return x_Math.Vector.Angle_180(v1, v2) <= angle;
      }

      public static void GetOrtogonalVectors(out Tekla.Structures.Geometry3d.Vector dirX, out Tekla.Structures.Geometry3d.Vector dirY, Tekla.Structures.Geometry3d.Vector normal)
      {
        dirX = new Tekla.Structures.Geometry3d.Vector(0.0, 0.0, 0.0);
        int num = 0;
        if (x_Math.Compare.Values(normal.X, 0.0))
          dirX.X = 1.0;
        else
          ++num;
        if (x_Math.Compare.Values(normal.Y, 0.0))
          dirX.Y = 1.0;
        else
          ++num;
        if (x_Math.Compare.Values(normal.Z, 0.0))
          dirX.Z = 1.0;
        else
          ++num;
        if (num == 3)
        {
          dirX.X = 1.0;
          dirX.Y = 1.0;
          dirX.Z = -(normal.X + normal.Y) / normal.Z;
        }
        double NewLength = 100.0;
        dirX.Normalize(NewLength);
        dirY = normal.Cross(dirX);
      }

      public static Tekla.Structures.Geometry3d.Vector Summ(Tekla.Structures.Geometry3d.Vector v1, Tekla.Structures.Geometry3d.Vector v2)
      {
        return new Tekla.Structures.Geometry3d.Vector(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
      }
    }

    public static class Rotate
    {
      public static void Line(ref Line line, double angle, Tekla.Structures.Geometry3d.Vector rotateAxis)
      {
        Point point = MatrixFactory.Rotate(angle, rotateAxis).Transform(new Point(line.Direction.X, line.Direction.Y, line.Direction.Z));
        line.Direction = x_New.Vector(point);
      }

      public static Line LineNew(Line line, double angle, Tekla.Structures.Geometry3d.Vector rotateAxis)
      {
        Point point = MatrixFactory.Rotate(angle, rotateAxis).Transform(new Point(line.Direction.X, line.Direction.Y, line.Direction.Z));
        return x_New.Line(line.Origin, x_New.Vector(point));
      }

      public static Line LineNew(Line line, Matrix matrix)
      {
        Point p1 = new Point(line.Origin.X, line.Origin.Y, line.Origin.Z);
        Point p2 = new Point(line.Direction.X, line.Direction.Y, line.Direction.Z);
        Point Point = matrix.Transform(p1);
        Point point = matrix.Transform(p2);
        return new Line(Point, new Tekla.Structures.Geometry3d.Vector(point.X, point.Y, point.Z));
      }

      public static Tekla.Structures.Geometry3d.Vector VectorNew(Tekla.Structures.Geometry3d.Vector vector, double angle, Tekla.Structures.Geometry3d.Vector rotateAxis)
      {
        Matrix matrix = MatrixFactory.Rotate(angle, rotateAxis);
        return x_Math.Rotate.VectorNew(vector, matrix);
      }

      public static Tekla.Structures.Geometry3d.Vector VectorNew(Tekla.Structures.Geometry3d.Vector vector, Matrix matrix)
      {
        Point p = new Point(vector.X, vector.Y, vector.Z);
        Point point = matrix.Transform(p);
        return new Tekla.Structures.Geometry3d.Vector(point.X, point.Y, point.Z);
      }

      public static void Plane(GeometricPlane plane, double angle, Tekla.Structures.Geometry3d.Vector rotateAxis)
      {
        Point point = MatrixFactory.Rotate(angle, rotateAxis).Transform(new Point(plane.Normal.X, plane.Normal.Y, plane.Normal.Z));
        plane.Normal = new Tekla.Structures.Geometry3d.Vector(point.X, point.Y, point.Z);
      }

      public static GeometricPlane PlaneNew(GeometricPlane plane, Matrix matrix)
      {
        Point p1 = new Point(plane.Origin.X, plane.Origin.Y, plane.Origin.Z);
        Point p2 = new Point(plane.Normal.X, plane.Normal.Y, plane.Normal.Z);
        Point Origin = matrix.Transform(p1);
        Point point = matrix.Transform(p2);
        return new GeometricPlane(Origin, new Tekla.Structures.Geometry3d.Vector(point.X, point.Y, point.Z));
      }
    }

    public static class Translate
    {
      public static Point PointNew(Point Point, Tekla.Structures.Geometry3d.Vector Dir)
      {
        Point point = new Point(Point);
        double X = Dir.X;
        double Y = Dir.Y;
        double Z = Dir.Z;
        point.Translate(X, Y, Z);
        return point;
      }

      public static void Point(Point Point, Tekla.Structures.Geometry3d.Vector Dir)
      {
        Point.Translate(Dir.X, Dir.Y, Dir.Z);
      }

      public static void Point(Point Point, Tekla.Structures.Geometry3d.Vector Dir, double Length)
      {
        if (Length == 0.0)
          return;
        Tekla.Structures.Geometry3d.Vector vector = new Tekla.Structures.Geometry3d.Vector(Dir.X, Dir.Y, Dir.Z);
        vector.Normalize(Length);
        Point.Translate(vector.X, vector.Y, vector.Z);
      }

      public static Point PointNew(Point Point, Tekla.Structures.Geometry3d.Vector Dir, double Length)
      {
        Tekla.Structures.Geometry3d.Vector vector = new Tekla.Structures.Geometry3d.Vector(Dir.X, Dir.Y, Dir.Z);
        Point point = new Point(Point);
        vector.Normalize(Length);
        double X = vector.X;
        double Y = vector.Y;
        double Z = vector.Z;
        point.Translate(X, Y, Z);
        return point;
      }

      public static Line LineNew(Line line, Tekla.Structures.Geometry3d.Vector Dir)
      {
        Line line1 = x_New.Line(line);
        x_Math.Translate.Point(line1.Origin, Dir);
        return line1;
      }

      public static Line LineNew(Line line, Tekla.Structures.Geometry3d.Vector Dir, double Length)
      {
        Line line1 = x_New.Line(line);
        x_Math.Translate.Point(line1.Origin, Dir, Length);
        return line1;
      }

      public static void Line(ref Line Line, Point Point, double Length)
      {
        Point point = Tekla.Structures.Geometry3d.Projection.PointToLine(Point, Line);
        Tekla.Structures.Geometry3d.Vector Dir = new Tekla.Structures.Geometry3d.Vector(Point.X - point.X, Point.Y - point.Y, Point.Z - point.Z);
        x_Math.Translate.Point(Line.Origin, Dir, Length);
      }

      public static Line LineNew(Line Line, Point Point, double Length)
      {
        Line Line1 = x_New.Line(Line.Origin, Line.Direction);
        x_Math.Translate.Line(ref Line1, Point, Length);
        return Line1;
      }

      public static void Line(ref Line Line, Tekla.Structures.Geometry3d.Vector Dir, double Length)
      {
        x_Math.Translate.Point(Line.Origin, Dir, Length);
      }

      public static void Segment(LineSegment segment, Tekla.Structures.Geometry3d.Vector Dir, double Length)
      {
        x_Math.Translate.Point(segment.Point1, Dir, Length);
        x_Math.Translate.Point(segment.Point2, Dir, Length);
      }

      public static LineSegment SegmentNew(LineSegment segment, Tekla.Structures.Geometry3d.Vector Dir, double Length)
      {
        LineSegment lineSegment = new LineSegment();
        Point point1 = x_Math.Translate.PointNew(segment.Point1, Dir, Length);
        lineSegment.Point1 = point1;
        Point point2 = x_Math.Translate.PointNew(segment.Point2, Dir, Length);
        lineSegment.Point2 = point2;
        return lineSegment;
      }

      public static GeometricPlane PlaneNew(GeometricPlane Plane, Tekla.Structures.Geometry3d.Vector Dir, double Length)
      {
        GeometricPlane geometricPlane = new GeometricPlane(new Point(Plane.Origin.X, Plane.Origin.Y, Plane.Origin.Z), new Tekla.Structures.Geometry3d.Vector(Plane.Normal.X, Plane.Normal.Y, Plane.Normal.Z));
        x_Math.Translate.Point(geometricPlane.Origin, Dir, Length);
        return geometricPlane;
      }

      public static void Plane(GeometricPlane Plane, Tekla.Structures.Geometry3d.Vector Dir, double Length)
      {
        x_Math.Translate.Point(Plane.Origin, Dir, Length);
      }
    }

    public static class Opposite
    {
      public static Tekla.Structures.Geometry3d.Vector VectorNew(Tekla.Structures.Geometry3d.Vector vector)
      {
        return new Tekla.Structures.Geometry3d.Vector(-vector.X, -vector.Y, -vector.Z);
      }

      public static void Vector(Tekla.Structures.Geometry3d.Vector vector)
      {
        vector.X = -vector.X;
        vector.Y = -vector.Y;
        vector.Z = -vector.Z;
      }

      public static void Plane(GeometricPlane plane)
      {
        plane.Normal.X = -plane.Normal.X;
        plane.Normal.Y = -plane.Normal.Y;
        plane.Normal.Z = -plane.Normal.Z;
      }

      public static GeometricPlane PlaneNew(GeometricPlane plane)
      {
        GeometricPlane geometricPlane = x_New.GeometricPlane(plane);
        geometricPlane.Normal.X = -geometricPlane.Normal.X;
        geometricPlane.Normal.Y = -geometricPlane.Normal.Y;
        geometricPlane.Normal.Z = -geometricPlane.Normal.Z;
        return geometricPlane;
      }
    }

    public static class Projection
    {
      public static void Segment(ref LineSegment segment, GeometricPlane plane)
      {
        if (segment == (LineSegment) null)
          return;
        segment.Point1 = Tekla.Structures.Geometry3d.Projection.PointToPlane(segment.Point1, plane);
        segment.Point2 = Tekla.Structures.Geometry3d.Projection.PointToPlane(segment.Point2, plane);
      }

      public static LineSegment SegmentNew(LineSegment segment, GeometricPlane plane)
      {
        if (segment == (LineSegment) null)
          return (LineSegment) null;
        LineSegment lineSegment = new LineSegment();
        Point point1 = Tekla.Structures.Geometry3d.Projection.PointToPlane(segment.Point1, plane);
        lineSegment.Point1 = point1;
        Point point2 = Tekla.Structures.Geometry3d.Projection.PointToPlane(segment.Point2, plane);
        lineSegment.Point2 = point2;
        return lineSegment;
      }

      public static void Line(Line line, GeometricPlane plane)
      {
        line.Origin = Tekla.Structures.Geometry3d.Projection.PointToPlane(line.Origin, plane);
      }

      public static Line LineNew(Line line, GeometricPlane plane)
      {
        Line line1 = x_New.Line(line);
        Point point = Tekla.Structures.Geometry3d.Projection.PointToPlane(line1.Origin, plane);
        line1.Origin = point;
        Tekla.Structures.Geometry3d.Vector vector = x_Math.Projection.VectorNew(line1.Direction, plane);
        line1.Direction = vector;
        return line1;
      }

      public static void Vector(Tekla.Structures.Geometry3d.Vector vector, GeometricPlane plane)
      {
        Point point1 = Tekla.Structures.Geometry3d.Projection.PointToPlane(new Point(0.0, 0.0, 0.0), plane);
        Point point2 = Tekla.Structures.Geometry3d.Projection.PointToPlane(new Point(vector.X, vector.Y, vector.Z), plane);
        vector.X = point2.X - point1.X;
        vector.Y = point2.Y - point1.Y;
        vector.Z = point2.Z - point1.Z;
      }

      public static Tekla.Structures.Geometry3d.Vector VectorNew(Tekla.Structures.Geometry3d.Vector vector, GeometricPlane plane)
      {
        Point point1 = Tekla.Structures.Geometry3d.Projection.PointToPlane(new Point(0.0, 0.0, 0.0), plane);
        Point point2 = Tekla.Structures.Geometry3d.Projection.PointToPlane(new Point(vector.X, vector.Y, vector.Z), plane);
        return new Tekla.Structures.Geometry3d.Vector(point2.X - point1.X, point2.Y - point1.Y, point2.Z - point1.Z);
      }
    }

    public static class Compare
    {
      public static bool Points(Point p1, Point p2)
      {
        return Math.Abs(p1.X - p2.X) <= x_Const.Eps && Math.Abs(p1.Y - p2.Y) <= x_Const.Eps && Math.Abs(p1.Z - p2.Z) <= x_Const.Eps;
      }

      public static bool Points(ContourPoint cp, Point p)
      {
        return Math.Abs(cp.X - p.X) <= x_Const.Eps && Math.Abs(cp.Y - p.Y) <= x_Const.Eps && Math.Abs(cp.Z - p.Z) <= x_Const.Eps;
      }

      public static bool Points(ContourPoint cp1, ContourPoint cp2)
      {
        return Math.Abs(cp1.X - cp2.X) <= x_Const.Eps && Math.Abs(cp1.Y - cp2.Y) <= x_Const.Eps && Math.Abs(cp1.Z - cp2.Z) <= x_Const.Eps;
      }

      public static bool Chamfers(Chamfer ch1, Chamfer ch2)
      {
        return ch1.Type == ch2.Type && Math.Abs(ch1.X - ch2.X) <= x_Const.Eps && (Math.Abs(ch1.Y - ch2.Y) <= x_Const.Eps && Math.Abs(ch1.DZ1 - ch2.DZ1) <= x_Const.Eps) && Math.Abs(ch1.DZ2 - ch2.DZ2) <= x_Const.Eps;
      }

      public static bool Values(double d1, double d2)
      {
        return Math.Abs(d1 - d2) <= x_Const.Eps;
      }

      public static bool Angle(double d1, double d2)
      {
        return Math.Abs(d1 - d2) <= x_Const.Eps / 100.0;
      }

      public static bool Plane(GeometricPlane plane1, GeometricPlane plane2)
      {
        return Distance.PointToPlane(plane1.Origin, plane2) <= x_Const.Eps && x_Math.Vector.Angle_90(plane1.Normal, plane2.Normal) <= x_Const.EpsAng;
      }
    }

    public static class Intersection
    {
      public static Point LineAndLine(Line line1, Line line2)
      {
        return Tekla.Structures.Geometry3d.Intersection.LineToLine(line1, line2).Point1;
      }

      public static Point LineAndVector(Line line, Tekla.Structures.Geometry3d.Vector vector)
      {
        Line line2 = new Line(new Point(0.0, 0.0, 0.0), vector);
        return x_Math.Intersection.LineAndLine(line, line2);
      }

      public static LineSegment SegmentAndSegment(LineSegment segment1, LineSegment segment2)
      {
        Line line1 = new Line(segment1.Point1, segment1.Point2);
        Line line2 = new Line(segment2.Point1, segment2.Point2);
        LineSegment lineSegment = Tekla.Structures.Geometry3d.Intersection.LineToLine(line1, line2);
        if (lineSegment == (LineSegment) null)
          return (LineSegment) null;
        Point Point1_1 = Tekla.Structures.Geometry3d.Projection.PointToLine(lineSegment.Point1, line1);
        if (Distance.PointToPoint(Point1_1, segment1.Point1) + Distance.PointToPoint(Point1_1, segment1.Point2) > segment1.Length() + x_Const.Eps)
          return (LineSegment) null;
        Point Point1_2 = Tekla.Structures.Geometry3d.Projection.PointToLine(lineSegment.Point1, line2);
        if (Distance.PointToPoint(Point1_2, segment2.Point1) + Distance.PointToPoint(Point1_2, segment2.Point2) > segment2.Length() + x_Const.Eps)
          return (LineSegment) null;
        else
          return lineSegment;
      }

      public static ArrayList SolidAndLine(Tekla.Structures.Model.Solid solid, Point point1, Point point2)
      {
        double Length = 1000000000.0;
        Tekla.Structures.Geometry3d.Vector Dir = new Tekla.Structures.Geometry3d.Vector(point2.X - point1.X, point2.Y - point1.Y, point2.Z - point1.Z);
        Point point1_1 = x_Math.Translate.PointNew(point1, Dir, Length);
        Point point2_1 = x_Math.Translate.PointNew(point2, Dir, -Length);
        return solid.Intersect(point1_1, point2_1);
      }

      public static ArrayList SolidAndLine(Tekla.Structures.Model.Solid solid, Line line, Point point)
      {
        double Length = 1000000.0;
        Point point1 = x_Math.Translate.PointNew(point, line.Direction, Length);
        Point point2 = x_Math.Translate.PointNew(point, line.Direction, -Length);
        return solid.Intersect(point1, point2);
      }

      public static List<Point> SolidAndPlane(Tekla.Structures.Model.Solid solid, GeometricPlane section)
      {
        Point point1 = x_New.Point(section.Origin);
        Point point2 = x_New.Point(section.Origin);
        Point point3 = x_New.Point(section.Origin);
        Tekla.Structures.Geometry3d.Vector normal = section.Normal;
        Tekla.Structures.Geometry3d.Vector dirX = (Tekla.Structures.Geometry3d.Vector) null;
        Tekla.Structures.Geometry3d.Vector dirY = (Tekla.Structures.Geometry3d.Vector) null;
        x_Math.Vector.GetOrtogonalVectors(out dirX, out dirY, normal);
        point2.Translate(dirX.X, dirX.Y, dirX.Z);
        point3.Translate(dirY.X, dirY.Y, dirY.Z);
        ArrayList arrayList = solid.Intersect(point1, point2, point3);
        List<Point> list = new List<Point>();
        if (arrayList.Count == 0)
          return list;
        foreach (Point point in arrayList[0] as ArrayList)
          list.Add(point);
        return list;
      }

      public static ArrayList SolidAndPlaneArray(Tekla.Structures.Model.Solid solid, GeometricPlane section)
      {
        Point point1 = x_New.Point(section.Origin);
        Point point2 = x_New.Point(section.Origin);
        Point point3 = x_New.Point(section.Origin);
        Tekla.Structures.Geometry3d.Vector normal = section.Normal;
        Tekla.Structures.Geometry3d.Vector dirX = (Tekla.Structures.Geometry3d.Vector) null;
        Tekla.Structures.Geometry3d.Vector dirY = (Tekla.Structures.Geometry3d.Vector) null;
        x_Math.Vector.GetOrtogonalVectors(out dirX, out dirY, normal);
        point2.Translate(dirX.X, dirX.Y, dirX.Z);
        point3.Translate(dirY.X, dirY.Y, dirY.Z);
        ArrayList arrayList = solid.Intersect(point1, point2, point3);
        if (arrayList.Count == 0)
          return arrayList;
        else
          return arrayList[0] as ArrayList;
      }

      public static List<Point> SolidAndPlane_PolyLine(Part part, GeometricPlane section)
      {
        ContourPlate contourPlate = part as ContourPlate;
        if (contourPlate == null || contourPlate.GetBooleans().GetSize() == 0)
          return x_Math.Intersection.SolidAndPlane(part.GetSolid(), section);
        List<Point> list = new List<Point>();
        ArrayList contourPoints = contourPlate.Contour.ContourPoints;
        Point point1 = Tekla.Structures.Geometry3d.Intersection.LineSegmentToPlane(x_New.LineSegment(contourPoints[contourPoints.Count - 1] as ContourPoint, contourPoints[0] as ContourPoint), section);
        if (point1 != (Point) null)
          list.Add(point1);
        for (int index = 1; index < contourPoints.Count; ++index)
        {
          Point point2 = Tekla.Structures.Geometry3d.Intersection.LineSegmentToPlane(x_New.LineSegment(contourPoints[index - 1] as ContourPoint, contourPoints[index] as ContourPoint), section);
          if (point2 != (Point) null)
            list.Add(point2);
        }
        return list;
      }

      public static bool SolidAndPlaneYesNo(Tekla.Structures.Model.Solid solid, GeometricPlane section)
      {
        List<Point> list = x_Math.Intersection.SolidAndPlane(solid, section);
        bool flag = false;
        if (list.Count > 0)
          flag = true;
        return flag;
      }
    }

    public static class Solid
    {
      public static bool PointIsInside(Tekla.Structures.Model.Solid solid, Point p)
      {
        double NewLength = 1000000000.0;
        Point Point2 = new Point(p);
        Tekla.Structures.Geometry3d.Vector vector = new Tekla.Structures.Geometry3d.Vector(1.0, 1.0, 1.0);
        vector.Normalize(NewLength);
        Point2.Translate(vector.X, vector.Y, vector.Z);
        LineSegment line = new LineSegment(p, Point2);
        ArrayList arrayList = solid.Intersect(line);
        return arrayList.Count != 0 && arrayList.Count % 2 != 0;
      }
    }

    public static class Transform
    {
      public static Tekla.Structures.Geometry3d.Vector VectorNew(Tekla.Structures.Geometry3d.Vector v, Matrix matrix)
      {
        Point p1 = new Point(0.0, 0.0, 0.0);
        Point p2 = x_New.Point(v);
        return x_New.Vector(matrix.Transform(p1), matrix.Transform(p2));
      }

      public static Point PointNew(Point point, TransformationPlane from, TransformationPlane to)
      {
        Point p = from.TransformationMatrixToGlobal.Transform(point);
        return to.TransformationMatrixToLocal.Transform(p);
      }

      public static GeometricPlane PlaneNew(GeometricPlane plane, TransformationPlane from, TransformationPlane to)
      {
        Point origin = plane.Origin;
        Point p1 = new Point(0.0, 0.0, 0.0);
        Point p2 = x_New.Point(plane.Normal);
        Point p3 = from.TransformationMatrixToGlobal.Transform(origin);
        Point p4 = from.TransformationMatrixToGlobal.Transform(p1);
        Point p5 = from.TransformationMatrixToGlobal.Transform(p2);
        return new GeometricPlane(to.TransformationMatrixToLocal.Transform(p3), x_New.Vector(to.TransformationMatrixToLocal.Transform(p4), to.TransformationMatrixToLocal.Transform(p5)));
      }
    }

    public static class Convert
    {
      public static double ToDouble(string str)
      {
        string oldValue = ",";
        if (oldValue == CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator)
          oldValue = ".";
        return System.Convert.ToDouble(str.Replace(oldValue, CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));
      }

      public static string RoundToString(double val)
      {
        return Math.Round(val).ToString();
      }
    }
  }
}
