﻿using System;

using tsd = Tekla.Structures.Drawing;
using tsg = Tekla.Structures.Geometry3d;

namespace ark_TeklaDrawingTools
{
	public class EmbededMark
	{
		public EmbededMark()
		{
			tsd.DrawingHandler dh = new tsd.DrawingHandler();
			tsd.Drawing draw = dh.GetActiveDrawing();

			tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();

			tsd.UI.Picker picker = dh.GetPicker();
			tsd.ViewBase vb = null;
			tsg.Point p = new tsg.Point(0, 0, 0);
			picker.PickPoint("", out p, out vb);

			while (doe.MoveNext())
			{
				if (doe.Current is tsd.Mark)
				{

					tsd.Mark m = doe.Current as tsd.Mark;
					m.InsertionPoint = p;
					m.Modify();
					m.InsertionPoint = p;
					m.Modify();

				}
			}
		}
	}
}
