﻿using System.Collections;
using tsd = Tekla.Structures.Drawing;

namespace ark_TeklaDrawingTools
{
   public class Numbering2016i
    {

       public string Run(string mask)
       {
           Numb numberator = new Numb(mask);

           tsd.DrawingHandler dh = new tsd.DrawingHandler();
           if (dh.GetConnectionStatus() == false) return "";
           //tsd.Drawing draw = dh.GetActiveDrawing();

           tsd.DrawingEnumerator doe = dh.GetDrawingSelector().GetSelected();

           while (doe.MoveNext())
           {
               tsd.Drawing draw = doe.Current as tsd.Drawing;
               draw.SetUserProperty("DR_SHEET", numberator.ToString());
              // draw.Title2 = "См. лист: " + numberator.ToString();


               ++numberator.StartNum;

               draw.Modify();
               draw.CommitChanges();
           }
           return numberator.ToString();
       }

        public struct Numb
        {
            public int StartNum;
            public string Mask;
            public Numb(string Start)
            {
                StartNum = 1;
                Mask = Start;
                if (Start != "")
                {
                    string decStr = "";
                    while (Mask.Length > 0)
                    {
                        if (Mask[Mask.Length - 1] >= '0' & Mask[Mask.Length - 1] <= '9')
                        {
                            decStr = Mask[Mask.Length - 1] + decStr;
                            Mask = Mask.Remove(Mask.Length - 1);
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (decStr != "")
                    {
                        StartNum = int.Parse(decStr);
                    }
                }
            }
            public override string ToString()
            {
                return Mask + StartNum.ToString();
            }


        }
        public class UDAItem
        {
            public string Name;
            public string Title;
            public string TitleO
            {
                get { return Title; }
            }
            public string NameO
            {
                get { return Name; }
            }
            public UDAItem(string name, string title)
            {
                Name = name; Title = title;
            }
        }
        public class StrNumComp : IComparer
        {
            int IComparer.Compare(System.Object x, System.Object y)
            {
                return (x as UDAItem).Title.CompareTo((y as UDAItem).Title);
            }
        }
    }
}
