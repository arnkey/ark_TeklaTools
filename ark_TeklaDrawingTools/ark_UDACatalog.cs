﻿using System;
using System.Collections.Generic;

using Tekla.Structures.Catalogs;
using Tekla.Structures.Dialog;


namespace ArkLib.ark_Tekla.Catalogs
{
    [Serializable]
    public static class ark_UDACatalog
    {
        public static Dictionary<string, string> Drawing_User_Attributes;
        public static void Load()
        {
            GetUDA_Drawings();
        }
        private static void GetUDA_Drawings()
        {
            Drawing_User_Attributes = new Dictionary<string, string>();
            CatalogHandler CH = new CatalogHandler();
            Localization TekLoc = new Localization();
            UserPropertyItemEnumerator UIE = CH.GetUserPropertyItems(CatalogObjectTypeEnum.ASSEMBLY_DRAWING);

            while (UIE.MoveNext())
            {
                UserPropertyItem PI = UIE.Current;
                try
                {
                    Drawing_User_Attributes.Add(PI.Name, (TekLoc.GetText(PI.GetLabel()) == "") ? PI.Name : TekLoc.GetText(PI.GetLabel()));
                }
                catch { }
            }
            UIE = CH.GetUserPropertyItems(CatalogObjectTypeEnum.MULTI_DRAWING);
            while (UIE.MoveNext())
            {
                UserPropertyItem PI = UIE.Current;
                try
                {
                    Drawing_User_Attributes.Add(PI.Name, (TekLoc.GetText(PI.GetLabel()) == "") ? PI.Name : TekLoc.GetText(PI.GetLabel()));
                }
                catch { }
            }
            UIE = CH.GetUserPropertyItems(CatalogObjectTypeEnum.SINGLE_PART_DRAWING);
            while (UIE.MoveNext())
            {
                UserPropertyItem PI = UIE.Current;
                try
                {
                    Drawing_User_Attributes.Add(PI.Name, (TekLoc.GetText(PI.GetLabel()) == "") ? PI.Name : TekLoc.GetText(PI.GetLabel()));
                }
                catch { }
            }
            UIE = CH.GetUserPropertyItems(CatalogObjectTypeEnum.GA_DRAWING);
            while (UIE.MoveNext())
            {
                UserPropertyItem PI = UIE.Current;
                try
                {
                    Drawing_User_Attributes.Add(PI.Name, (TekLoc.GetText(PI.GetLabel()) == "") ? PI.Name : TekLoc.GetText(PI.GetLabel()));
                }
                catch { }
            }
        }
    }
}
