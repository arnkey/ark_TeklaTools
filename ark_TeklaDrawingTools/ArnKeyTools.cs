﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using tsg = Tekla.Structures.Geometry3d;
using tsm = Tekla.Structures.Model;
using tsd = Tekla.Structures.Drawing;

namespace ark_TeklaDrawingTools
{
    public partial class ArnKeyTools : Form
    {
        public ArnKeyTools()
        {
            InitializeComponent();
        }


        tsg.Vector dirX = new tsg.Vector(1, 0, 0);
        tsg.Vector dirY = new tsg.Vector(0, 1, 0);
        tsg.Vector dirXY = new tsg.Vector(1, 1, 0);

        private void btn_dim_bolt_start_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
                tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                List<tsd.Bolt> bolts = new List<tsd.Bolt>();
                while (doe.MoveNext())
                    if (doe.Current is tsd.Bolt) bolts.Add(doe.Current as tsd.Bolt);

                if (bolts.Count == 0) return;
                tsd.ViewBase vb = bolts[0].GetView(); vb.Select();
                tsg.Matrix m = tsg.MatrixFactory.ToCoordinateSystem((vb as tsd.View).DisplayCoordinateSystem);


                tsd.PointList plY = new tsd.PointList();

                tsd.UI.Picker picker = dh.GetPicker();
                tsg.Point p1 = new tsg.Point();
                tsg.Point p2 = new tsg.Point();
                tsd.ViewBase vb0 = null;
                picker.PickPoint("", out p1, out vb0);
                plY.Add(p1);
                picker.PickPoint("", out p2, out vb0);

                foreach (tsd.Bolt b in bolts)
                {
                    tsm.BoltGroup bg = new tsm.Model().SelectModelObject(b.ModelIdentifier) as tsm.BoltGroup;

                    tsg.Point minY = m.Transform(bg.BoltPositions[0] as tsg.Point);
                    tsg.Point maxY = m.Transform(bg.BoltPositions[0] as tsg.Point);

                    foreach (tsg.Point p in bg.BoltPositions)
                    {
                        minY = ReturnMin(minY, m.Transform(p), 2);
                        maxY = ReturnMax(maxY, m.Transform(p), 2);
                    }

                    plY.Add(maxY);
                    tsd.StraightDimensionSetHandler sds_rel = new tsd.StraightDimensionSetHandler();
                    try
                    {
                        sds_rel.CreateDimensionSet(vb, new tsd.PointList() { minY, maxY },
                            (rb_dim_bolt_column.Checked == true ? dirX : dirY), 100);
                    }
                    catch { }
                    tsg.Point intPoint = new tsg.Point(p1);
                    intPoint.Y = maxY.Y;
                    try
                    {
                        sds_rel.CreateDimensionSet(vb, new tsd.PointList() { intPoint, maxY }, (rb_dim_bolt_column.Checked == true ? dirY : dirX), 100);
                    }
                    catch { }
                }

                plY.Add(p2);


                tsd.StraightDimensionSetHandler sds = new tsd.StraightDimensionSetHandler();
                try
                {
                    sds.CreateDimensionSet(vb, plY, (rb_dim_bolt_column.Checked == true ? dirX : dirY), 100);
                }
                catch { }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="type">0 - x, 1 - y</param>
        /// <returns></returns>
        tsg.Point ReturnMin(tsg.Point p1, tsg.Point p2, int type)
        {
            if (type == 0)
            {
                if (p1.X == Math.Min(p1.X, p2.X)) return p1;
                else return p2;
            }
            else if (type == 1)
            {
                if (p1.Y == Math.Min(p1.Y, p2.Y)) return p1;
                else return p2;
            }
            else
            {
                if (p1.Y == Math.Min(p1.Y, p2.Y) && p1.X == Math.Min(p1.X, p2.X))
                {
                    return p1;
                }
                else return p2;
            }
        }
        tsg.Point ReturnMax(tsg.Point p1, tsg.Point p2, int type)
        {
            if (type == 0)
            {
                if (p1.X == Math.Max(p1.X, p2.X)) return p1;
                else return p2;
            }
            else if (type == 1)
            {
                if (p1.Y == Math.Max(p1.Y, p2.Y)) return p1;
                else return p2;
            }
            else
            {
                if (p1.Y == Math.Max(p1.Y, p2.Y) && p1.X == Math.Max(p1.X, p2.X))
                {
                    return p1;
                }
                else return p2;
            }
        }

        private void btn_dim_part_start_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
             if (dh.GetConnectionStatus() == false) return;
             tsd.Drawing draw = dh.GetActiveDrawing();
             if (draw != null)
             {
                 tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                 List<tsd.Part> parts = new List<tsd.Part>();
                 while (doe.MoveNext())
                 {
                     if (doe.Current is tsd.Part) parts.Add(doe.Current as tsd.Part);
                 }

                 if (parts.Count == 0) return;

                 tsd.ViewBase vb = parts[0].GetView();

                 vb.Select();


                 tsg.Matrix m = tsg.MatrixFactory.ToCoordinateSystem((vb as tsd.View).DisplayCoordinateSystem);

                 tsd.PointList pl = new tsd.PointList();

                 tsd.UI.Picker picker = dh.GetPicker();
                 tsg.Point p1 = new tsg.Point();
                 tsg.Point p2 = new tsg.Point();
                 tsd.ViewBase vb0 = null;
                 picker.PickPoint("", out p1, out vb0);
                 pl.Add(p1);
                 picker.PickPoint("", out p2, out vb0);

                 foreach (tsd.Part p in parts)
                 {
                     tsm.Part part = new tsm.Model().SelectModelObject(p.ModelIdentifier) as tsm.Part;
                     if (rb_dim_part_cl.Checked)
                        pl.Add(m.Transform(part.GetReferenceLine(true)[0] as tsg.Point));
                     if (rb_dim_part_rl.Checked)
                        pl.Add(m.Transform(part.GetReferenceLine(true)[0] as tsg.Point));
                 }

                 pl.Add(p2);



                 //tsd.StraightDimension sd = new tsd.StraightDimension(bolts[0].GetView(), min, max, dir, 100);

                 tsd.StraightDimensionSetHandler sds = new tsd.StraightDimensionSetHandler();

                 if (rb_dim_part_column.Checked)
                    sds.CreateDimensionSet(vb, pl, dirX, 100);
                 else
                     sds.CreateDimensionSet(vb, pl, dirY, 100);
                     //sds.CreateDimensionSet(vb, pl, dirXY, 100);

             }
        }


        private void ArnKeyTools_Load(object sender, EventArgs e)
        {

        }
    }
}
