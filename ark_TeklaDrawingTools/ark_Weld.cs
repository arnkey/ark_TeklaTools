﻿using System;
using System.Windows.Forms;

using tsd = Tekla.Structures.Drawing;
using tsg = Tekla.Structures.Geometry3d;
using tsm = Tekla.Structures.Model;

namespace ark_TeklaDrawingTools
{
    public partial class ark_Weld : Form
    {
        public ark_Weld()
        {
            InitializeComponent();
        }

        private void ark_Weld_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
        }



        void Insert(int num, string text)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
                tsd.TextElement te = new tsd.TextElement("");
                tsd.ViewBase vb = draw.GetSheet();
                tsd.UI.Picker picker = dh.GetPicker();
                tsg.Point p = new tsg.Point();
                picker.PickPoint("", out p, out vb);
                tsd.Text t = new tsd.Text(vb, p, "arkWeld@" + num.ToString() + " " + text);
                t.Attributes.LoadAttributes("standard");
                t.TextString = "arkWeld@" + num.ToString() + " " + text;
                t.Insert();
            }
        }

        private void btn_pp_Click(object sender, EventArgs e)
        {
            Insert(3, "Полный провар");
        }

        private void btn_t1_Click(object sender, EventArgs e)
        {
            Insert(1, "Т1");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Insert(3, "Т3");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Insert(2, "Т6");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Insert(4, "Т7");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Insert(1, "У1");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Insert(7, "С2");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Insert(1, "Н1");
        }

    }
}
