﻿namespace ark_TeklaDrawingTools
{
    partial class ArnKeyTools
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_dim_bolt_start = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_dim_bolt_beam = new System.Windows.Forms.RadioButton();
            this.rb_dim_bolt_column = new System.Windows.Forms.RadioButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rb_dim_part_beam = new System.Windows.Forms.RadioButton();
            this.rb_dim_part_column = new System.Windows.Forms.RadioButton();
            this.btn_dim_part_start = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rb_dim_part_rl = new System.Windows.Forms.RadioButton();
            this.rb_dim_part_cl = new System.Windows.Forms.RadioButton();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(278, 260);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tabControl2);
            this.tabPage1.Location = new System.Drawing.Point(23, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(251, 252);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Размеры";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(245, 246);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_dim_bolt_start);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(237, 220);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Болты";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_dim_bolt_start
            // 
            this.btn_dim_bolt_start.Location = new System.Drawing.Point(6, 191);
            this.btn_dim_bolt_start.Name = "btn_dim_bolt_start";
            this.btn_dim_bolt_start.Size = new System.Drawing.Size(212, 23);
            this.btn_dim_bolt_start.TabIndex = 1;
            this.btn_dim_bolt_start.Text = "Начать";
            this.btn_dim_bolt_start.UseVisualStyleBackColor = true;
            this.btn_dim_bolt_start.Click += new System.EventHandler(this.btn_dim_bolt_start_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_dim_bolt_beam);
            this.groupBox1.Controls.Add(this.rb_dim_bolt_column);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 69);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ориентация";
            // 
            // rb_dim_bolt_beam
            // 
            this.rb_dim_bolt_beam.AutoSize = true;
            this.rb_dim_bolt_beam.Checked = true;
            this.rb_dim_bolt_beam.Location = new System.Drawing.Point(6, 42);
            this.rb_dim_bolt_beam.Name = "rb_dim_bolt_beam";
            this.rb_dim_bolt_beam.Size = new System.Drawing.Size(195, 17);
            this.rb_dim_bolt_beam.TabIndex = 1;
            this.rb_dim_bolt_beam.TabStop = true;
            this.rb_dim_bolt_beam.Text = "Балка (горизонтальный элемент)";
            this.rb_dim_bolt_beam.UseVisualStyleBackColor = true;
            // 
            // rb_dim_bolt_column
            // 
            this.rb_dim_bolt_column.AutoSize = true;
            this.rb_dim_bolt_column.Checked = true;
            this.rb_dim_bolt_column.Location = new System.Drawing.Point(6, 19);
            this.rb_dim_bolt_column.Name = "rb_dim_bolt_column";
            this.rb_dim_bolt_column.Size = new System.Drawing.Size(196, 17);
            this.rb_dim_bolt_column.TabIndex = 0;
            this.rb_dim_bolt_column.TabStop = true;
            this.rb_dim_bolt_column.Text = "Колонна (вертикальный элемент)";
            this.rb_dim_bolt_column.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.btn_dim_part_start);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(237, 220);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Деталь";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rb_dim_part_beam);
            this.groupBox3.Controls.Add(this.rb_dim_part_column);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(221, 69);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ориентация";
            // 
            // rb_dim_part_beam
            // 
            this.rb_dim_part_beam.AutoSize = true;
            this.rb_dim_part_beam.Checked = true;
            this.rb_dim_part_beam.Location = new System.Drawing.Point(6, 42);
            this.rb_dim_part_beam.Name = "rb_dim_part_beam";
            this.rb_dim_part_beam.Size = new System.Drawing.Size(195, 17);
            this.rb_dim_part_beam.TabIndex = 1;
            this.rb_dim_part_beam.TabStop = true;
            this.rb_dim_part_beam.Text = "Балка (горизонтальный элемент)";
            this.rb_dim_part_beam.UseVisualStyleBackColor = true;
            // 
            // rb_dim_part_column
            // 
            this.rb_dim_part_column.AutoSize = true;
            this.rb_dim_part_column.Checked = true;
            this.rb_dim_part_column.Location = new System.Drawing.Point(6, 19);
            this.rb_dim_part_column.Name = "rb_dim_part_column";
            this.rb_dim_part_column.Size = new System.Drawing.Size(196, 17);
            this.rb_dim_part_column.TabIndex = 0;
            this.rb_dim_part_column.TabStop = true;
            this.rb_dim_part_column.Text = "Колонна (вертикальный элемент)";
            this.rb_dim_part_column.UseVisualStyleBackColor = true;
            // 
            // btn_dim_part_start
            // 
            this.btn_dim_part_start.Location = new System.Drawing.Point(3, 191);
            this.btn_dim_part_start.Name = "btn_dim_part_start";
            this.btn_dim_part_start.Size = new System.Drawing.Size(221, 23);
            this.btn_dim_part_start.TabIndex = 3;
            this.btn_dim_part_start.Text = "Начать";
            this.btn_dim_part_start.UseVisualStyleBackColor = true;
            this.btn_dim_part_start.Click += new System.EventHandler(this.btn_dim_part_start_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rb_dim_part_rl);
            this.groupBox2.Controls.Add(this.rb_dim_part_cl);
            this.groupBox2.Location = new System.Drawing.Point(3, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(221, 73);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Привязка";
            // 
            // rb_dim_part_rl
            // 
            this.rb_dim_part_rl.AutoSize = true;
            this.rb_dim_part_rl.Location = new System.Drawing.Point(6, 42);
            this.rb_dim_part_rl.Name = "rb_dim_part_rl";
            this.rb_dim_part_rl.Size = new System.Drawing.Size(102, 17);
            this.rb_dim_part_rl.TabIndex = 1;
            this.rb_dim_part_rl.Text = "Опорная линия";
            this.rb_dim_part_rl.UseVisualStyleBackColor = true;
            // 
            // rb_dim_part_cl
            // 
            this.rb_dim_part_cl.AutoSize = true;
            this.rb_dim_part_cl.Checked = true;
            this.rb_dim_part_cl.Location = new System.Drawing.Point(6, 19);
            this.rb_dim_part_cl.Name = "rb_dim_part_cl";
            this.rb_dim_part_cl.Size = new System.Drawing.Size(125, 17);
            this.rb_dim_part_cl.TabIndex = 0;
            this.rb_dim_part_cl.TabStop = true;
            this.rb_dim_part_cl.Text = "Центральная линия";
            this.rb_dim_part_cl.UseVisualStyleBackColor = true;
            // 
            // ArnKeyTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 260);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ArnKeyTools";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ArnKeyTools";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ArnKeyTools_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb_dim_bolt_column;
        private System.Windows.Forms.Button btn_dim_bolt_start;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btn_dim_part_start;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rb_dim_part_rl;
        private System.Windows.Forms.RadioButton rb_dim_part_cl;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rb_dim_part_column;
        private System.Windows.Forms.RadioButton rb_dim_bolt_beam;
        private System.Windows.Forms.RadioButton rb_dim_part_beam;

    }
}