﻿// Type: x_algor.x_New
// Assembly: WeldApp, Version=1.15.12.29, Culture=neutral, PublicKeyToken=null
// MVID: 8148BD28-E7ED-4BDF-BDEA-0ED2A6A16AE0
// Assembly location: C:\TeklaStructures\20.0\Environments\Common\macros\drawings\WeldApp.exe

using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model;

namespace ArkLib.x_algoritms
{
  public static class x_New
  {
    public static Point Point(ContourPoint point)
    {
      return new Point(point.X, point.Y, point.Z);
    }

    public static Point Point(Point point)
    {
      return new Point(point.X, point.Y, point.Z);
    }

    public static Point Point(Tekla.Structures.Geometry3d.Vector vector)
    {
      return new Point(vector.X, vector.Y, vector.Z);
    }

    public static ContourPoint ContourPoint(Point p, Chamfer ch)
    {
      Chamfer C = new Chamfer();
      if (ch != null)
      {
        C.Type = ch.Type;
        C.X = ch.X;
        C.Y = ch.Y;
        C.DZ1 = ch.DZ1;
        C.DZ2 = ch.DZ2;
      }
      return new ContourPoint(x_New.Point(p), C);
    }

    public static ContourPoint ContourPoint(ContourPoint cp)
    {
      return x_New.ContourPoint(new Point(cp.X, cp.Y, cp.Z), cp.Chamfer);
    }

    public static Chamfer Chamfer()
    {
      Chamfer chamfer = new Chamfer();
      int num1 = 0;
      chamfer.Type = (Chamfer.ChamferTypeEnum) num1;
      double num2 = 0.0;
      chamfer.X = num2;
      double num3 = 0.0;
      chamfer.Y = num3;
      double num4 = 0.0;
      chamfer.DZ1 = num4;
      double num5 = 0.0;
      chamfer.DZ2 = num5;
      return chamfer;
    }

    public static GeometricPlane GeometricPlane(GeometricPlane plane)
    {
      return new GeometricPlane(new Point(plane.Origin.X, plane.Origin.Y, plane.Origin.Z), new Tekla.Structures.Geometry3d.Vector(plane.Normal.X, plane.Normal.Y, plane.Normal.Z));
    }

    public static GeometricPlane GeometricPlane(Point origin, Tekla.Structures.Geometry3d.Vector normal)
    {
      return new GeometricPlane(new Point(origin.X, origin.Y, origin.Z), new Tekla.Structures.Geometry3d.Vector(normal.X, normal.Y, normal.Z));
    }

    public static Tekla.Structures.Geometry3d.Vector Vector(Point point)
    {
      return new Tekla.Structures.Geometry3d.Vector(point.X, point.Y, point.Z);
    }

    public static Tekla.Structures.Geometry3d.Vector Vector(Tekla.Structures.Geometry3d.Vector vector)
    {
      return new Tekla.Structures.Geometry3d.Vector(vector.X, vector.Y, vector.Z);
    }

    public static Tekla.Structures.Geometry3d.Vector Vector(Point begin, Point end)
    {
      return new Tekla.Structures.Geometry3d.Vector(end.X - begin.X, end.Y - begin.Y, end.Z - begin.Z);
    }

    public static Tekla.Structures.Geometry3d.Vector Vector(Point begin, ContourPoint end)
    {
      return new Tekla.Structures.Geometry3d.Vector(end.X - begin.X, end.Y - begin.Y, end.Z - begin.Z);
    }

    public static Tekla.Structures.Geometry3d.Vector Vector(ContourPoint begin, ContourPoint end)
    {
      return new Tekla.Structures.Geometry3d.Vector(end.X - begin.X, end.Y - begin.Y, end.Z - begin.Z);
    }

    public static Line Line(Line line)
    {
      return new Line(new Point(line.Origin.X, line.Origin.Y, line.Origin.Z), new Tekla.Structures.Geometry3d.Vector(line.Direction.X, line.Direction.Y, line.Direction.Z));
    }

    public static Line Line(Point origin, Tekla.Structures.Geometry3d.Vector dir)
    {
      return new Line(new Point(origin.X, origin.Y, origin.Z), new Tekla.Structures.Geometry3d.Vector(dir.X, dir.Y, dir.Z));
    }

    public static Line Line(LineSegment segment)
    {
      return new Line(new Point(segment.Point1.X, segment.Point1.Y, segment.Point1.Z), new Point(segment.Point2.X, segment.Point2.Y, segment.Point2.Z));
    }

    public static Line Line(Point p1, Point p2)
    {
      return new Line(new Point(p1.X, p1.Y, p1.Z), new Point(p2.X, p2.Y, p2.Z));
    }

    public static Line Line(ContourPoint cp1, ContourPoint cp2)
    {
      return new Line(new Point(cp1.X, cp1.Y, cp1.Z), new Point(cp2.X, cp2.Y, cp2.Z));
    }

    public static LineSegment LineSegment(Point p1, Point p2)
    {
      return new LineSegment(x_New.Point(p1), x_New.Point(p2));
    }

    public static LineSegment LineSegment(LineSegment segment)
    {
      return new LineSegment(x_New.Point(segment.Point1), x_New.Point(segment.Point2));
    }

    public static LineSegment LineSegment(ContourPoint cp1, ContourPoint cp2)
    {
      return new LineSegment(new Point(cp1.X, cp1.Y, cp1.Z), new Point(cp2.X, cp2.Y, cp2.Z));
    }

    public static CoordinateSystem CoordinateSystem(Point origin, Tekla.Structures.Geometry3d.Vector axisX, Tekla.Structures.Geometry3d.Vector axisY)
    {
      CoordinateSystem coordinateSystem = new CoordinateSystem();
      Point point = x_New.Point(origin);
      coordinateSystem.Origin = point;
      Tekla.Structures.Geometry3d.Vector vector1 = x_New.Vector(axisX);
      coordinateSystem.AxisX = vector1;
      Tekla.Structures.Geometry3d.Vector vector2 = x_New.Vector(axisY);
      coordinateSystem.AxisY = vector2;
      return coordinateSystem;
    }
  }
}
