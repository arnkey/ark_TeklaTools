﻿namespace ark_TeklaDrawingTools
{
    partial class ark_TeklaDrawingTools
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_SinglePartTube = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Drawing = new System.Windows.Forms.TabPage();
            this.btn_OpenSaveClose = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cb_DrawingNmTlType = new System.Windows.Forms.ComboBox();
            this.btn_DrawingNmTlModify = new System.Windows.Forms.Button();
            this.cb_btn_DrawingNmTl = new System.Windows.Forms.ComboBox();
            this.txt_btn_DrawingNmTl = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_DrawingNumbering = new System.Windows.Forms.Button();
            this.txt_DrawingNumbering = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_FormatReverse = new System.Windows.Forms.Button();
            this.btn_FormatA_Minus = new System.Windows.Forms.Button();
            this.btn_FormatA_Plus = new System.Windows.Forms.Button();
            this.btn_Format_Plus = new System.Windows.Forms.Button();
            this.btn_Format_Minus = new System.Windows.Forms.Button();
            this.btn_FormatChange = new System.Windows.Forms.Button();
            this.View = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cmb_DrawingProperties = new System.Windows.Forms.ComboBox();
            this.btn_DrawingProperties = new System.Windows.Forms.Button();
            this.btn_VwName = new System.Windows.Forms.Button();
            this.txt_Vw_Name = new System.Windows.Forms.TextBox();
            this.txt_VwScale = new System.Windows.Forms.TextBox();
            this.btn_VwScale = new System.Windows.Forms.Button();
            this.btn_VwExc = new System.Windows.Forms.Button();
            this.txt_VwExc = new System.Windows.Forms.TextBox();
            this.Part = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rb_pres_accur = new System.Windows.Forms.RadioButton();
            this.rb_pres_contour = new System.Windows.Forms.RadioButton();
            this.btn_pres_Part = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rb_CenLine = new System.Windows.Forms.RadioButton();
            this.rb_RefLine = new System.Windows.Forms.RadioButton();
            this.btn_RefCenLineModify = new System.Windows.Forms.Button();
            this.Dimensions = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.rb_dim_absolute = new System.Windows.Forms.RadioButton();
            this.rb_dim_absolute_relative = new System.Windows.Forms.RadioButton();
            this.rb_dim_relative = new System.Windows.Forms.RadioButton();
            this.btn_Del_All_Dimensions = new System.Windows.Forms.Button();
            this.sub = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btn_SinglePartCurved = new System.Windows.Forms.Button();
            this.debug = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_DimensionBeetwenParts = new System.Windows.Forms.Button();
            this.btn_RadiusPolyBeam = new System.Windows.Forms.Button();
            this.toNameASMRUS = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.Drawing.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.View.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.Part.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Dimensions.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.sub.SuspendLayout();
            this.debug.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_SinglePartTube
            // 
            this.btn_SinglePartTube.Location = new System.Drawing.Point(14, 105);
            this.btn_SinglePartTube.Name = "btn_SinglePartTube";
            this.btn_SinglePartTube.Size = new System.Drawing.Size(160, 23);
            this.btn_SinglePartTube.TabIndex = 10;
            this.btn_SinglePartTube.Text = "Деталь трубы";
            this.btn_SinglePartTube.UseVisualStyleBackColor = true;
            this.btn_SinglePartTube.Click += new System.EventHandler(this.btn_SinglePartTube_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.Drawing);
            this.tabControl1.Controls.Add(this.View);
            this.tabControl1.Controls.Add(this.Part);
            this.tabControl1.Controls.Add(this.Dimensions);
            this.tabControl1.Controls.Add(this.sub);
            this.tabControl1.Controls.Add(this.debug);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(209, 425);
            this.tabControl1.TabIndex = 0;
            // 
            // Drawing
            // 
            this.Drawing.Controls.Add(this.toNameASMRUS);
            this.Drawing.Controls.Add(this.btn_OpenSaveClose);
            this.Drawing.Controls.Add(this.groupBox4);
            this.Drawing.Controls.Add(this.groupBox2);
            this.Drawing.Controls.Add(this.groupBox1);
            this.Drawing.Location = new System.Drawing.Point(23, 4);
            this.Drawing.Name = "Drawing";
            this.Drawing.Padding = new System.Windows.Forms.Padding(3);
            this.Drawing.Size = new System.Drawing.Size(182, 417);
            this.Drawing.TabIndex = 1;
            this.Drawing.Text = "Чертеж";
            this.Drawing.UseVisualStyleBackColor = true;
            // 
            // btn_OpenSaveClose
            // 
            this.btn_OpenSaveClose.Location = new System.Drawing.Point(6, 279);
            this.btn_OpenSaveClose.Name = "btn_OpenSaveClose";
            this.btn_OpenSaveClose.Size = new System.Drawing.Size(172, 23);
            this.btn_OpenSaveClose.TabIndex = 7;
            this.btn_OpenSaveClose.Text = "Открыть-сохранить-закрыть";
            this.btn_OpenSaveClose.UseVisualStyleBackColor = true;
            this.btn_OpenSaveClose.Click += new System.EventHandler(this.btn_OpenSaveClose_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cb_DrawingNmTlType);
            this.groupBox4.Controls.Add(this.btn_DrawingNmTlModify);
            this.groupBox4.Controls.Add(this.cb_btn_DrawingNmTl);
            this.groupBox4.Controls.Add(this.txt_btn_DrawingNmTl);
            this.groupBox4.Location = new System.Drawing.Point(6, 141);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(172, 132);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Имя\\Заголовки";
            // 
            // cb_DrawingNmTlType
            // 
            this.cb_DrawingNmTlType.AutoCompleteCustomSource.AddRange(new string[] {
            "БЧ",
            "На выдачу",
            "Ошибка в модели",
            "Клонировать",
            "Образмерить",
            "Проверить"});
            this.cb_DrawingNmTlType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_DrawingNmTlType.FormattingEnabled = true;
            this.cb_DrawingNmTlType.Items.AddRange(new object[] {
            "Имя",
            "Заголовок 1",
            "Заголовок 2",
            "Заголовок 3"});
            this.cb_DrawingNmTlType.Location = new System.Drawing.Point(8, 72);
            this.cb_DrawingNmTlType.Name = "cb_DrawingNmTlType";
            this.cb_DrawingNmTlType.Size = new System.Drawing.Size(158, 21);
            this.cb_DrawingNmTlType.TabIndex = 3;
            // 
            // btn_DrawingNmTlModify
            // 
            this.btn_DrawingNmTlModify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_DrawingNmTlModify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_DrawingNmTlModify.Location = new System.Drawing.Point(62, 99);
            this.btn_DrawingNmTlModify.Name = "btn_DrawingNmTlModify";
            this.btn_DrawingNmTlModify.Size = new System.Drawing.Size(50, 25);
            this.btn_DrawingNmTlModify.TabIndex = 2;
            this.btn_DrawingNmTlModify.Text = "Пуск";
            this.btn_DrawingNmTlModify.UseVisualStyleBackColor = true;
            this.btn_DrawingNmTlModify.Click += new System.EventHandler(this.btn_DrawingNmTlModify_Click);
            // 
            // cb_btn_DrawingNmTl
            // 
            this.cb_btn_DrawingNmTl.AutoCompleteCustomSource.AddRange(new string[] {
            "БЧ",
            "На выдачу",
            "Ошибка в модели",
            "Клонировать",
            "Образмерить",
            "Проверить"});
            this.cb_btn_DrawingNmTl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_btn_DrawingNmTl.FormattingEnabled = true;
            this.cb_btn_DrawingNmTl.Items.AddRange(new object[] {
            "-",
            "БЧ",
            "На выдачу",
            "Ошибка в модели",
            "Клонировать",
            "Образмерить",
            "Проверить"});
            this.cb_btn_DrawingNmTl.Location = new System.Drawing.Point(8, 45);
            this.cb_btn_DrawingNmTl.Name = "cb_btn_DrawingNmTl";
            this.cb_btn_DrawingNmTl.Size = new System.Drawing.Size(158, 21);
            this.cb_btn_DrawingNmTl.TabIndex = 1;
            // 
            // txt_btn_DrawingNmTl
            // 
            this.txt_btn_DrawingNmTl.Location = new System.Drawing.Point(8, 19);
            this.txt_btn_DrawingNmTl.Name = "txt_btn_DrawingNmTl";
            this.txt_btn_DrawingNmTl.Size = new System.Drawing.Size(158, 20);
            this.txt_btn_DrawingNmTl.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_DrawingNumbering);
            this.groupBox2.Controls.Add(this.txt_DrawingNumbering);
            this.groupBox2.Location = new System.Drawing.Point(6, 84);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 51);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Нумеровать";
            // 
            // btn_DrawingNumbering
            // 
            this.btn_DrawingNumbering.BackColor = System.Drawing.Color.Transparent;
            this.btn_DrawingNumbering.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_DrawingNumbering.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_DrawingNumbering.Location = new System.Drawing.Point(62, 17);
            this.btn_DrawingNumbering.Name = "btn_DrawingNumbering";
            this.btn_DrawingNumbering.Size = new System.Drawing.Size(50, 25);
            this.btn_DrawingNumbering.TabIndex = 3;
            this.btn_DrawingNumbering.Text = "Пуск";
            this.btn_DrawingNumbering.UseVisualStyleBackColor = false;
            this.btn_DrawingNumbering.Click += new System.EventHandler(this.btn_DrawingNumbering_Click_1);
            // 
            // txt_DrawingNumbering
            // 
            this.txt_DrawingNumbering.Location = new System.Drawing.Point(8, 20);
            this.txt_DrawingNumbering.Name = "txt_DrawingNumbering";
            this.txt_DrawingNumbering.Size = new System.Drawing.Size(50, 20);
            this.txt_DrawingNumbering.TabIndex = 2;
            this.txt_DrawingNumbering.Text = "1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_FormatReverse);
            this.groupBox1.Controls.Add(this.btn_FormatA_Minus);
            this.groupBox1.Controls.Add(this.btn_FormatA_Plus);
            this.groupBox1.Controls.Add(this.btn_Format_Plus);
            this.groupBox1.Controls.Add(this.btn_Format_Minus);
            this.groupBox1.Controls.Add(this.btn_FormatChange);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(172, 72);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Формат";
            // 
            // btn_FormatReverse
            // 
            this.btn_FormatReverse.Location = new System.Drawing.Point(34, 47);
            this.btn_FormatReverse.Name = "btn_FormatReverse";
            this.btn_FormatReverse.Size = new System.Drawing.Size(22, 22);
            this.btn_FormatReverse.TabIndex = 5;
            this.btn_FormatReverse.Text = "ↄ";
            this.btn_FormatReverse.UseVisualStyleBackColor = true;
            this.btn_FormatReverse.Click += new System.EventHandler(this.btn_FormatReverse_Click);
            // 
            // btn_FormatA_Minus
            // 
            this.btn_FormatA_Minus.Location = new System.Drawing.Point(6, 47);
            this.btn_FormatA_Minus.Name = "btn_FormatA_Minus";
            this.btn_FormatA_Minus.Size = new System.Drawing.Size(22, 22);
            this.btn_FormatA_Minus.TabIndex = 4;
            this.btn_FormatA_Minus.Text = "-";
            this.btn_FormatA_Minus.UseVisualStyleBackColor = true;
            this.btn_FormatA_Minus.Click += new System.EventHandler(this.btn_FormatA_Minus_Click);
            // 
            // btn_FormatA_Plus
            // 
            this.btn_FormatA_Plus.Location = new System.Drawing.Point(6, 19);
            this.btn_FormatA_Plus.Name = "btn_FormatA_Plus";
            this.btn_FormatA_Plus.Size = new System.Drawing.Size(22, 22);
            this.btn_FormatA_Plus.TabIndex = 3;
            this.btn_FormatA_Plus.Text = "+";
            this.btn_FormatA_Plus.UseVisualStyleBackColor = true;
            this.btn_FormatA_Plus.Click += new System.EventHandler(this.btn_FormatA_Plus_Click);
            // 
            // btn_Format_Plus
            // 
            this.btn_Format_Plus.Location = new System.Drawing.Point(90, 47);
            this.btn_Format_Plus.Name = "btn_Format_Plus";
            this.btn_Format_Plus.Size = new System.Drawing.Size(22, 22);
            this.btn_Format_Plus.TabIndex = 2;
            this.btn_Format_Plus.Text = ">";
            this.btn_Format_Plus.UseVisualStyleBackColor = true;
            this.btn_Format_Plus.Click += new System.EventHandler(this.btn_Format_Plus_Click);
            // 
            // btn_Format_Minus
            // 
            this.btn_Format_Minus.Location = new System.Drawing.Point(62, 47);
            this.btn_Format_Minus.Name = "btn_Format_Minus";
            this.btn_Format_Minus.Size = new System.Drawing.Size(22, 22);
            this.btn_Format_Minus.TabIndex = 1;
            this.btn_Format_Minus.Text = "<";
            this.btn_Format_Minus.UseVisualStyleBackColor = true;
            this.btn_Format_Minus.Click += new System.EventHandler(this.btn_Format_Minus_Click);
            // 
            // btn_FormatChange
            // 
            this.btn_FormatChange.Location = new System.Drawing.Point(34, 19);
            this.btn_FormatChange.Name = "btn_FormatChange";
            this.btn_FormatChange.Size = new System.Drawing.Size(78, 22);
            this.btn_FormatChange.TabIndex = 0;
            this.btn_FormatChange.Text = "button1";
            this.btn_FormatChange.UseVisualStyleBackColor = true;
            this.btn_FormatChange.Click += new System.EventHandler(this.btn_FormatChange_Click);
            // 
            // View
            // 
            this.View.Controls.Add(this.groupBox5);
            this.View.Controls.Add(this.btn_VwName);
            this.View.Controls.Add(this.txt_Vw_Name);
            this.View.Controls.Add(this.txt_VwScale);
            this.View.Controls.Add(this.btn_VwScale);
            this.View.Controls.Add(this.btn_VwExc);
            this.View.Controls.Add(this.txt_VwExc);
            this.View.Location = new System.Drawing.Point(23, 4);
            this.View.Name = "View";
            this.View.Padding = new System.Windows.Forms.Padding(3);
            this.View.Size = new System.Drawing.Size(182, 417);
            this.View.TabIndex = 0;
            this.View.Text = "Вид";
            this.View.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cmb_DrawingProperties);
            this.groupBox5.Controls.Add(this.btn_DrawingProperties);
            this.groupBox5.Location = new System.Drawing.Point(6, 89);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(172, 80);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Свойство вида";
            // 
            // cmb_DrawingProperties
            // 
            this.cmb_DrawingProperties.AutoCompleteCustomSource.AddRange(new string[] {
            "БЧ",
            "На выдачу",
            "Ошибка в модели",
            "Клонировать",
            "Образмерить",
            "Проверить"});
            this.cmb_DrawingProperties.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_DrawingProperties.FormattingEnabled = true;
            this.cmb_DrawingProperties.Location = new System.Drawing.Point(8, 19);
            this.cmb_DrawingProperties.Name = "cmb_DrawingProperties";
            this.cmb_DrawingProperties.Size = new System.Drawing.Size(158, 21);
            this.cmb_DrawingProperties.TabIndex = 3;
            this.cmb_DrawingProperties.SelectedIndexChanged += new System.EventHandler(this.cmb_DrawingProperties_SelectedIndexChanged);
            // 
            // btn_DrawingProperties
            // 
            this.btn_DrawingProperties.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_DrawingProperties.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_DrawingProperties.Location = new System.Drawing.Point(62, 46);
            this.btn_DrawingProperties.Name = "btn_DrawingProperties";
            this.btn_DrawingProperties.Size = new System.Drawing.Size(50, 25);
            this.btn_DrawingProperties.TabIndex = 2;
            this.btn_DrawingProperties.Text = "Пуск";
            this.btn_DrawingProperties.UseVisualStyleBackColor = true;
            this.btn_DrawingProperties.Click += new System.EventHandler(this.btn_DrawingProperties_Click);
            // 
            // btn_VwName
            // 
            this.btn_VwName.Location = new System.Drawing.Point(78, 60);
            this.btn_VwName.Name = "btn_VwName";
            this.btn_VwName.Size = new System.Drawing.Size(72, 23);
            this.btn_VwName.TabIndex = 5;
            this.btn_VwName.Text = "Имя вида";
            this.btn_VwName.UseVisualStyleBackColor = true;
            this.btn_VwName.Click += new System.EventHandler(this.btn_VwName_Click);
            // 
            // txt_Vw_Name
            // 
            this.txt_Vw_Name.Location = new System.Drawing.Point(8, 62);
            this.txt_Vw_Name.Name = "txt_Vw_Name";
            this.txt_Vw_Name.Size = new System.Drawing.Size(64, 20);
            this.txt_Vw_Name.TabIndex = 4;
            this.txt_Vw_Name.Text = "Имя вида";
            // 
            // txt_VwScale
            // 
            this.txt_VwScale.Location = new System.Drawing.Point(8, 35);
            this.txt_VwScale.Name = "txt_VwScale";
            this.txt_VwScale.Size = new System.Drawing.Size(64, 20);
            this.txt_VwScale.TabIndex = 3;
            this.txt_VwScale.Text = "5";
            // 
            // btn_VwScale
            // 
            this.btn_VwScale.Location = new System.Drawing.Point(78, 33);
            this.btn_VwScale.Name = "btn_VwScale";
            this.btn_VwScale.Size = new System.Drawing.Size(72, 23);
            this.btn_VwScale.TabIndex = 2;
            this.btn_VwScale.Text = "Масштаб";
            this.btn_VwScale.UseVisualStyleBackColor = true;
            this.btn_VwScale.Click += new System.EventHandler(this.btn_VwScale_Click);
            // 
            // btn_VwExc
            // 
            this.btn_VwExc.Location = new System.Drawing.Point(78, 4);
            this.btn_VwExc.Name = "btn_VwExc";
            this.btn_VwExc.Size = new System.Drawing.Size(72, 23);
            this.btn_VwExc.TabIndex = 1;
            this.btn_VwExc.Text = "Усечение";
            this.btn_VwExc.UseVisualStyleBackColor = true;
            this.btn_VwExc.Click += new System.EventHandler(this.btn_VwExc_Click);
            // 
            // txt_VwExc
            // 
            this.txt_VwExc.Location = new System.Drawing.Point(8, 6);
            this.txt_VwExc.Name = "txt_VwExc";
            this.txt_VwExc.Size = new System.Drawing.Size(64, 20);
            this.txt_VwExc.TabIndex = 0;
            this.txt_VwExc.Text = "-1";
            // 
            // Part
            // 
            this.Part.Controls.Add(this.groupBox7);
            this.Part.Controls.Add(this.groupBox3);
            this.Part.Location = new System.Drawing.Point(23, 4);
            this.Part.Name = "Part";
            this.Part.Size = new System.Drawing.Size(182, 417);
            this.Part.TabIndex = 3;
            this.Part.Text = "Деталь";
            this.Part.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rb_pres_accur);
            this.groupBox7.Controls.Add(this.rb_pres_contour);
            this.groupBox7.Controls.Add(this.btn_pres_Part);
            this.groupBox7.Location = new System.Drawing.Point(3, 87);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(172, 68);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Представление";
            // 
            // rb_pres_accur
            // 
            this.rb_pres_accur.AutoSize = true;
            this.rb_pres_accur.Location = new System.Drawing.Point(6, 42);
            this.rb_pres_accur.Name = "rb_pres_accur";
            this.rb_pres_accur.Size = new System.Drawing.Size(55, 17);
            this.rb_pres_accur.TabIndex = 3;
            this.rb_pres_accur.Text = "Точно";
            this.rb_pres_accur.UseVisualStyleBackColor = true;
            // 
            // rb_pres_contour
            // 
            this.rb_pres_contour.AutoSize = true;
            this.rb_pres_contour.Checked = true;
            this.rb_pres_contour.Location = new System.Drawing.Point(6, 19);
            this.rb_pres_contour.Name = "rb_pres_contour";
            this.rb_pres_contour.Size = new System.Drawing.Size(60, 17);
            this.rb_pres_contour.TabIndex = 2;
            this.rb_pres_contour.TabStop = true;
            this.rb_pres_contour.Text = "Контур";
            this.rb_pres_contour.UseVisualStyleBackColor = true;
            // 
            // btn_pres_Part
            // 
            this.btn_pres_Part.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_pres_Part.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_pres_Part.Location = new System.Drawing.Point(141, 15);
            this.btn_pres_Part.Name = "btn_pres_Part";
            this.btn_pres_Part.Size = new System.Drawing.Size(25, 25);
            this.btn_pres_Part.TabIndex = 1;
            this.btn_pres_Part.Text = "П";
            this.btn_pres_Part.UseVisualStyleBackColor = true;
            this.btn_pres_Part.Click += new System.EventHandler(this.btn_pres_Part_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rb_CenLine);
            this.groupBox3.Controls.Add(this.rb_RefLine);
            this.groupBox3.Controls.Add(this.btn_RefCenLineModify);
            this.groupBox3.Location = new System.Drawing.Point(3, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(172, 73);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Опорная\\Центральная линия";
            // 
            // rb_CenLine
            // 
            this.rb_CenLine.AutoSize = true;
            this.rb_CenLine.Location = new System.Drawing.Point(6, 42);
            this.rb_CenLine.Name = "rb_CenLine";
            this.rb_CenLine.Size = new System.Drawing.Size(125, 17);
            this.rb_CenLine.TabIndex = 3;
            this.rb_CenLine.TabStop = true;
            this.rb_CenLine.Text = "Центральная линия";
            this.rb_CenLine.UseVisualStyleBackColor = true;
            this.rb_CenLine.CheckedChanged += new System.EventHandler(this.rb_CenLine_CheckedChanged);
            // 
            // rb_RefLine
            // 
            this.rb_RefLine.AutoSize = true;
            this.rb_RefLine.Location = new System.Drawing.Point(6, 19);
            this.rb_RefLine.Name = "rb_RefLine";
            this.rb_RefLine.Size = new System.Drawing.Size(102, 17);
            this.rb_RefLine.TabIndex = 2;
            this.rb_RefLine.TabStop = true;
            this.rb_RefLine.Text = "Опорная линия";
            this.rb_RefLine.UseVisualStyleBackColor = true;
            this.rb_RefLine.CheckedChanged += new System.EventHandler(this.rb_RefLine_CheckedChanged);
            // 
            // btn_RefCenLineModify
            // 
            this.btn_RefCenLineModify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_RefCenLineModify.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_RefCenLineModify.Location = new System.Drawing.Point(141, 38);
            this.btn_RefCenLineModify.Name = "btn_RefCenLineModify";
            this.btn_RefCenLineModify.Size = new System.Drawing.Size(25, 25);
            this.btn_RefCenLineModify.TabIndex = 1;
            this.btn_RefCenLineModify.Text = "Пуск";
            this.btn_RefCenLineModify.UseVisualStyleBackColor = true;
            this.btn_RefCenLineModify.Click += new System.EventHandler(this.btn_RefCenLineModify_Click);
            // 
            // Dimensions
            // 
            this.Dimensions.Controls.Add(this.groupBox6);
            this.Dimensions.Controls.Add(this.btn_Del_All_Dimensions);
            this.Dimensions.Location = new System.Drawing.Point(23, 4);
            this.Dimensions.Name = "Dimensions";
            this.Dimensions.Size = new System.Drawing.Size(182, 417);
            this.Dimensions.TabIndex = 2;
            this.Dimensions.Text = "Размер";
            this.Dimensions.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button5);
            this.groupBox6.Controls.Add(this.rb_dim_absolute);
            this.groupBox6.Controls.Add(this.rb_dim_absolute_relative);
            this.groupBox6.Controls.Add(this.rb_dim_relative);
            this.groupBox6.Location = new System.Drawing.Point(13, 37);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(151, 127);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Тип";
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(41, 88);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(54, 26);
            this.button5.TabIndex = 3;
            this.button5.Text = "Пуск";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // rb_dim_absolute
            // 
            this.rb_dim_absolute.AutoSize = true;
            this.rb_dim_absolute.Location = new System.Drawing.Point(6, 42);
            this.rb_dim_absolute.Name = "rb_dim_absolute";
            this.rb_dim_absolute.Size = new System.Drawing.Size(89, 17);
            this.rb_dim_absolute.TabIndex = 2;
            this.rb_dim_absolute.Text = "Абсолютный";
            this.rb_dim_absolute.UseVisualStyleBackColor = true;
            // 
            // rb_dim_absolute_relative
            // 
            this.rb_dim_absolute_relative.AutoSize = true;
            this.rb_dim_absolute_relative.Location = new System.Drawing.Point(6, 65);
            this.rb_dim_absolute_relative.Name = "rb_dim_absolute_relative";
            this.rb_dim_absolute_relative.Size = new System.Drawing.Size(115, 17);
            this.rb_dim_absolute_relative.TabIndex = 1;
            this.rb_dim_absolute_relative.Text = "Относит. + абсол.";
            this.rb_dim_absolute_relative.UseVisualStyleBackColor = true;
            // 
            // rb_dim_relative
            // 
            this.rb_dim_relative.AutoSize = true;
            this.rb_dim_relative.Checked = true;
            this.rb_dim_relative.Location = new System.Drawing.Point(6, 19);
            this.rb_dim_relative.Name = "rb_dim_relative";
            this.rb_dim_relative.Size = new System.Drawing.Size(105, 17);
            this.rb_dim_relative.TabIndex = 0;
            this.rb_dim_relative.TabStop = true;
            this.rb_dim_relative.Text = "Относительный";
            this.rb_dim_relative.UseVisualStyleBackColor = true;
            // 
            // btn_Del_All_Dimensions
            // 
            this.btn_Del_All_Dimensions.Location = new System.Drawing.Point(13, 8);
            this.btn_Del_All_Dimensions.Name = "btn_Del_All_Dimensions";
            this.btn_Del_All_Dimensions.Size = new System.Drawing.Size(151, 23);
            this.btn_Del_All_Dimensions.TabIndex = 0;
            this.btn_Del_All_Dimensions.Text = "Убрать все размеры";
            this.btn_Del_All_Dimensions.UseVisualStyleBackColor = true;
            this.btn_Del_All_Dimensions.Click += new System.EventHandler(this.btn_Del_All_Dimensions_Click);
            // 
            // sub
            // 
            this.sub.Controls.Add(this.button7);
            this.sub.Controls.Add(this.button6);
            this.sub.Controls.Add(this.btn_SinglePartCurved);
            this.sub.Controls.Add(this.btn_SinglePartTube);
            this.sub.Location = new System.Drawing.Point(23, 4);
            this.sub.Name = "sub";
            this.sub.Size = new System.Drawing.Size(182, 417);
            this.sub.TabIndex = 5;
            this.sub.Text = "Подпрограммы";
            this.sub.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(14, 42);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(160, 23);
            this.button7.TabIndex = 8;
            this.button7.Text = "Метки сварных швов";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(14, 8);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(160, 28);
            this.button6.TabIndex = 7;
            this.button6.Text = "Образмерка";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btn_SinglePartCurved
            // 
            this.btn_SinglePartCurved.Location = new System.Drawing.Point(14, 76);
            this.btn_SinglePartCurved.Name = "btn_SinglePartCurved";
            this.btn_SinglePartCurved.Size = new System.Drawing.Size(160, 23);
            this.btn_SinglePartCurved.TabIndex = 9;
            this.btn_SinglePartCurved.Text = "Гнутая деталь";
            this.btn_SinglePartCurved.UseVisualStyleBackColor = true;
            this.btn_SinglePartCurved.Click += new System.EventHandler(this.btn_SinglePartCurved_Click);
            // 
            // debug
            // 
            this.debug.Controls.Add(this.button10);
            this.debug.Controls.Add(this.button8);
            this.debug.Controls.Add(this.button9);
            this.debug.Controls.Add(this.button4);
            this.debug.Controls.Add(this.button3);
            this.debug.Controls.Add(this.button2);
            this.debug.Controls.Add(this.button1);
            this.debug.Controls.Add(this.btn_DimensionBeetwenParts);
            this.debug.Controls.Add(this.btn_RadiusPolyBeam);
            this.debug.Location = new System.Drawing.Point(23, 4);
            this.debug.Name = "debug";
            this.debug.Size = new System.Drawing.Size(182, 417);
            this.debug.TabIndex = 4;
            this.debug.Text = "ArnKey\'s";
            this.debug.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(24, 268);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(64, 53);
            this.button8.TabIndex = 9;
            this.button8.Text = "Профиль в имя";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(24, 209);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(64, 53);
            this.button9.TabIndex = 8;
            this.button9.Text = "Имя детали\\марки - в имя";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(110, 355);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(64, 51);
            this.button4.TabIndex = 5;
            this.button4.Text = "Steps";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(110, 298);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(64, 51);
            this.button3.TabIndex = 4;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(110, 239);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 53);
            this.button2.TabIndex = 3;
            this.button2.Text = "bolt_min_max";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 53);
            this.button1.TabIndex = 2;
            this.button1.Text = "btn_DimensionBeetwenParts_Y";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_DimensionBeetwenParts
            // 
            this.btn_DimensionBeetwenParts.Location = new System.Drawing.Point(24, 91);
            this.btn_DimensionBeetwenParts.Name = "btn_DimensionBeetwenParts";
            this.btn_DimensionBeetwenParts.Size = new System.Drawing.Size(135, 53);
            this.btn_DimensionBeetwenParts.TabIndex = 1;
            this.btn_DimensionBeetwenParts.Text = "btn_DimensionBeetwenParts_X";
            this.btn_DimensionBeetwenParts.UseVisualStyleBackColor = true;
            this.btn_DimensionBeetwenParts.Click += new System.EventHandler(this.btn_DimensionBeetwenParts_Click);
            // 
            // btn_RadiusPolyBeam
            // 
            this.btn_RadiusPolyBeam.Location = new System.Drawing.Point(24, 23);
            this.btn_RadiusPolyBeam.Name = "btn_RadiusPolyBeam";
            this.btn_RadiusPolyBeam.Size = new System.Drawing.Size(135, 53);
            this.btn_RadiusPolyBeam.TabIndex = 0;
            this.btn_RadiusPolyBeam.Text = "!Embeded MARK!";
            this.btn_RadiusPolyBeam.UseVisualStyleBackColor = true;
            this.btn_RadiusPolyBeam.Click += new System.EventHandler(this.btn_RadiusPolyBeam_Click);
            // 
            // toNameASMRUS
            // 
            this.toNameASMRUS.Location = new System.Drawing.Point(6, 308);
            this.toNameASMRUS.Name = "toNameASMRUS";
            this.toNameASMRUS.Size = new System.Drawing.Size(172, 23);
            this.toNameASMRUS.TabIndex = 8;
            this.toNameASMRUS.Text = "Имена сборок Russia";
            this.toNameASMRUS.UseVisualStyleBackColor = true;
            this.toNameASMRUS.Click += new System.EventHandler(this.toNameASMRUS_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(24, 355);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(64, 51);
            this.button10.TabIndex = 10;
            this.button10.Text = "pdf";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // ark_TeklaDrawingTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(209, 425);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ark_TeklaDrawingTools";
            this.Text = "ark_TeklaDrawingTools";
            this.Load += new System.EventHandler(this.ark_TeklaDrawingTools_Load);
            this.tabControl1.ResumeLayout(false);
            this.Drawing.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.View.ResumeLayout(false);
            this.View.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.Part.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Dimensions.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.sub.ResumeLayout(false);
            this.debug.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage View;
        private System.Windows.Forms.TabPage Drawing;
        private System.Windows.Forms.Button btn_VwExc;
        private System.Windows.Forms.TextBox txt_VwExc;
        private System.Windows.Forms.TextBox txt_VwScale;
        private System.Windows.Forms.Button btn_VwScale;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_FormatA_Minus;
        private System.Windows.Forms.Button btn_FormatA_Plus;
        private System.Windows.Forms.Button btn_Format_Plus;
        private System.Windows.Forms.Button btn_Format_Minus;
        private System.Windows.Forms.Button btn_FormatChange;
        private System.Windows.Forms.Button btn_FormatReverse;
        private System.Windows.Forms.TabPage Dimensions;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_DrawingNumbering;
        private System.Windows.Forms.TextBox txt_DrawingNumbering;
        private System.Windows.Forms.Button btn_VwName;
        private System.Windows.Forms.TextBox txt_Vw_Name;
        private System.Windows.Forms.TabPage Part;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rb_CenLine;
        private System.Windows.Forms.RadioButton rb_RefLine;
        private System.Windows.Forms.Button btn_RefCenLineModify;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cb_DrawingNmTlType;
        private System.Windows.Forms.Button btn_DrawingNmTlModify;
        private System.Windows.Forms.ComboBox cb_btn_DrawingNmTl;
        private System.Windows.Forms.TextBox txt_btn_DrawingNmTl;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cmb_DrawingProperties;
        private System.Windows.Forms.Button btn_DrawingProperties;
        private System.Windows.Forms.Button btn_Del_All_Dimensions;
        private System.Windows.Forms.TabPage debug;
        private System.Windows.Forms.Button btn_RadiusPolyBeam;
        private System.Windows.Forms.Button btn_DimensionBeetwenParts;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_OpenSaveClose;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RadioButton rb_dim_absolute;
        private System.Windows.Forms.RadioButton rb_dim_absolute_relative;
        private System.Windows.Forms.RadioButton rb_dim_relative;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton rb_pres_accur;
        private System.Windows.Forms.RadioButton rb_pres_contour;
        private System.Windows.Forms.Button btn_pres_Part;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TabPage sub;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button btn_SinglePartCurved;
		private System.Windows.Forms.Button btn_SinglePartTube;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button toNameASMRUS;
        private System.Windows.Forms.Button button10;
    }
}

