﻿using System;
using Tekla.Structures.Drawing;
using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;
using tsd = Tekla.Structures.Drawing;
using System.Collections.Generic;

using swf = System.Windows.Forms;
using System.Collections;

namespace ark_TeklaDrawingTools
{
	public class SinglePart_Curved
	{
		public SinglePart_Curved()
		{
			tsg.Vector dirX = new tsg.Vector(1, 0, 0);
			tsg.Vector dirY = new tsg.Vector(0, 1, 0);
			tsg.Vector dirXY = new tsg.Vector(1, 1, 0);

			DrawingHandler dh = new DrawingHandler();
			if (dh.GetConnectionStatus() == true)
			{
				Drawing draw = dh.GetActiveDrawing();
				tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
				ViewBase vb = null;

				while (doe.MoveNext())
				{
					if (doe.Current is tsd.ViewBase)
					{
						vb = doe.Current as tsd.ViewBase;
						break;
					}
				}
				if (vb == null) return;

				// set's up 

				tsd.View v = vb as tsd.View;
				v.Attributes.LoadAttributes("Деталь_Развернуто");
				v.Modify();
				v.Attributes.UndeformedView = true;
				v.Attributes.UnfoldedView = true;
				v.Attributes.Shortening.CutParts = false;
				v.Attributes.Shortening.CutSkewParts = false;
				v.Modify();
                v.Select();
				tsg.Matrix m = tsg.MatrixFactory.ToCoordinateSystem(v.DisplayCoordinateSystem);

				tsd.PointList plX = new PointList(); // габарит
				tsd.PointList plY = new PointList();
				tsd.PointList plM = new PointList(); // линия гиба
				PointList plBX = new PointList(); //отверстия
				PointList plBY = new PointList(); //отверстия
				PointList plBlX = new PointList(); // вырезы
				PointList plBlY = new PointList(); // вырезы

				tsm.Part part =
					new tsm.Model().SelectModelObject((draw as tsd.SinglePartDrawing).PartIdentifier) as tsm.Part;

				double s = 0; part.GetReportProperty("PROFILE.WIDTH", ref s);
				double w = 0; part.GetReportProperty("PROFILE.HEIGHT", ref w);

				tsm.PolyBeam pb = part as tsm.PolyBeam;

				double[] length = new double[pb.Contour.ContourPoints.Count];
				double[] lengthRadius = new double[pb.Contour.ContourPoints.Count];
				double[] lengthMRadius = new double[pb.Contour.ContourPoints.Count];


				// сбор радиусов и углов


				tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();
				tsg.Matrix mp = tsg.MatrixFactory.ToCoordinateSystem(part.GetCoordinateSystem());

				tsm.TransformationPlane tpCurrent = wph.GetCurrentTransformationPlane();
				tsm.TransformationPlane tp = new tsm.TransformationPlane(part.GetCoordinateSystem());
				wph.SetCurrentTransformationPlane(tp);

				double dist = 0;

				tsg.Point rp0 = tp.TransformationMatrixToLocal.Transform(pb.Contour.ContourPoints[0] as tsm.ContourPoint);
				tsg.Point cp0 = part.GetCenterLine(false)[0] as tsg.Point;

				//Console.WriteLine(rp0.ToString() + " " + cp0.ToString());

				dist = rp0.Z - cp0.Z;

				//Console.WriteLine("Dist = " + dist);



				int i = 1;

				//Console.WriteLine("=======");

				while (true) // длина дуги
				{

					wph.SetCurrentTransformationPlane(tp);

					tsm.ContourPoint cpC = pb.Contour.ContourPoints[i] as tsm.ContourPoint;
					tsm.ContourPoint cpN = pb.Contour.ContourPoints[i + 1] as tsm.ContourPoint;
					tsm.ContourPoint cpP = pb.Contour.ContourPoints[i - 1] as tsm.ContourPoint;

					tsg.Point pcpC = cp(cpC);// m.Transform(cp(cpC));
					tsg.Point pcpN = cp(cpN); //m.Transform(cp(cpN));
					tsg.Point pcpP = cp(cpP); //m.Transform(cp(cpP));

					//поиск ближайшей точки


					double dangle = 0;



					tsg.Point c = tp.TransformationMatrixToLocal.Transform(cpC); // mp.Transform(pcpN); 
					tsg.Point a = tp.TransformationMatrixToLocal.Transform(cpN);//mp.Transform(mPoint);
					tsg.Point b = tp.TransformationMatrixToLocal.Transform(cpP);//mp.Transform(mPoint);

					dangle = -1 * ((c.X - a.X) * (b.Z - a.Z) - (c.Z - a.Z) * (b.X - a.X));

					double direct = dist;

					wph.SetCurrentTransformationPlane(tpCurrent);

					//Console.WriteLine("Point : " + i);
					//Console.WriteLine("direct: " + direct);
					//Console.WriteLine("-dangle: " + dangle);

					////Console.WriteLine("-dist: " + dist);
					if (cpC.Chamfer.Type == tsm.Chamfer.ChamferTypeEnum.CHAMFER_ROUNDING)
					{
						if (dangle > 0) direct = dist;
						if (dangle < 0) direct = -1 * dist;

						double r = cpC.Chamfer.X == 0 ? cpC.Chamfer.Y : cpC.Chamfer.X;
						//Console.WriteLine("R in point ( model ) : " + r);
						double angle = (newV(pcpC, pcpP).GetAngleBetween(newV(pcpC, pcpN)) * 180 / Math.PI);
						//Console.WriteLine("Angle: " + angle);
						double dR = getR(r, direct);
						//Console.WriteLine("Cen. radius: " + dR);
						double r0 = getArlLength(dR, s, angle);
						//Console.WriteLine("Arc length: " + r0);
						r = Math.Abs(getH(dR, s, direct) * Math.Sin((90 - angle * 0.5) * Math.PI / 180) / Math.Sin(angle * 0.5 * Math.PI / 180));
						//Console.WriteLine("Projection R on ref line: " + r);
						lengthMRadius[i] = (r * 2);
						lengthRadius[i] = (r0);
					}
					else if (cpC.Chamfer.Type == tsm.Chamfer.ChamferTypeEnum.CHAMFER_ARC_POINT)
					{

						//Console.WriteLine("= ARC:");
						Tuple<double, tsg.Point> iarc = arc(b, c, a);
						double angle =
							Math.Abs(newV(iarc.Item2, b).GetAngleBetween(newV(iarc.Item2, a)) * 180 / Math.PI);
						//Console.WriteLine("angle: " + angle);

						double dR = getH(iarc.Item1, s, direct);
						double r0 = Math.PI * dR * angle / 180;
						//Console.WriteLine(" = RADIUS: " + dR.ToString());
						//Console.WriteLine(" = arc length" + r0.ToString());
						lengthMRadius[i] = (getL(pcpN, pcpC) + getL(pcpC, pcpP));
						lengthRadius[i] = (r0);

					}
					////Console.WriteLine(cpC.Chamfer.Type.ToString()) ;

					//Console.WriteLine("=======");
					i++;
					if (i >= pb.Contour.ContourPoints.Count - 1) break;
				}

				for (int f = 1; f < pb.Contour.ContourPoints.Count; f++)
				{
					tsm.ContourPoint cpC = pb.Contour.ContourPoints[f] as tsm.ContourPoint;
					tsm.ContourPoint cpN = pb.Contour.ContourPoints[f - 1] as tsm.ContourPoint;
					tsg.Point pcpC = cp(cpC);// m.Transform(cp(cpC));
					tsg.Point pcpN = cp(cpN); //m.Transform(cp(cpN));

					length[f] = getL(pcpN, pcpC);
					plM.Add(mp.Transform(cpN));
				}

				double flength = FullLength(length, lengthRadius, lengthMRadius);

				//Console.WriteLine("full length:" + flength);

				tsg.Point StartPoint = new tsg.Point(0, 0, 0);
				tsg.Point EndPoint = (new tsg.Point(flength, 0, 0));

				plX.Add(StartPoint); plX.Add(EndPoint);
				plM.Add(StartPoint); plM.Add(EndPoint);


				tsd.StraightDimensionSetHandler sds = new tsd.StraightDimensionSetHandler();


				tsd.Line Cline = new Line(vb, StartPoint, EndPoint);
				Cline.Attributes.Line.Type = LineTypes.DashDoubleDot;
				Cline.Attributes.Line.Color = DrawingColors.Red;
				Cline.Insert();

				plY.Add(new tsg.Point(0, w * 0.5, 0)); plY.Add(new tsg.Point(0, -1 * w * 0.5, 0)); plY.Add(new tsg.Point(0, 0, 0));

				plBX.Add(StartPoint); plBX.Add(EndPoint);

				plBY.Add(new tsg.Point(0, 0, 0)); plBlY.Add(new tsg.Point(0, 0, 0));

				plBlX.Add(StartPoint); plBlX.Add(EndPoint);


				ArrayList pbpoints = new ArrayList();
				foreach (tsm.ContourPoint cpf in pb.Contour.ContourPoints)
				{
					pbpoints.Add(
						new tsg.Point(tp.TransformationMatrixToLocal.Transform(new tsg.Point(cp(cpf))))
					);
				}

				//Console.WriteLine((pbpoints.Count));

				foreach (var b in part.GetBolts())
				{
					PointList ppgn = new PointList();

					tsm.BoltGroup bg = b as tsm.BoltGroup;

					////Console.WriteLine(bg.Identifier.ID.ToString());
					foreach (tsg.Point p in bg.BoltPositions)
					{

						tsg.Point p0 = new tsg.Point(tp.TransformationMatrixToLocal.Transform(p));

						int k = GetMinPoint(pbpoints, p0).Item1;
						double d = GetMinPoint(pbpoints, p0).Item2;

						////Console.WriteLine(" k=" + k.ToString() + "; d=" + d.ToString());

						double temp = 0;
						for (int z = 0; z <= k; z++)
							temp += (length[z] + lengthRadius[z] - lengthMRadius[z]);

						tsg.Point pbo = new tsg.Point((temp + d), (p0).Y, (p0).Z);
						plBX.Add(pbo);
						plBY.Add(pbo);
						ppgn.Add(pbo);
					}

					//tsd.Polygon pgn = new Polygon(vb, ppgn); pgn.Insert();
				}

				foreach (var bl in part.GetBooleans())
				{
					if (bl is tsm.BooleanPart)
					{
						tsm.BooleanPart blp = bl as tsm.BooleanPart;

						if (blp.OperativePart is tsm.ContourPlate)
						{

							tsm.ContourPlate opart = blp.OperativePart as tsm.ContourPlate;

							PointList ppgn = new PointList();

							foreach (tsm.ContourPoint cp00 in opart.Contour.ContourPoints)
							{
								tsg.Point p0 = new tsg.Point(tp.TransformationMatrixToLocal.Transform(cp(cp00)));

								int k = GetMinPoint(pbpoints, p0).Item1;
								double d = GetMinPoint(pbpoints, p0).Item2;
								////Console.WriteLine("k=" + k);

								double temp = 0;
								for (int z = 0; z <= k; z++)
									temp += (length[z] + lengthRadius[z] - lengthMRadius[z]);

								tsg.Point pbo = new tsg.Point((temp + d), p0.Y, p0.Z);
								plBlX.Add(pbo);
								plBlY.Add(pbo);

								ppgn.Add(pbo);

								if (cp00.Chamfer.Type != tsm.Chamfer.ChamferTypeEnum.CHAMFER_NONE &&
									(cp00.Chamfer.X != 0 || cp00.Chamfer.Y != 0))
								{
									string text = "R" + (cp00.Chamfer.X == 0 ? cp00.Chamfer.Y : cp00.Chamfer.X).ToString();
									InsertText(vb, text, pbo);
								}

							}

							tsd.Polygon pgn = new Polygon(vb, ppgn); pgn.Insert();
							//pgn.s
						}
					}

				}
				//sds.CreateDimensionSet(vb, plM, dirY, w * 0.5 + 10 * v.Attributes.Scale, attr("По линии гиба")); // линии гиба
				sds.CreateDimensionSet(vb, plX, dirY, w * 0.5 + 20 * v.Attributes.Scale); // габарит
				sds.CreateDimensionSet(vb, plY, dirX, -1 * (20 * v.Attributes.Scale)); // габарит

				if (plBX.Count > 2) sds.CreateDimensionSet(vb, plBX, dirY, -1 * w * 0.5 - 10 * v.Attributes.Scale, attr("По отверстиям")); // отверстия
				if (plBY.Count > 1) sds.CreateDimensionSet(vb, plBY, dirX, -1 * (10 * v.Attributes.Scale)); // отверстия

				if (plBlX.Count > 2) sds.CreateDimensionSet(vb, plBlX, dirY, -1 * w * 0.5 - 20 * v.Attributes.Scale, attr("По вырезам")); // вырезы
				if (plBlY.Count > 1) sds.CreateDimensionSet(vb, plBlY, dirX, -1 * (10 * v.Attributes.Scale)); // вырезы

				draw.Modify();
				draw.CommitChanges();

				wph.SetCurrentTransformationPlane(tpCurrent);
			}
			else {
				//Console.WriteLine("Connection false");
			}
			//Console.ReadKey();
		}

		private static void InsertText(ViewBase vb, string text, tsg.Point p)
		{
			tsd.Text t = new Text(vb, p, text);
			t.Attributes.Font.Name = "GOST 2.304 type A";
			t.Attributes.Font.Height = 3.5;
			t.Attributes.ArrowHead.ArrowPosition = ArrowheadPositions.Start;
			t.Attributes.ArrowHead.Head = ArrowheadTypes.FilledArrow;
			t.Attributes.PlacingAttributes.PlacingDistance.MinimalDistance = 10;
			t.Attributes.Angle = 0;
			t.Attributes.PreferredPlacing = PreferredTextPlacingTypes.LeaderLinePlacingType();
			t.Attributes.Alignment = TextAlignment.Left;
			t.Attributes.Frame.Type = FrameTypes.Line;
			t.Insert();
		}

		private static tsd.StraightDimensionSet.StraightDimensionSetAttributes attr(string text)
		{
			tsd.StraightDimensionSet.StraightDimensionSetAttributes attG = new StraightDimensionSet.StraightDimensionSetAttributes();
			TextElement te = new TextElement(text);
			te.Font.Name = "GOST 2.304 type A";
			te.Font.Height = 3.5;
			attG.RightUpperTag.Add(te);

			return attG;
		}

		private static Tuple<double, tsg.Point> arc(tsg.Point p1, tsg.Point p2, tsg.Point p3)
		{

			tsg.Point p = new tsg.Point();
			double radius = 0;

			double ydelta_a = p2.Z - p1.Z;
			double xdelta_a = p2.X - p1.X;
			double ydelta_b = p3.Z - p2.Z;
			double xdelta_b = p3.X - p2.X;

			if (Math.Abs(xdelta_a) <= 0.000000001 && Math.Abs(ydelta_b) <= 0.000000001)
			{
				Console.WriteLine("first");
				p.X = 0.5 * (p2.X + p3.X);
				p.Y = 0.5 * (p1.Y + p2.Y);
				p.Z = p1.Z;
				radius = tsg.Distance.PointToPoint(p, p1);

				return new Tuple<double, tsg.Point>(radius, p);
			}

			double aSlope = ydelta_a / xdelta_a;
			double bSlope = ydelta_b / xdelta_b;

			if (Math.Abs(aSlope - bSlope) <= 0.000000001)
			{
				Console.WriteLine("second");
				return null;
			}
			Console.WriteLine("third");
			p.X = (aSlope * bSlope * (p1.Z - p3.Z) + bSlope * (p1.X + p2.X) - aSlope * (p2.X + p3.X))
				/ (2 * (bSlope - aSlope));
			p.Z = -1 * (p.X - (p1.X + p2.X) / 2) / aSlope + (p1.Z + p2.Z) / 2;
			p.Y = p1.Y;
			radius = tsg.Distance.PointToPoint(p1, p);
			return new Tuple<double, tsg.Point>(radius, p);
		}

		private static double getH(double r, double s, double dir)
		{
			if (dir < 0)
			{
				return (r + s * 0.5);
			}
			if (dir > 0)
			{
				return (r - s * 0.5);
			}
			return r;

		}

		private static double getR(double r, double dir)
		{
			if (dir < 0)
			{
				return (r * 2.5 / 3);
			}
			if (dir > 0)
			{
				return (r * 2.5 / 2);
			}
			return r;
		}

		private static double getL(tsm.ContourPoint p1, tsm.ContourPoint p2, double r = 0)
		{
			tsg.Point p11 = new tsg.Point(p1.X, p1.Y, p1.Z);
			tsg.Point p22 = new tsg.Point(p2.X, p2.Y, p2.Z);
			return (tsg.Distance.PointToPoint(p11, p22) - r);
		}
		private static double getL(tsg.Point p1, tsg.Point p2, double r = 0)
		{
			return (tsg.Distance.PointToPoint(p1, p2) - r);
		}

		private static tsg.Point cp(tsm.ContourPoint c)
		{
			return new tsg.Point(c.X, c.Y, c.Z);
		}

		private static double getArlLength(double r, double h, double angle)
		{
			if (r == 0) return 0;
			double R = r; //h / (Math.Log(1 + h / r));
			return Math.PI * R * (180 - angle) / 180;
		}
		private static tsg.Vector getVy(tsg.Vector v)
		{
			return new tsg.Vector(v.Y, -v.X, v.Z);
		}
		private static tsg.Vector newV(tsg.Point p1, tsg.Point p2)
		{
			return new tsg.Vector(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
		}
		private static tsg.Vector newV(tsm.ContourPoint p1, tsm.ContourPoint p2)
		{
			tsg.Point p11 = new tsg.Point(p1.X, p1.Y, p1.Z);
			tsg.Point p22 = new tsg.Point(p2.X, p2.Y, p2.Z);
			return new tsg.Vector(p22.X - p11.X, p22.Y - p11.Y, p22.Z - p11.Z);
		}
		private static void DrawCircle(tsg.Point p, tsd.ViewBase vb)
		{
			tsd.Circle cl = new Circle(vb, p, 10);
			cl.Insert();
		}

		private static Tuple<int, double> GetMinPoint(ArrayList list, tsg.Point ps)
		{
			int f = 0;

			// отрезки
			List<tsg.LineSegment> ls = new List<tsg.LineSegment>();
			for (int i = 0; i < list.Count - 1; i++)
			{
				tsg.Point next = (list[i + 1] as tsg.Point);
				tsg.Point cur = (list[i] as tsg.Point);
				ls.Add(new tsg.LineSegment(next, cur));
			}

			// ближайший отрезок
			double dist = double.MaxValue;
			tsg.LineSegment l0 = new tsg.LineSegment();
			for (int i = 0; i < ls.Count; i++) // (tsg.LineSegment l in ls)
			{
				double d = tsg.Distance.PointToLineSegment(ps, ls[i]);

				if (dist > d)
				{
					l0 = ls[i];
					dist = d;
					f = i;
				}
			}

			double len = tsg.Distance.PointToPoint(tsg.Projection.PointToLine(ps, new tsg.Line(l0)), l0.Point2);

			return new Tuple<int, double>(f, len);

		}

		private static double FullLength(double[] l, double[] lr, double[] lmr)
		{
			double t = 0;

			for (int i = 0; i < l.Length; i++)
			{
				//Console.WriteLine("i = " + i + "; l=" + l[i] + "; lr= " + lr[i] + "; lmr= " + lmr[i]);
				t = t + (l[i] + lr[i] - lmr[i]);
			}
			return t;
		}
	}
}
