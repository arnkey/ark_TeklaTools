﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using tsd = Tekla.Structures.Drawing;
using ts = Tekla.Structures;
using ctg = ArkLib.ark_Tekla.Catalogs.ark_UDACatalog;
using tsm = Tekla.Structures.Model;
using tsg = Tekla.Structures.Geometry3d;
using Tekla.Structures;
using iText.Kernel.Pdf;
using System.Text;

namespace ark_TeklaDrawingTools
{
    public partial class ark_TeklaDrawingTools : Form
    {
        public ark_TeklaDrawingTools()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Усечение вида
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_VwExc_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            tsd.UI.DrawingObjectSelector dos = dh.GetDrawingObjectSelector();
            tsd.DrawingObjectEnumerator doe = dos.GetSelected();
            while (doe.MoveNext())
            {
                if (doe.Current is tsd.View)
                {
                    tsd.View vw = doe.Current as tsd.View;
                    double length = Convert.ToDouble(txt_VwExc.Text);
                    if (length == -1 || length == 0)
                    {
                        vw.Attributes.Shortening.CutParts = false;
                    }
                    else
                    {
                        vw.Attributes.Shortening.CutParts = true;
                        //#if
                        //vw.Attributes.Shortening.CutPartType = tsd.View.ShorteningCutPartType.BothDirections;
                        vw.Attributes.Shortening.MinimumLength = length;
                    }
                    vw.Modify();
                }
            }
        }

        private void btn_VwScale_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            tsd.UI.DrawingObjectSelector dos = dh.GetDrawingObjectSelector();
            tsd.DrawingObjectEnumerator doe = dos.GetSelected();
            while (doe.MoveNext())
            {
                if (doe.Current is tsd.View)
                {
                    tsd.View vw = doe.Current as tsd.View;
                    double scale = Convert.ToDouble(txt_VwScale.Text);

                    if (vw.Attributes.Scale != scale)
                    {
                        vw.Attributes.Scale = scale;
                        vw.Modify();
                    }
                }
            }
        }

        private void btn_DrawingNumbering_Click(object sender, EventArgs e)
        {
            Numbering2016i num = new Numbering2016i();
            num.Run(txt_DrawingNumbering.Text);
        }

        #region Формат чертежа

        private int[] fFormat = new int[5]; //a0; a1; a2; a3; a4;
        private bool[] fFormatReverse = new bool[5]; // false = горизонтальный, true = вертикальный
        private int[] fFormatExtend = new int[5]; // 1..6
        // текущие настройки
        private int now_fFormatNow = 3; // a3
        private bool now_fFormatReverse = false; // =
        private int now_fFormatExtend = 1; // a4x1
        private double width = 0;
        private double height = 0;
        private bool ch_reverse_format = false;

        private void btn_Format_Minus_Click(object sender, EventArgs e)
        {
            ext_format(1);
        }

        private void btn_Format_Plus_Click(object sender, EventArgs e)
        {
            ext_format(0);
        }

        private void btn_FormatReverse_Click(object sender, EventArgs e)
        {
            now_fFormatReverse = !now_fFormatReverse;
            fFormatReverse[now_fFormatNow] = now_fFormatReverse;
            vChangeFormat();
        }

        private void vChangeFormat()
        {
            double temp = 0;
            string s = "";

            #region Формат
            switch (now_fFormatNow)
            {
                case 4:
                    width = 297;
                    height = 210;
                    break;
                case 3:
                    width = 420;
                    height = 297;
                    break;
                case 2:
                    width = 594;
                    height = 420;
                    break;
                case 1:
                    width = 841;
                    height = 594;
                    break;
                case 0:
                    width = 1189;
                    height = 841;
                    break;
            }
            #endregion
            #region Расширенный формат
            s = "A" + now_fFormatNow.ToString();
            if (now_fFormatExtend >= 2)
            {
                s += "x" + now_fFormatExtend;
                temp = width;
                width = height;
                height = temp;
                width *= now_fFormatExtend;
            }
            #endregion
            #region Перевернутый формат
            //s += (now_fFormatReverse == true ? " ||" : " =");
            #endregion
            #region Метка
            btn_FormatChange.Text = s + (now_fFormatReverse == true ? (" ||\n" + height + "x" + width) : (" =\n" + width + "x" + height));
            #endregion
        }

        private void btn_FormatChange_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            draw.Layout.SheetSize.Height = Convert.ToDouble(now_fFormatReverse == true ? width : height);
            draw.Layout.SheetSize.Width = Convert.ToDouble(now_fFormatReverse == true ? height : width);
            draw.Modify();
            draw.CommitChanges();
        }

        private void btn_FormatA_Minus_Click(object sender, EventArgs e)
        {
            ch_format(0);
        }

        private void btn_FormatA_Plus_Click(object sender, EventArgs e)
        {
            ch_format(1);
        }


        private void ext_format(int k)
        {
            // k = параметр, понижения формата или повышения
            if (k == 0)
            {
                if (now_fFormatExtend != 6)
                {
                    // если формат не а0 и не = Nx1;
                    if ((now_fFormatNow != 0) && (fFormatExtend[now_fFormatNow] == 1))
                    {
                        now_fFormatExtend++;
                        fFormatExtend[now_fFormatNow]++;
                    }
                    now_fFormatExtend++;
                    fFormatExtend[now_fFormatNow]++;
                }
            }
            else if (k == 1)
            {
                if (now_fFormatExtend != 1)
                {
                    if ((now_fFormatExtend == 3) && (now_fFormatNow != 0))
                    {
                        now_fFormatExtend--;
                        fFormatExtend[now_fFormatNow]--;
                    }
                    now_fFormatExtend--;
                    fFormatExtend[now_fFormatNow]--;
                }
            }
            vChangeFormat();
        }
        // смена формата
        private void ch_format(int k)
        {
            if (k == 0)
            {
                if (now_fFormatNow != 4)
                {
                    now_fFormatNow++;
                    now_fFormatExtend = fFormatExtend[now_fFormatNow];
                }
            }
            else if (k == 1)
            {
                if (now_fFormatNow != 0)
                {
                    now_fFormatNow--;
                    now_fFormatExtend = fFormatExtend[now_fFormatNow];
                }
            }

            vChangeFormat();
        }

        #endregion

        private void ark_TeklaDrawingTools_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            vChangeFormat();
            rb_CenLine.Checked = true;

            cb_btn_DrawingNmTl.SelectedIndex = 0;
            cb_DrawingNmTlType.SelectedIndex = 0;

            //cb_DrawingNmTlType

            ctg.Load();
            foreach (KeyValuePair<string, string> uda in ctg.Drawing_User_Attributes)
            {
                cb_DrawingNmTlType.Items.Add(uda.Value);
            }

            //tabControl1.TabPages.Remove(tabControl1.TabPages["debug"]);

            /* Файлы свойств */
            #region Файлы свойств вида чертежа

            //tsm.Model model = new tsm.Model();
            //tsm.ModelInfo minfo = model.GetInfo();

            //TeklaStructuresFiles tsf = new TeklaStructuresFiles(minfo.ModelPath);
            //List<string> list = tsf.GetMultiDirectoryFileList("ad", false);
            //foreach(string l in list)
            //    cmb_DrawingProperties.Items.Add(l);

            string xs_driver = "";
            ts.TeklaStructuresSettings.GetAdvancedOption("XS_DRIVER", ref xs_driver);
            System.IO.DirectoryInfo dif = new System.IO.DirectoryInfo(System.IO.Path.GetDirectoryName(xs_driver));
            List<string> DrawingProperties = new List<string>();
            //cmb_DrawingProperties.Items.Add("-");
            foreach (System.IO.FileInfo fi in dif.GetFiles("*.vi"))
            {
                cmb_DrawingProperties.Items.Add(fi.Name.Replace(".vi", ""));
            }
            //cmb_DrawingProperties.Items.Add("Монтажная схема");
            //cmb_DrawingProperties.SelectedIndex = 0;
            #endregion
        }

        private void btn_VwName_Click(object sender, EventArgs e)
        {
            string name = txt_Vw_Name.Text;
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
            while (doe.MoveNext())
            {
                //MessageBox.Show(doe.Current.GetType().ToString());
                if (doe.Current is tsd.View)
                {
                    tsd.View v = doe.Current as tsd.View;
                    //v.Name = name; 
#region rev 19.01.2017
                    v.Attributes.TagsAttributes.TagA1.TagContent.Clear();
                    tsd.TextElement text = new tsd.TextElement(name);
                    text.Font.Name = "GOST 2.304 type A";
                    text.Font.Color = tsd.DrawingColors.Blue;
                    text.Font.Height = 4;
                    v.Attributes.TagsAttributes.TagA1.TagContent.Add(text);
#endregion 
                    v.Modify();
                }
            }
        }

        private void rb_RefLine_CheckedChanged(object sender, EventArgs e)
        {
            rb_CenLine.Checked = !rb_RefLine.Checked;
        }

        private void rb_CenLine_CheckedChanged(object sender, EventArgs e)
        {
            rb_RefLine.Checked = !rb_CenLine.Checked;
        }

        private void btn_RefCenLineModify_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            tsd.UI.DrawingObjectSelector dos = dh.GetDrawingObjectSelector();
            tsd.DrawingObjectEnumerator doe = dos.GetSelected();
            while (doe.MoveNext())
            {
                if (doe.Current is tsd.Part)
                {
                    tsd.Part p = doe.Current as tsd.Part;
                    int inx = rb_CenLine.Checked ? 1 : 0;
                    if (inx == 0)
                    {
                        p.Attributes.DrawReferenceLine = !p.Attributes.DrawReferenceLine;
                    }
                    if (inx == 1)
                    {
                        p.Attributes.DrawCenterLine = !p.Attributes.DrawCenterLine;
                    }
                    p.Modify();
                }
            }
            draw.CommitChanges();
        }


        void help_DrawTitles(tsd.Drawing draw, string text)
        {
            if (cb_DrawingNmTlType.SelectedIndex == 0)
            {
                if (draw.Name != text)
                {
                    draw.Name = text;
                    draw.Modify();
                }
            }
            else if (cb_DrawingNmTlType.SelectedIndex == 1)
            {
                if (draw.Title1 != text)
                {
                    draw.Title1 = text;
                    draw.Modify();
                }
            }
            else if (cb_DrawingNmTlType.SelectedIndex == 2)
            {
                if (draw.Title2 != text)
                {
                    draw.Title2 = text;
                    draw.Modify();
                }
            }
            else if (cb_DrawingNmTlType.SelectedIndex == 3)
            {
                if (draw.Title3 != text)
                {
                    draw.Title3 = text;
                    draw.Modify();
                }
            }
        }

        private void btn_DrawingNmTlModify_Click(object sender, EventArgs e)
        {
            string cmb_text = (cb_btn_DrawingNmTl.SelectedItem.ToString() == "-" ? "" : cb_btn_DrawingNmTl.SelectedItem.ToString());
            string text = (cmb_text != "-" ? cmb_text + " " : "") + txt_btn_DrawingNmTl.Text;
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
                help_DrawTitles(draw, text);
            else
            {
                tsd.DrawingEnumerator de = dh.GetDrawingSelector().GetSelected();

                while (de.MoveNext())
                {
                    if (cb_DrawingNmTlType.SelectedIndex >= 0 && cb_DrawingNmTlType.SelectedIndex <= 3)
                        help_DrawTitles(de.Current as tsd.Drawing, text);
                    else
                    {
                        foreach (KeyValuePair<string, string> k in ctg.Drawing_User_Attributes)
                        {
                            if (k.Value == cb_DrawingNmTlType.SelectedItem.ToString())
                            {
                                (de.Current as tsd.Drawing).SetUserProperty(k.Key, text);
                            }
                        }
                    }
                }
            }
        }

        private void btn_DrawingNumbering_Click_1(object sender, EventArgs e)
        {
            Numbering2016i num = new Numbering2016i();
            txt_DrawingNumbering.Text = num.Run(txt_DrawingNumbering.Text);
        }
        private void btn_DrawingProperties_Click(object sender, EventArgs e)
        {
            string property = cmb_DrawingProperties.SelectedItem.ToString();
            if (property == "-") return;
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
                tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                while (doe.MoveNext())
                {
                    if (doe.Current is tsd.View)
                    {
                        tsd.View v = doe.Current as tsd.View;
                        v.Attributes.LoadAttributes(property);
                        v.Modify();
                    }
                }
            }
        }

        private void cmb_DrawingProperties_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btn_Del_All_Dimensions_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
                tsd.DrawingObjectEnumerator doe = draw.GetSheet().GetAllObjects();
                while (doe.MoveNext())
                {
                    if (doe.Current is tsd.DimensionBase)
                    {
                        doe.Current.Delete();
                    }
                }
            }
        }

        private void btn_RadiusPolyBeam_Click(object sender, EventArgs e)
        {
			EmbededMark em = new EmbededMark();
        }

        private void btn_DimensionBeetwenParts_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
                tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                List<tsd.Part> parts = new List<tsd.Part>();
                while (doe.MoveNext())
                {
                    if (doe.Current is tsd.Part) parts.Add(doe.Current as tsd.Part);
                }

                tsd.ViewBase vb = parts[0].GetView();
                vb.Select();
                tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();

                tsm.TransformationPlane tpCur = wph.GetCurrentTransformationPlane();
                tsm.TransformationPlane tpView = new tsm.TransformationPlane((vb as tsd.View).DisplayCoordinateSystem);
                wph.SetCurrentTransformationPlane(tpView);

                tsm.ModelObject mobj1 = new tsm.Model().SelectModelObject(parts[0].ModelIdentifier);
                tsm.ModelObject mobj2 = new tsm.Model().SelectModelObject(parts[1].ModelIdentifier);

                // Для контурных пластин
                if (mobj1 is tsm.ContourPlate && mobj2 is tsm.ContourPlate)
                {
                    tsm.ContourPlate cp1 = mobj1 as tsm.ContourPlate;
                    tsm.ContourPlate cp2 = mobj2 as tsm.ContourPlate;
                    cp1.Select();
                    cp2.Select();
                    tsg.Point p1 = ReturnPoint(cp1.Contour.ContourPoints[0] as tsm.ContourPoint);
                    tsg.Point p2 = ReturnPoint(cp2.Contour.ContourPoints[0] as tsm.ContourPoint);

                    tsd.PointList pl = new tsd.PointList();
                    pl.Add(p1); pl.Add(p2);

                    //tsd.StraightDimensionSet sds = new tsd.StraightDimensionSetHandler().CreateDimensionSet(vb,
                    //    pl, ArkLib.x_algoritms.x_New.Vector(p1, p2), 100);

                    tsg.Vector v = ArkLib.x_algoritms.x_New.Vector(p1, p2);
                    v.Normalize();
                   

                    tsd.StraightDimension sd = new tsd.StraightDimension(parts[0].GetView(), p1, p2,
                         new tsg.Vector(1, 0,0) , 100);
                    sd.Insert().ToString();
                }

                wph.SetCurrentTransformationPlane(tpCur);
            }
        }

        tsg.Point ReturnPoint(tsm.ContourPoint cp)
        {
            return new tsg.Point(cp.X, cp.Y, cp.Z);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
                tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                List<tsd.Part> parts = new List<tsd.Part>();
                while (doe.MoveNext())
                {
                    if (doe.Current is tsd.Part) parts.Add(doe.Current as tsd.Part);
                }

                tsd.ViewBase vb = parts[0].GetView();
                vb.Select();
                tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();

                tsm.TransformationPlane tpCur = wph.GetCurrentTransformationPlane();
                tsm.TransformationPlane tpView = new tsm.TransformationPlane((vb as tsd.View).DisplayCoordinateSystem);
                wph.SetCurrentTransformationPlane(tpView);

                tsm.ModelObject mobj1 = new tsm.Model().SelectModelObject(parts[0].ModelIdentifier);
                tsm.ModelObject mobj2 = new tsm.Model().SelectModelObject(parts[1].ModelIdentifier);

                // Для контурных пластин
                if (mobj1 is tsm.ContourPlate && mobj2 is tsm.ContourPlate)
                {
                    tsm.ContourPlate cp1 = mobj1 as tsm.ContourPlate;
                    tsm.ContourPlate cp2 = mobj2 as tsm.ContourPlate;
                    cp1.Select();
                    cp2.Select();
                    tsg.Point p1 = ReturnPoint(cp1.Contour.ContourPoints[0] as tsm.ContourPoint);
                    tsg.Point p2 = ReturnPoint(cp2.Contour.ContourPoints[0] as tsm.ContourPoint);

                    tsd.PointList pl = new tsd.PointList();
                    pl.Add(p1); pl.Add(p2);

                    //tsd.StraightDimensionSet sds = new tsd.StraightDimensionSetHandler().CreateDimensionSet(vb,
                    //    pl, ArkLib.x_algoritms.x_New.Vector(p1, p2), 100);

                    tsg.Vector v = ArkLib.x_algoritms.x_New.Vector(p1, p2);
                    v.Normalize();


                    tsd.StraightDimension sd = new tsd.StraightDimension(parts[0].GetView(), p1, p2,
                         new tsg.Vector(0, 1, 0), 100);
                   sd.Insert().ToString();
                }

                wph.SetCurrentTransformationPlane(tpCur);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
                tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                List<tsd.Bolt> bolts = new List<tsd.Bolt>();
                while (doe.MoveNext())
                {
                    if (doe.Current is tsd.Bolt) bolts.Add(doe.Current as tsd.Bolt);
                }

                tsd.ViewBase vb = bolts[0].GetView();

                vb.Select();
                tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();

                tsm.TransformationPlane tpCur = wph.GetCurrentTransformationPlane();
                tsm.TransformationPlane tpView = new tsm.TransformationPlane((vb as tsd.View).DisplayCoordinateSystem);
                wph.SetCurrentTransformationPlane(tpView);

                tsm.BoltArray ba = new tsm.Model().SelectModelObject(bolts[0].ModelIdentifier) as tsm.BoltArray;
                ba.Select();
                tsg.Point min = ba.BoltPositions[0] as tsg.Point;
                tsg.Point max = ba.BoltPositions[ba.BoltPositions.Count - 1] as tsg.Point;

                tsg.Vector v = ArkLib.x_algoritms.x_New.Vector(min, max);
                v.Normalize();

                tsg.Vector dir = new tsg.Vector(1, 0, 0);
                if (max.X - min.X <= 0)
                    dir = new tsg.Vector(0, 1, 0);
                else
                    dir = new tsg.Vector(1, 0, 0);

                tsd.StraightDimension sd = new tsd.StraightDimension(bolts[0].GetView(), min, max, dir, 100);
                sd.Insert();

               wph.SetCurrentTransformationPlane(tpCur);
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
				tsd.DrawingObjectEnumerator doe = draw.GetSheet().GetAllObjects();
				while (doe.MoveNext())
				{
					if (doe.Current is tsd.Mark)
					{
						tsd.Mark m = doe.Current as tsd.Mark;
						tsd.DrawingObjectEnumerator doe2 = m.GetRelatedObjects();
						string s = "";
						while (doe2.MoveNext())
						{
							if (doe2.Current is tsd.Part)
							{
								
								tsm.ModelObject mobj = new tsm.Model().SelectModelObject(((tsd.Part)doe2.Current).ModelIdentifier);
								mobj.GetReportProperty("ASSEMBLY_POS", ref s); break;
							}
						}
						if (s == "D-5" || s == "D-18") m.Delete();
					}
				}
            }
        }


         private void btn_OpenSaveClose_Click(object sender, EventArgs e)
         {
             tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.Drawing draw = dh.GetActiveDrawing();
            if (draw != null)
            {
                dh.CloseActiveDrawing(true);
                return;
            }
            else
            {
                tsd.DrawingEnumerator de = dh.GetDrawingSelector().GetSelected();
                while (de.MoveNext())
                {
                    tsd.Drawing dr = de.Current as tsd.Drawing;
                    dh.SetActiveDrawing(dr, false);
                    dh.CloseActiveDrawing(true);
                }
            }
            MessageBox.Show("done");
         }

         private void button4_Click(object sender, EventArgs e)
         {
             tsd.DrawingHandler dh = new tsd.DrawingHandler();
             if (dh.GetConnectionStatus() == false) return;
             tsd.Drawing draw = dh.GetActiveDrawing();
             if (draw != null)
             {
                 tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                 List<tsd.Part> parts = new List<tsd.Part>();
                 while (doe.MoveNext())
                 {
                     if (doe.Current is tsd.Part) parts.Add(doe.Current as tsd.Part);
                 }

                 tsd.ViewBase vb = parts[0].GetView();

                 vb.Select();
                 //tsm.WorkPlaneHandler wph = new tsm.Model().GetWorkPlaneHandler();

                 //tsm.TransformationPlane tpCur = wph.GetCurrentTransformationPlane();
                 //tsm.TransformationPlane tpView = new tsm.TransformationPlane((vb as tsd.View).DisplayCoordinateSystem);
                 //wph.SetCurrentTransformationPlane(tpView);

                 tsg.Matrix m = tsg.MatrixFactory.ToCoordinateSystem((vb as tsd.View).DisplayCoordinateSystem);

                 tsd.PointList pl = new tsd.PointList();
                 tsd.PointList pl2 = new tsd.PointList();



                 tsd.UI.Picker picker = dh.GetPicker();
                 tsg.Point p1 = new tsg.Point();
                 tsg.Point p2 = new tsg.Point();
                 tsd.ViewBase vb0 = null;
                 picker.PickPoint("", out p1, out vb0);
                 pl.Add(p1);
                 pl2.Add(p1);
                 picker.PickPoint("", out p2, out vb0);
                 pl2.Add(p2);
                 foreach (tsd.Part p in parts)
                 {
                     tsm.Part part = new tsm.Model().SelectModelObject(p.ModelIdentifier) as tsm.Part;
                     pl.Add(m.Transform(part.GetReferenceLine(true)[0] as tsg.Point));
                     pl2.Add(m.Transform(part.GetReferenceLine(true)[0] as tsg.Point));
                 }

                 pl.Add(p2);

                 tsg.Vector dirX = new tsg.Vector(1, 0, 0);
                 tsg.Vector dirY = new tsg.Vector(0, 1, 0);
                 tsg.Vector dirXY = new tsg.Vector(1, 1, 0);

                 //tsd.StraightDimension sd = new tsd.StraightDimension(bolts[0].GetView(), min, max, dir, 100);

                 tsd.StraightDimensionSetHandler sds = new tsd.StraightDimensionSetHandler();
                 sds.CreateDimensionSet(vb, pl, dirX, 100);
                 sds.CreateDimensionSet(vb, pl, dirY, 100);
                 sds.CreateDimensionSet(vb, pl2, dirXY, 100);
                 //sds.Insert();

                 ///sd.Insert();

                 //wph.SetCurrentTransformationPlane(tpCur);
             }
         }

         private void button5_Click(object sender, EventArgs e)
         {
              tsd.DrawingHandler dh = new tsd.DrawingHandler();
             if (dh.GetConnectionStatus() == false) return;
             tsd.Drawing draw = dh.GetActiveDrawing();
             if (draw != null)
             {
                 tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                 while (doe.MoveNext())
                 {
                     if (doe.Current is tsd.StraightDimensionSet)
                     {
                         tsd.StraightDimensionSet sd = doe.Current as tsd.StraightDimensionSet;

                         if (rb_dim_absolute.Checked)
                         {
                             sd.Attributes.DimensionType = tsd.DimensionSetBaseAttributes.DimensionTypes.Absolute;
                             sd.Modify();
                         }
                         else if (rb_dim_absolute_relative.Checked)
                         {
                             sd.Attributes.DimensionType = tsd.DimensionSetBaseAttributes.DimensionTypes.RelativeAndAbsolute;
                             sd.Modify();
                         }
                         else if (rb_dim_relative.Checked)
                         {
                             sd.Attributes.DimensionType = tsd.DimensionSetBaseAttributes.DimensionTypes.Relative;
                             sd.Modify();
                         }
                     }

                 }
             }
         }

         private void button6_Click(object sender, EventArgs e)
         {
             ArnKeyTools at = new ArnKeyTools();
             at.Show();
         }

         private void button7_Click(object sender, EventArgs e)
         {
             
         }

         private void button8_Click(object sender, EventArgs e)
         {
             //tsd.DrawingHandler dh = new tsd.DrawingHandler();
             //if (dh.GetConnectionStatus() == false) return;
             //tsd.Drawing draw = dh.GetActiveDrawing();
             //if (draw != null)
             //{
             //    tsd.UI.Picker pick = dh.GetPicker();
             //    tsd.ViewBase vb = null;
             //    tsg.Point p1 = null; tsg.Point p2 = null;
             //    List<tsg.Point> points = new List<tsg.Point>();
             //    tsd.PointList plist = new tsd.PointList();
             //    pick.PickTwoPoints("Первая точка", "Вторая точка", out p1, out p2, out vb);
             //    plist.Add(p1); plist.Add(p2);

             //    tsd.PluginPickerInput newPluginInput = new tsd.PluginPickerInput();
             //    newPluginInput.Add(new tsd.PickerInputNPoints(vb, plist));
             //    tsd.Plugin pl = new tsd.Plugin(vb, "ark_weld_pl");

             //    bool b = pl.LoadStandardValues("standard");

             //    pl.SetPickerInput(newPluginInput);
             //    pl.Insert();
             //    //pl.Select();
             //    bool b1 = pl.TrySetAttribute("w_type", 2);
             //    pl.SetAttribute("w_type", (int)2);
             //    //bool b1 = pl.TrySetAttribute("shop", "1");
             //    int str = pl.GetIntAttribute("w_type");
             //    pl.Modify();
             //    MessageBox.Show(b + " "+ b1 + str);
             //}
         }

         private void btn_pres_Part_Click(object sender, EventArgs e)
         {
              tsd.DrawingHandler dh = new tsd.DrawingHandler();
             if (dh.GetConnectionStatus() == false) return;
             tsd.Drawing draw = dh.GetActiveDrawing();
             if (draw != null)
             {
                 tsd.DrawingObjectEnumerator doe = dh.GetDrawingObjectSelector().GetSelected();
                 while (doe.MoveNext())
                 {
                     if (doe.Current is tsd.Part)
                     {
                         tsd.Part p = doe.Current as tsd.Part;
                        // _debug(p.Attributes.DrawPopMarks);
                         if (rb_pres_contour.Checked)
                         {
                             if (p.Attributes.Representation != tsd.Part.Representation.Outline)
                             {
                                 p.Attributes.Representation = tsd.Part.Representation.Outline;
                                 p.Modify();
                             }
                         }
                         if (rb_pres_accur.Checked)
                         {
                             if (p.Attributes.Representation != tsd.Part.Representation.Exact)
                             {
                                 p.Attributes.Representation = tsd.Part.Representation.Exact;
                                 p.Modify();
                             }
                         }
                     }
                 }
             }
         }


         void _debug(bool b)
         {
             _debug(b.ToString());
         }
         void _debug(int b)
         {
             _debug(b.ToString());
         }
         void _debug(double b)
         {
             _debug(b.ToString());
         }
         void _debug(string b)
         {
             MessageBox.Show(b);
         }

         private void button9_Click(object sender, EventArgs e)
         {
             tsd.DrawingHandler dh = new tsd.DrawingHandler();
             if (dh.GetConnectionStatus() == false) return;
             tsd.DrawingEnumerator de = dh.GetDrawingSelector().GetSelected();
             while (de.MoveNext())
             {
                 string name = ""; 
                 tsd.Drawing draw = de.Current as tsd.Drawing;
                 if (draw is tsd.AssemblyDrawing)
                 {
                     tsd.AssemblyDrawing ad = draw as tsd.AssemblyDrawing;
                     tsm.ModelObject mobj = new tsm.Model().SelectModelObject(ad.AssemblyIdentifier);
                     (mobj as tsm.Assembly).GetMainPart().GetReportProperty("NAME", ref name);
                 }
                 if (draw is tsd.SinglePartDrawing)
                 {
                     tsd.SinglePartDrawing spd = de.Current as tsd.SinglePartDrawing;
                     tsm.ModelObject mobj = new tsm.Model().SelectModelObject(spd.PartIdentifier);
                     (mobj as tsm.Part).GetReportProperty("NAME", ref name);
                 }
                 if (name != "" && draw.Name != name)
                 {
                     draw.Name = name;
                     draw.Modify();
                     draw.CommitChanges();
                 }
             }
             
         }

         private void button7_Click_1(object sender, EventArgs e)
         {
             ark_Weld aw = new ark_Weld();
             aw.Show();
         }


		private void btn_SinglePartCurved_Click(object sender, EventArgs e)
		{
			SinglePart_Curved sp = new SinglePart_Curved();
		}

		private void btn_SinglePartTube_Click(object sender, EventArgs e)
		{
			SinglePartTube sp = new SinglePartTube();
		}

        private void button8_Click_1(object sender, EventArgs e)
        {
            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.DrawingEnumerator de = dh.GetDrawingSelector().GetSelected();
            while (de.MoveNext())
            {
                string name = "";
                tsd.Drawing draw = de.Current as tsd.Drawing;
                if (draw is tsd.SinglePartDrawing)
                {
                    tsd.SinglePartDrawing spd = de.Current as tsd.SinglePartDrawing;
                    tsm.ModelObject mobj = new tsm.Model().SelectModelObject(spd.PartIdentifier);
                    (mobj as tsm.Part).GetReportProperty("PROFILE", ref name);
                    if (name.Contains("~"))
                    {
                        name = name.Split(new char[] { '~' })[0];
                    }
                    else if (name.Contains("PL"))
                    {
                        name = "-" + name.Replace("PL", "").Split(new char[] { 'X' })[0];
                    }
                }
                if (name != "" && draw.Title3 != name)
                {
                    draw.Title3 = name;
                    draw.Modify();
                    draw.CommitChanges();
                }
            }
        }

        private void toNameASMRUS_Click(object sender, EventArgs e)
        {
            tsd.DrawingHandler.SetMessageExecutionStatus(tsd.DrawingHandler.MessageExecutionModeEnum.BY_COMMIT);
            string s1 = "ru_naz_chert_1";
            string s2 = "ru_naz_chert_2";
            string s3 = "ru_naz_chert_3";

            tsd.DrawingHandler dh = new tsd.DrawingHandler();
            if (dh.GetConnectionStatus() == false) return;
            tsd.DrawingEnumerator de = dh.GetDrawingSelector().GetSelected();
            while (de.MoveNext())
            {
                tsd.Drawing draw = de.Current as tsd.Drawing;
                if (draw is tsd.AssemblyDrawing)
                {
                    tsd.AssemblyDrawing asd = draw as tsd.AssemblyDrawing;
                    tsm.Assembly asm = new tsm.Model().SelectModelObject(asd.AssemblyIdentifier) as tsm.Assembly;

                    string pos = ""; asm.GetReportProperty("ASSEMBLY_POS", ref pos);
                    string name = ""; asm.GetReportProperty("ASSEMBLY_NAME", ref name);

                    draw.SetUserProperty(s1, "Отправочная марка");
                    draw.SetUserProperty(s3, pos);
                    draw.SetUserProperty(s2, name);
                    draw.CommitChanges();
                }
                else if (draw is tsd.SinglePartDrawing)
                {
                    tsd.SinglePartDrawing asd = draw as tsd.SinglePartDrawing;
                    tsm.Part asm = new tsm.Model().SelectModelObject(asd.PartIdentifier) as tsm.Part;

                    string pos = ""; asm.GetReportProperty("PART_POS", ref pos);
                   // string name = ""; asm.GetReportProperty("ASSEMBLY_NAME", ref name);

                    draw.SetUserProperty(s1, "Деталь");
                    draw.SetUserProperty(s3, pos);
                    draw.CommitChanges();
                }
            }
           // dh.c
        }

        private void button10_Click(object sender, EventArgs e)
        {
            string inp = @"C:\TeklaStructuresModels\Аппарель\PlotFiles\G1.pdf";
            string outs = @"C:\TeklaStructuresModels\Аппарель\PlotFiles\G1_new.pdf";

            PdfDocument pdfDoc = new PdfDocument(new PdfReader(inp), new PdfWriter(outs));
            PdfPage page = pdfDoc.GetFirstPage();
            PdfDictionary dict = page.GetPdfObject();
            PdfObject objects = dict.Get(PdfName.Contents);
            if (objects is PdfStream)
            {
                PdfStream stream = (PdfStream)objects;
                byte[] data = stream.GetBytes();

                string dd = Encoding.UTF8.GetString(data, 0, data.Length)
                    .Replace("Tekla Structures Educational", "")
                    .Replace("Tekla Structures","");

                byte[] newdata = Encoding.UTF8.GetBytes(dd);

                stream.SetData(newdata);
            }
            pdfDoc.Close();
            
        }
    }
}
